<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;


class gradeModel extends Model
{
    protected $table='mst_grade_master';
    protected $primaryKey ='id';
    protected $fillable=['id ','grade_desc','min_score','max_score ','status','created_at','updated_at'];  
    public $timestamps = true;
    //public $softDelete =true;

}
