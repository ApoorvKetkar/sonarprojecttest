<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\instituteTypesModel;

class parametersModel extends Model
{
    protected $table='mst_parameter_master';
    protected $primaryKey ='id';
    protected $fillable=['id','parameter_name','para_desc','inst_type_id','status','created_at','updated_at'];  
    public $timestamps = true;
    public $softDelete =true;   


    

    // public function institutetype_name()
    // {
    //     return $this->hasOne('App\Models\instituteTypesModel');
    // }


}
