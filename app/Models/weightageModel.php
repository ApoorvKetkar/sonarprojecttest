<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class weightageModel extends Model
{
     protected $table='mst_weightage_master';
    protected $primaryKey ='id';
    protected $fillable=['id','option_value ','weightage'];  
    public $timestamps = false;
    public $softDelete =true;   

}
