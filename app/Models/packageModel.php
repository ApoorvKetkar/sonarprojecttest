<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class packageModel extends Model
{
     protected $table='mst_package_master';
    protected $primaryKey ='id';
    protected $fillable=['id','package_name','package_cost ','number_of_surveys','feedbacks_per_teacher','inst_type_id','status'];  
    public $timestamps = false;
    public $softDelete =true;   

}
