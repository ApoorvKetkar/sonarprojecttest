<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class loginModel extends Model
{
    protected $table='user_master';
    protected $primaryKey ='user_id';
    protected $fillable=['user_id','user_type','salutation','first_name','last_name','email','contact',]; 
}
