<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;


use Illuminate\Database\Eloquent\Model;

class commentsbankModel extends Model
{
    protected $table='mst_comment_master';
    protected $primaryKey ='id';
    protected $fillable=['id ','parameter_id ','grade_id','comment_desc '];  
    public $timestamps = false;
}
