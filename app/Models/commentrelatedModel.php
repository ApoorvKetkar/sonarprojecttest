<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;


class commentrelatedModel extends Model
{
	protected $table='mst_comment_related_master';
    protected $primaryKey ='id';
    protected $fillable=['id ','main_parameter_id','related_parameter_id','related_grade_id','comment_desc '];  
    public $timestamps = false;
    
}
