<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class instituteTypesModel extends Model
{
    // use SoftDeletes;
    protected $table='mst_inst_type_master';
    protected $primaryKey ='id';
    protected $fillable=['id','inst_type_name','inst_desc','status','created_at','updated_at'];  
    public $timestamps = true;
   public $softDelete =true;
}
