<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;


class instituteModel extends Model
{
	 protected $table='oh_master_institute';
     protected $primaryKey ='id';
     protected $fillable=['id','master_institute_name','inst_type_id','status','created_at ','updated_at ','address','city ','pincode ','state','country','user_id','master_inst_code'];  
     public $timestamps = true;
    public $softDelete =true;
    
}


class userCredModel extends Model{
    protected $table='user_credentials';
    protected $primaryKey ='id';
    protected $fillable=['id','master_user_name','password','created_at ','updated_at ','IsActive','tenant_db_connectstring'];  
    public $timestamps = true;
   public $softDelete =true;
}

class userDetailModel extends Model{
    protected $table='user_master';
    protected $primaryKey ='user_id';
    protected $fillable=['user_id','user_type','salutation','first_name','last_name','email','contact'];  
    public $timestamps = false;
   public $softDelete =true;
}