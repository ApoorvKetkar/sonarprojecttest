<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class questionsModel extends Model
{
    protected $table='mst_question_master';
    protected $primaryKey ='id';
    protected $fillable=['id','parameter_id ','questions ','option_1','option_2','option_3','option_4','status','created_at','updated_at'];  
    public $timestamps = true;
    public $softDelete =true;  
}
