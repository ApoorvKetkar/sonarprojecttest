<?php

namespace App\Http\Controllers\package;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\packageModel;
use Illuminate\Support\ServiceProvides  ;
use Illuminate\Support\Facades\DB;
use Session;

class packageController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datasetFilterData = null;
        $inst_type_id =Session::get('inst_id'); 
        if ($inst_type_id != null && +$inst_type_id != -1){
            
            $datasetFilterData =packageModel::where('inst_type_id', '=', $inst_type_id) ->get();
        }else{
            $inst_type_id = -1;
        }

        $dataset2 = DB::table('mst_inst_type_master') -> get();
        return view('/package/package', compact(array('dataset2','datasetFilterData','inst_type_id')));
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    public function institutetypes(){
        $dataset = DB::table('mst_package_master')
        ->select(['mst_package_master.id','mst_inst_type_master.inst_type_name'])
        ->join('mst_inst_type_master','mst_inst_type_master.id', '=','mst_package_master.id')
       ->GROUPBY ('mst_package_master.id')
        ->get();
    }

    public function showfilterdata(Request $request) {
        // echo "Selected Value is : " . $request->input('dropdown_institute_types'); 
        //$dataset2 =DB::table('inst_type_master') -> get();
        //$datasetFilterData = parametersModel::where('inst_type_id', '=', $request->input('dropdown_institute_types')) ->get();
        return back()->with(['inst_id' => $request->input('dropdown_institute_types')]);
    }
   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $int_type_id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$int_type_id)
    {

         $validatedData = $request->validate([
        'txt_name' => 'required|unique:mst_package_master,package_name',
        'txt_package_cost' => 'required|unique:mst_package_master,package_cost',
        'txt_noof_surveys'=>'required',
         'feedbacks_per_teacher'=>'required']);
        $store = new packageModel();
        $store->package_name = $request->txt_name;
        $store->package_cost = $request->txt_package_cost;
        $store->number_of_surveys = $request->txt_noof_surveys;
        $store->feedbacks_per_teacher = $request->txt_feedbacks_per_teacher;
         $store->inst_type_id = $int_type_id;
        $result = $store->save();   
        
        if($result == 1)
            return back()->with('inst_id', $int_type_id)
                         ->with('success', 'Data inserted successfully.');
        else 
            return back()->with('inst_id', $int_type_id)
                         ->with('error', 'Some error occurred while updating data. Please try again!');
   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @param  int  $int_type_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $int_type_id)
   {
         $validatedData = $request->validate([
        'txt_name' => 'required|unique:mst_package_master,package_name',
        'txt_package_cost' => 'required|unique:mst_package_master,package_cost',
        'txt_noof_surveys'=>'required',
         'feedbacks_per_teacher'=>'required']);

        $crud = packageModel::find($id); 
        $crud->id= $id;  
        $crud->package_name =$request->get('txt_name');  
        $crud->package_cost = $request->get('txt_package_cost');
        $crud->number_of_surveys = $request->get('txt_noof_surveys'); 
        $crud->feedbacks_per_teacher = $request->get('txt_feedbacks_per_teacher');   
        $result= $crud->save();

        if($result == 1)
            return back()->with('inst_id', $int_type_id)
                         ->with('success', 'Data updated successfully.');
        else 
            return back()->with('inst_id', $int_type_id)
                         ->with('error', 'Some error occurred while updating data. Please try again!');
    }

    /**
     * Enables the specified resource in Database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @param  int  $int_type_id
     * @return \Illuminate\Http\Response
     */
    public function enable(Request $request, $id, $int_type_id)
    {
        
        $result = packageModel::where('id', $id)
                ->update(['status' => 1]);

         if($result == 1)
            return back()->with('inst_id', $int_type_id)
                         ->with('success', 'Record Enabled successfully.');
         else 
            return back()->with('inst_id', $int_type_id)
                         ->with('error', 'Some error occurred while updating data. Please try again!');


    }

    /**
     * Disables the specified resource in Database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @param  int  $int_type_id
     * @return \Illuminate\Http\Response
     */
    public function disable(Request $request, $id, $int_type_id)
    {
        $result = packageModel::where('id', $id)
                ->update(['status' => 0]);

        if($result == 1)
            return back()->with('inst_id', $int_type_id)
                         ->with('success', 'Record Disabled successfully.');
        else 
            return back()->with('inst_id', $int_type_id)
                         ->with('error', 'Some error occurred while updating data. Please try again!');
            
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param  int  $int_type_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $int_type_id)
    {
        $crud = packageModel::find($id); 
        $crud->id = $id;  
        $crud->status = 0;
        $result= $crud->save();  
        
        if($result == 1)
            return back()->with('inst_id', $int_type_id)
                     ->with('success', 'Data deleted successfully.');
        else 
            return back()->with('inst_id', $int_type_id)
                     ->with('error', 'Some error occurred while updating data. Please try again!');
    
    }
   
   
}
