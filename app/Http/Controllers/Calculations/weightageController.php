<?php

namespace App\Http\Controllers\Calculations;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\weightageModel;
use Illuminate\Support\ServiceProvides  ;
use Illuminate\Support\Facades\DB;
use Session;

class weightageController extends Controller
{
     public function index()
    {
     
        $datasetweightage = weightageModel::get();
        // $option1 =DB::Table('mst_weightage_master')->select('option_value','weightage')->where('id',1)->get();
        //  $option2 =DB::Table('mst_weightage_master')->select('option_value','weightage')->where('id',2)->get();
        //    $option3 =DB::Table('mst_weightage_master')->select('option_value','weightage')->where('id',3)->get();
        //     $option4 =DB::Table('mst_weightage_master')->select('option_value','weightage')->where('id',4)->get();

        
       // return view('/Calculations/weightage',compact(array('option1','option2','option3','option4')));
        
       return view('/Calculations/weightage',compact(array('datasetweightage')));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     
      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $validatedData = $request->validate([
        'txt_weightage1' => 'required|unique:mst_weightage_master,weightage',
        'txt_weightage2' => 'required|unique:mst_weightage_master,weightage',
        'txt_weightage3' => 'required|unique:mst_weightage_master,weightage',
        'txt_weightage4' => 'required|unique:mst_weightage_master,weightage'
       ]);


        DB::beginTransaction();
        $result=1;
      
        try {
            
            $crud = weightageModel::where('option_value', '=', 'A')->firstOrFail();

            $crud->delete();  
            $crud = weightageModel::where('option_value', '=', 'B')->firstOrFail(); 
            $crud->delete();  
            $crud = weightageModel::where('option_value', '=', 'C')->firstOrFail(); 
            $crud->delete();  
            $crud = weightageModel::where('option_value', '=', 'D')->firstOrFail(); 
            $crud->delete();
 
            $store = new weightageModel();
            $store->option_value ='A';
            $store->weightage = $request->txt_weightage1;
            $store->save();   
             
            $store = new weightageModel();
            $store->option_value = 'B';
            $store->weightage = $request->txt_weightage2;
            $store->save();   
 
            $store = new weightageModel();
            $store->option_value = 'C';
            $store->weightage = $request->txt_weightage3;
            $store->save();   
 
            $store = new weightageModel();
            $store->option_value ='D';
            $store->weightage =$request->txt_weightage4;
            $store->save();   
 
            DB::commit();
            return back()->with('success', 'Data added successfully.');

        } catch (\Exception $e) {
            DB::rollback();
            return back()->with('error', 'Some error occurred while inserting data. Please try again!');
            return back()->with('error', $e->getMessage());
        }        

    }

    /**
     * Display the specified resource.
     *

     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     }

}
