<?php

namespace App\Http\Controllers\Calculations;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\gradeModel;
use Illuminate\Support\ServiceProvides  ;
use Illuminate\Support\Facades\DB;
use Session;
use Config;

class gradesController extends Controller
{
     public function index()
    {

        $datasetgrade = $dataset3 = null;
        $radio = Session::get('radio');
        $master_institute_id = Session::get('master_institute_id');

        if((!isset($radio)) || $radio == '' || $radio == 'First'){
            $master_institute_id = -1;
            $datasetgrade =  DB::connection('mysql')->table('mst_grade_master')->get();
           
        }else{
            if (isset($master_institute_id) && $master_institute_id != ''){

                Config::set("tenant_db", [
                    'driver' => 'mysql',
                    'url' => '',
                    'host' => '127.0.0.1',
                    'port' => 3306,
                    'database' => 'teaqip_tenant',
                    'username' => 'root',
                    'password' => '',
                    'unix_socket' => '',
                    'charset' => 'utf8mb4',
                    'collation' => 'utf8mb4_unicode_ci',
                    'prefix' => '',
                    'prefix_indexes' => true,
                    'strict' => true,
                    'engine' => null,
                    'options' => [],
                ]);

                $datasetgrade =  DB::connection('tenant_db')->table('mst_grade_master')->get();
                DB::disconnect('tenant_db');
               
            }
        }
        if($radio =='First'){
            session(['radio' => 'First']);
        }
        $dataset3 = DB::connection('mysql')->table('oh_master_institute')->get();

        return view('/Calculations/grades',compact(array('datasetgrade','dataset3','master_institute_id','radio'))); 
    }

    public function radiobuttongradechange(Request $request){
        echo '<br>';
        echo '<br>';
         echo $request->input('MyRadio');
       
        if($request->input('MyRadio') == 'First'){
            echo $request->input('MyRadio');
           
            return back()->with(['radio' => $request->input('MyRadio')]);      
        }
        else{
            echo $request->input('MyRadio');
          
            return back()->with(['radio' => $request->input('MyRadio')])->with(['master_institute_id' => $request->input('master_institute_dropdown')]);
        }
    }

    public function showfiltermasterinstitutedata(Request $request) {
        return back()
        ->with(['master_institute_id' => $request->input('master_institute_dropdown'),
        'radio' => $request->input('MyRadio')
       ]);
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     
      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$mst_inst_id,$radio)
    {

          $validatedData = $request->validate([
        'grade_desc' => 'required|unique:mst_grade_master,grade_desc',
        'min_score' => 'required|unique:mst_grade_master,min_score ',
        'max_score'=>'required|unique:mst_grade_master,max_score'

          ]);




        if (+$mst_inst_id != -1){
            Config::set("tenant_db", [
                'driver' => 'mysql',
                'url' => '',
                'host' => '127.0.0.1',
                'port' => 3306,
                'database' => 'teaqip_tenant',
                'username' => 'root',
                'password' => '',
                'unix_socket' => '',
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
                'options' => [],
            ]);

            $result = DB::connection('tenant_db')
            ->table('mst_grade_master')
            ->insert([
                'grade_desc' => $request->txt_name,
                'min_score' => $request->txt_min_values,
                'max_score' => $request->txt_max_values
            ]);
            DB::disconnect('tenant_db');

        }else{
            // $mst_inst_id = -1;
            $result = DB::connection('mysql')
            ->table('mst_grade_master')
            ->insert([
                'grade_desc' => $request->txt_name,
                'min_score' => $request->txt_min_values,
                'max_score' => $request->txt_max_values
            ]);

        }

        if($result == 1)
        return back()->with(['radio' => $radio,'master_institute_id' => $mst_inst_id])
        ->with('success', 'Data added successfully.');
        else 
        return back()->with(['radio' => $radio,'master_institute_id' => $mst_inst_id])
        ->with('error', 'Some error occurred while inserting data. Please try again!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,$mst_inst_id,$radio)
    {

           $validatedData = $request->validate([
        'grade_desc' => 'required|unique:mst_grade_master,grade_desc',
        'min_score' => 'required|unique:mst_grade_master,min_score ',
        'max_score'=>'required|unique:mst_grade_master,max_score'

          ]);


        
        if (+$mst_inst_id != -1){
            Config::set("tenant_db", [
                'driver' => 'mysql',
                'url' => '',
                'host' => '127.0.0.1',
                'port' => 3306,
                'database' => 'teaqip_tenant',
                'username' => 'root',
                'password' => '',
                'unix_socket' => '',
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
                'options' => [],
            ]);

            $result = DB::connection('tenant_db')
            ->table('mst_grade_master')
            ->where('id', '=', $id) 
            ->update([
                'grade_desc' => $request->get('txt_name'),
                'min_score' => $request->get('txt_min_values'),
                'max_score' => $request->get('txt_max_values')
            ]);
            DB::disconnect('tenant_db');

        }else{
             $mst_inst_id = -1;

            $result = DB::connection('mysql')
            ->table('mst_grade_master')
            ->where('id', '=', $id) 
            ->update([
                'grade_desc' => $request->get('txt_name'),
                'min_score' => $request->get('txt_min_values'),
                'max_score' => $request->get('txt_max_values')
            ]);

        }


        // $crud = gradeModel::find($id); 
        // $crud->id = $id;  
        // $crud->grade_desc =  $request->get('txt_name');  
        // $crud->min_score = $request->get('min_values'); 
        // $crud->max_score = $request->get('max_values');   
        // $result= $crud->save();

         if($result == 1)
         return back()->with(['radio' => $radio,
         'master_institute_id' => $mst_inst_id])
         ->with('success', 'Data updated successfully.');
         else 
         return back()->with(['radio' => $radio,
         'master_institute_id' => $mst_inst_id])
         ->with('error', 'Some error occurred while updating data. Please try again!');


       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $crud = gradeModel::find($id); 
        $crud->id = $id;  
        $crud->status = 0;
        $result= $crud->save();  
          if($result == 1)
             return back()->with('success', 'Data deleted successfully.');
          else 
            return back()->with('error', 'Some error occurred while deleting  data. Please try again!');
          
     }
      public function enable(Request $request, $id,$mst_inst_id,$radio)
    {
        if (+$mst_inst_id != -1){
            Config::set("tenant_db", [
                'driver' => 'mysql',
                'url' => '',
                'host' => '127.0.0.1',
                'port' => 3306,
                'database' => 'teaqip_tenant',
                'username' => 'root',
                'password' => '',
                'unix_socket' => '',
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
                'options' => [],
            ]);

            $result = DB::connection('tenant_db')
            ->table('mst_grade_master')
            ->where('id', '=', $id) 
            ->update([
                'status' => 1
              
            ]);
            DB::disconnect('tenant_db');
        }else{
            $mst_inst_id = -1;
            $result = DB::connection('mysql')
            ->table('mst_grade_master')
            ->where('id', '=', $id) 
            ->update([
                'status' => 1
            ]);

        }



        // $result = gradeModel::where('id', $id)
        //         ->update(['status' => 1]);

         if($result == 1)
            return back()->with(['radio' => $radio,'master_institute_id' => $mst_inst_id])
            ->with('success', 'Record Enabled successfully.');
         else 
            return back()->with(['radio' => $radio,'master_institute_id' => $mst_inst_id])
            ->with('error', 'Some error occurred while updating data. Please try again!');


    }

    /**
     * Disables the specified resource in Database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @param  int  $int_type_id
     * @return \Illuminate\Http\Response
     */
    public function disable(Request $request, $id,$mst_inst_id,$radio)
    {

        if (+$mst_inst_id != -1){
            Config::set("tenant_db", [
                'driver' => 'mysql',
                'url' => '',
                'host' => '127.0.0.1',
                'port' => 3306,
                'database' => 'teaqip_tenant',
                'username' => 'root',
                'password' => '',
                'unix_socket' => '',
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
                'options' => [],
            ]);
            $result = DB::connection('tenant_db')
            ->table('mst_grade_master')
            ->where('id', '=', $id) 
            ->update([
                'status' => 0
            ]);
            DB::disconnect('tenant_db');
        }else{
            $mst_inst_id = -1;
            $result = DB::connection('mysql')
            ->table('mst_grade_master')
            ->where('id', '=', $id) 
            ->update([
                'status' => 0
            ]);
        }
        // $result = gradeModel::where('id', $id)
        //         ->update(['status' => 0]);

        if($result == 1)
            return back()->with(['radio' => $radio,'master_institute_id' => $mst_inst_id])
            ->with('success', 'Record Disabled successfully.');
        else 
            return back()->with(['radio' => $radio,'master_institute_id' => $mst_inst_id])
            ->with('error', 'Some error occurred while updating data. Please try again!');
            
    }


}
