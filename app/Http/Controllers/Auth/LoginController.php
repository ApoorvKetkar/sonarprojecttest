<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use App\Models\loginModel;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
  
     public function CheckUser(Request $request)
    {
          $model = loginModel::where('email', $request->username)->first();
          if($model==[]){
              return back()->with('login-error', 'Sorry! No user found.');
        }
        else{
            if($request->password == $model->password){
                return redirect('/dashboard');
            }else{
                return back()->with('login-error', 'You have entered incorrect login credentials.');
            }
        }
    }
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
}
