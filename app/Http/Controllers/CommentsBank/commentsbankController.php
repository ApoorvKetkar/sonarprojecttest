<?php

namespace App\Http\Controllers\CommentsBank;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\commentsbankModel;
use APP\Models\commentrelatedModel;
use Illuminate\Support\ServiceProvides  ;
use Illuminate\Support\Facades\DB;
use Session;


class commentsbankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $show_comments = Session::get('show_comments');
        $inst_type_id = Session::get('inst_type_id');
        $parameter_id = Session::get('parameter_id');
        $related_parameter_id = Session::get('related_parameter_id');
        $insttype_dataset = DB::table('mst_inst_type_master') ->get();

        if (isset($inst_type_id) && $inst_type_id!='' && $inst_type_id!=-1) {
         $parameter_dataset = DB::table('mst_parameter_master')->where('inst_type_id','=',$inst_type_id)->get();
         $grade_dataset =DB::table('mst_grade_master')->get();

         if (isset($parameter_id) && $parameter_id !='' && $parameter_id != -1) { 
                $commentsbank_dataset = DB::table('mst_grade_master') 
                ->leftJoin("mst_comment_master",function($join){
                $join->on('mst_comment_master.grade_id','=','mst_grade_master.id')
                ->where('mst_comment_master.parameter_id','=',Session::get('parameter_id') )
                ->orWhere('mst_comment_master.parameter_id','=',null );
            })                    
            ->select('mst_comment_master.*','mst_grade_master.id as grade_id','mst_grade_master.grade_desc')
            ->orderby('mst_grade_master.id')
            ->get();

            $relatedparameter_dataset = DB::table('mst_comment_related_master')
            ->select('mst_comment_related_master.related_parameter_id','mst_parameter_master.parameter_name')
            ->distinct('mst_comment_related_master.related_parameter_id')
            ->join('mst_parameter_master','mst_comment_related_master.related_parameter_id','=','mst_parameter_master.id')
            ->where('mst_comment_related_master.main_parameter_id','=', Session::get('parameter_id'))
            ->get();

            $new_related_parameters = DB::select('SELECT * FROM mst_parameter_master where id not in (
    SELECT related_parameter_id FROM mst_comment_related_master where main_parameter_id=' . Session::get('parameter_id') .') and inst_type_id=' . $inst_type_id);


            if (count( $relatedparameter_dataset)>0){
                if (!isset($related_parameter_id) || $related_parameter_id == ''){
                    $related_parameter_id=$relatedparameter_dataset[0]->related_parameter_id;                     
                }
                define("rel_param_id",$related_parameter_id);
                define("main_param_id",$parameter_id);
                $relatedparameter_comments = DB::table('mst_grade_master') 
                ->leftJoin("mst_comment_related_master",function($join){
                    $join->on('mst_comment_related_master.related_grade_id','=','mst_grade_master.id')
                    ->where('mst_comment_related_master.main_parameter_id','=',main_param_id )
                    ->where('mst_comment_related_master.related_parameter_id','=',rel_param_id )
                    ->orWhere('mst_comment_related_master.related_parameter_id','=',null );
                })                    
                ->select('mst_comment_related_master.*','mst_grade_master.id as grade_id','mst_grade_master.grade_desc as grade') 
                ->orderby('mst_grade_master.id')
                ->get();



            }else{
                $relatedparameter_comments = [];
            } 
     }   

     else{
     
           $commentsbank_dataset = DB::table('mst_grade_master') 
                ->leftJoin("mst_comment_master",function($join){
                $join->on('mst_comment_master.grade_id','=','mst_grade_master.id')
                ->where('mst_comment_master.parameter_id','=',Session::get('parameter_id') )
                ->orWhere('mst_comment_master.parameter_id','=',null );
            })                    
            ->select('mst_comment_master.*','mst_grade_master.id as grade_id','mst_grade_master.grade_desc')
            ->orderby('mst_grade_master.id')
            ->get();

        $relatedparameter_dataset =[];
        $relatedparameter_comments=[];
        $new_related_parameters=[];
    }

}else{
    $parameter_dataset = [];
    // $commentsbank_dataset = [];
    $relatedparameter_dataset =[];
    $relatedparameter_comments=[];
    $new_related_parameters=[];
    $grade_dataset=[];

     $commentsbank_dataset = DB::table('mst_grade_master') 
                ->leftJoin("mst_comment_master",function($join){
                $join->on('mst_comment_master.grade_id','=','mst_grade_master.id')
                ->where('mst_comment_master.parameter_id','=',Session::get('parameter_id') )
                ->orWhere('mst_comment_master.parameter_id','=',null );
            })                    
            ->select('mst_comment_master.*','mst_grade_master.id as grade_id','mst_grade_master.grade_desc')
            ->orderby('mst_grade_master.id')
            ->get();

}

session(['show_comments' => '0']);
return view('CommentBank\commentsbank', compact(array('parameter_id','inst_type_id','insttype_dataset','parameter_dataset','commentsbank_dataset','relatedparameter_dataset','grade_dataset','relatedparameter_comments','related_parameter_id','show_comments','new_related_parameters')));
}


public function getParameters(Request $request) {
    return back()->with(['inst_type_id' => $request->input('dropdown_institute_types')]);
}

public function filtercommentsbank(Request $request, $int_type_id){
    session(['show_comments' => '1']);
    return back()->with(['inst_type_id' => $int_type_id,'parameter_id' => $request->input('dropdown_parameters')]);
} 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
public function createnewdropdown()
{

    
} 

public function getRelatedParameterComments(Request $request, $inst_type_id, $parameter_id, $rel_param_id,
    $show_comments){         
    return back()->with(['inst_type_id' => $inst_type_id,'parameter_id' => $parameter_id,'related_parameter_id'=> $rel_param_id, 'show_comments' => $show_comments]);
} 


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$inst_type_id)
    {
    
        $show_comments = Session::get('show_comments');
        $parameter_id =$request->input('_parameterid');
       
        
        $comments_count =$request->_count ;
        DB::beginTransaction();
        $result=1;
        try {

            for($i=0 ; $i<$comments_count ; $i++){

                $grade_id = $i+1;
                $id = null;
                if (+$request->input('_id'.$i) > 0){
                    $id = +$request->input('_id'.$i);
                }


                if ($id == null || +$id == 0){
                    $commentsbank_dataset = DB::table('mst_comment_master')->insert(
                        ['parameter_id'=>+$request->input('_parameterid'),
                        'grade_id' => $grade_id,
                        'comment_desc' => $request->get('txt_desc'.$i)
                    ]);

                }else{

                    $commentsbank_dataset = DB::table('mst_comment_master')
                    ->where("id","=",$id)
                    ->update(
                        ['parameter_id'=>+$request->input('_parameterid'),

                        'grade_id' => $grade_id,
                        'comment_desc' => $request->get('txt_desc'.$i)
                    ]);

                }


            }
            session(['show_comments' => '0']);

            DB::commit();

              // return back()->with(['inst_type_id' => $inst_type_id,'parameter_id' => $parameter_id,'commentsbank_dataset'=>$commentsbank_dataset])
           
             // return back()->with(['inst_type_id' => $inst_type_id,'parameter_id' => $parameter_id,'show_comments'=>$show_comments])
             //         ->withInput($request->input());
             // return redirect()->action('App\Http\Controllers\CommentsBank\commentsbankController@filterCommentsbank',[1] );
            return redirect()->back()->with(['inst_type_id' => $inst_type_id,'parameter_id' => $parameter_id,'show_comments'=>$show_comments])
                ->withInput($request->input());

           
                                                    
            
        } catch (\Exception $e) {
            DB::rollback();
            // echo $e;
            //return back()->with('error', 'Error Saving Data');
        }        


    }

    public function relatedParameterstore(Request $request)
    {
       
        $parameter_id = +$request->input('_parameterid');
        $related_parameter_id = +$request->input('_relatedparameterid'); 
        $total_grades = +$request->input('_totalgrades') ;
    
         DB::beginTransaction();
         $result=1;

        try {

            for($i=0 ; $i<$total_grades ; $i++){


                     $result = DB::table('mst_comment_related_master')->where(['main_parameter_id'=> $parameter_id,
                           'related_parameter_id'=> $related_parameter_id,
                            'related_grade_id' => +$request->input('_grade_id'.$i)]);

                     if ($result->count() > 0){
                        $response = DB::table('mst_comment_related_master')
                        ->where(['main_parameter_id'=> $parameter_id,
                           'related_parameter_id'=> $related_parameter_id,
                            'related_grade_id' => +$request->input('_grade_id'.$i)])
                        ->update( ['comment_desc' => $request->get('txt_desc'.$i)]);
                     }else{
                         $response = DB::table('mst_comment_related_master')->insert(
                         ['main_parameter_id'=> $parameter_id,
                               'related_parameter_id'=> $related_parameter_id,
                                'related_grade_id' => +$request->input('_grade_id'.$i),
                                'comment_desc' => $request->get('txt_desc'.$i)
                         ]);

                     }


            }

            DB::commit();
            return back()->with('success', 'Data added successfully.');

        }catch (\Exception $e) {
            DB::rollback();
            // echo $e;
            // echo $e->getMessage();
            // echo DB::getQueryLog();
            return back()->with('error', $e->getMessage());
        }        





   }
   public function insertrelatedParameters(Request $request){
   
    echo $parameter_id = +$request->input('_parameterid');
     echo $related_parameter_id = $request->input('dropdown_new_related_parameter'); 
     echo $total_grades = +$request->input('_count') ;
     // echo $grade = +$request->input('_grade_id');
     DB::beginTransaction();
    $result=1;
       try {

            for($i=0 ; $i<$total_grades ; $i++){

                 $response = DB::table('mst_comment_related_master')->insert(
                 ['main_parameter_id'=> $parameter_id,
                       'related_parameter_id'=> $related_parameter_id,
                        'related_grade_id' => +$request->input('_grade_id'.$i),
                        'comment_desc' => $request->get('txt_desc'.$i)
                 ]);

            }
            DB::commit();
            return back()->with('success', 'Data added successfully.');

       }catch (\Exception $e) {
            DB::rollback();
            // echo $e;
            // echo $e->getMessage();
             
            return back()->with('error', $e->getMessage());
        }        





   }



/*    public function relatedParameterstore(Request $request)
    {
       
        $parameter_id= +$request->input('_parameterid');
        $related_parameter_id=+$request->input('_relatedparameterid'); 
        $comments_count = $request->_count ;
    
         DB::beginTransaction();
         $result=1;

        // $id=+$request->input('_parameterid');
          try {

            for($i=0 ; $i<$comments_count ; $i++){

               $grade_id = $i+1;
               $related_parameter_id = null;

                if (+$request->input('_id'.$i) > 0){
                    $id = +$request->input('_id'.$i);
                   
                }
                echo $id;
               


                 if($id == null || +$id == 0){
                     $response = DB::table('mst_comment_related_master')->insert(
                     ['main_parameter_id'=>+$request->input('_parameterid'),
                           'related_parameter_id'=>+$request->input('_relatedparameterid'),
                            'related_grade_id' => $grade_id,
                            'comment_desc' => $request->get('txt_desc'.$i)
                     ]);

                 }else{

                    $response = DB::table('mst_comment_related_master')
                    ->where(['main_parameter_id'=> $parameter_id,
                             'related_parameter_id'=> $related_parameter_id,
                             'related_grade_id'=>$id])
                     ->update(
                           ['main_parameter_id'=>+$request->input('_parameterid'),
                            'related_parameter_id'=>+$request->input('_relatedparameterid'),
                            'related_grade_id' => $grade_id,
                          'comment_desc' => $request->get('txt_desc'.$i)]);

                     echo $response;

                    //print_r($response);
                }


            }

             DB::commit();
             // return back()->with('success', 'Data added successfully.');

        }catch (\Exception $e) {
            DB::rollback();
            // echo $e;
            // return back()->with('error', 'Error Saving Data');
        }        





   }
*/



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
