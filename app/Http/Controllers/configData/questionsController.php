<?php

namespace App\Http\Controllers\configData;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\questionsModel;
use Illuminate\Support\ServiceProvides  ;
use Illuminate\Support\Facades\DB;
use Session;
use Config;

class questionsController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$datasetquestions = questionsModel::get();
        $dataset3 = $dataset2 = $parameter_dataset = $master_institute_id = null;
        $radio = Session::get('radio');
        $master_institute_id = Session::get('master_institute_id');

       
        $inst_type_id = Session::get('inst_type_id'); 
        $parameter_id = Session::get('parameter_id'); 
        $dataset3 = DB::connection('mysql')->table('oh_master_institute') -> get();
        error_log("$inst_type_id : " . $inst_type_id);
        error_log("$parameter_id : " . $parameter_id);
        $dataset2 = DB::connection('mysql')->table('mst_inst_type_master') ->get();
        if((!isset($radio)) || $radio == '' || $radio == 'First'){
            // $datasetFilterData = DB::connection('mysql')->table('mst_parameter_master')->where('inst_type_id', '=', $inst_type_id) ->get();
            $master_institute_id = -1;
          
            if (isset($inst_type_id) && $inst_type_id!='' && $inst_type_id!=-1){
                $parameter_dataset = DB::connection('mysql')->table('mst_parameter_master')->where('inst_type_id','=',$inst_type_id)
                    ->get();
                    // echo $parameter_dataset;
                    
                if (isset($parameter_id) && $parameter_id!='' && $parameter_id!=-1){
                    $questions_dataset = DB::connection('mysql')->table('mst_question_master') 
                    -> select('mst_question_master.*','mst_parameter_master.parameter_name as parameter_name')
                    ->join('mst_parameter_master', 'mst_parameter_master.id', '=', 'mst_question_master.parameter_id')
                    ->where('parameter_id','=',$parameter_id)
                    ->get();
                    // $questions_dataset =DB::table('mst_question_master')
                    //                    ->join('mst_parameter_master','mst_question_master.parameter_id','=','mst_parameter_master.id')
                    //                    ->get();
    
                }else{
                    $questions_dataset = DB::connection('mysql')->table('mst_question_master')->whereRaw(" parameter_id in (select id from mst_parameter_master where inst_type_id = " . $inst_type_id .")") 
                    -> select('mst_question_master.*','mst_parameter_master.parameter_name as parameter_name')
                    ->join('mst_parameter_master', 'mst_question_master.parameter_id', '=', 'mst_parameter_master.id')
                    -> get();
                }
    
            }else{
                 $parameter_dataset = [];
           $questions_dataset = [];
    
            }
    
        }else{
            if (isset($master_institute_id) && $master_institute_id != ''){

                Config::set("tenant_db", [
                    'driver' => 'mysql',
                    'url' => '',
                    'host' => '127.0.0.1',
                    'port' => 3306,
                    'database' => 'teaqip_tenant',
                    'username' => 'root',
                    'password' => '',
                    'unix_socket' => '',
                    'charset' => 'utf8mb4',
                    'collation' => 'utf8mb4_unicode_ci',
                    'prefix' => '',
                    'prefix_indexes' => true,
                    'strict' => true,
                    'engine' => null,
                    'options' => [],
                ]);
                // echo $radio;

                // $datasetFilterData = DB::connection('tenant_db')->table('mst_parameter_master')->where('inst_type_id', '=', $inst_type_id) ->get();
                // $insttype_dataset = DB::table('mst_inst_type_master') ->get();
            if (isset($inst_type_id) && $inst_type_id!='' && $inst_type_id!=-1){
                $parameter_dataset = DB::connection('tenant_db')->table('mst_parameter_master')->where('inst_type_id','=',$inst_type_id)
                    ->get();
                    // echo $parameter_dataset;
                    // echo $parameter_id;
                if (isset($parameter_id) && $parameter_id!='' && $parameter_id!=-1){
                    $questions_dataset = DB::connection('tenant_db')->table('mst_question_master') 
                    -> select('mst_question_master.*','mst_parameter_master.parameter_name as parameter_name')
                    ->join('mst_parameter_master', 'mst_parameter_master.id', '=', 'mst_question_master.parameter_id')
                    ->where('parameter_id','=',$parameter_id)
                    ->get();

                    // echo $questions_dataset;
                    // $questions_dataset =DB::table('mst_question_master')
                    //                    ->join('mst_parameter_master','mst_question_master.parameter_id','=','mst_parameter_master.id')
                    //                    ->get();
    
                }else{
                    $questions_dataset = DB::connection('tenant_db')->table('mst_question_master')->whereRaw(" parameter_id in (select id from mst_parameter_master where inst_type_id = " . $inst_type_id .")") 
                    -> select('mst_question_master.*','mst_parameter_master.parameter_name as parameter_name')
                    ->join('mst_parameter_master', 'mst_question_master.parameter_id', '=', 'mst_parameter_master.id')
                    -> get();
                }
    
            }else{
                $parameter_dataset = [];
                $questions_dataset = [];
    
            }
                DB::disconnect('tenant_db');
               
            }
        }   
       
         return view('masterquestions.index',compact(array('parameter_id','inst_type_id','radio','master_institute_id',
         'dataset3','dataset2','parameter_dataset','questions_dataset')));

    }


    public function radiobuttonquestionschange(Request $request){
        // echo '<br>';
        // echo '<br>';
        //  echo $request->input('MyRadio');
       
        if($request->input('MyRadio') == 'First'){
            return back()->with(['radio' => $request->input('MyRadio'),'inst_type_id' => +$request->input('dropdown_institute_types')]);      
        }
        else{
            return back()->with(['radio' => $request->input('MyRadio')])->with(['master_institute_id' => +$request->input('master_institute_dropdown')]);
        }
    }

    public function showmasterinstitutefilterdata(Request $request) {
        $dataset3 = DB::connection('mysql')->table('oh_master_institute') -> get();
        return back()
        ->with(['master_institute_id' => $request->input('master_institute_dropdown'),
        'radio' => $request->input('MyRadio'),
        'inst_type_id' => $request->input('dropdown_institute_types'),'dataset3']);
   }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function create($inst_type_id,$master_institute_id,$radio)
     { 
        if (+$master_institute_id != -1){
            Config::set("tenant_db", [
                'driver' => 'mysql',
                'url' => '',
                'host' => '127.0.0.1',
                'port' => 3306,
                'database' => 'teaqip_tenant',
                'username' => 'root',
                'password' => '',
                'unix_socket' => '',
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
                'options' => [],
            ]);
          
            $parameter_dataset = DB::connection('tenant_db')->table('mst_parameter_master')->where('inst_type_id','=',$inst_type_id)
            ->get();
            DB::disconnect('tenant_db');

        }else{
            $master_institute_id = -1;
            $parameter_dataset = DB::connection('mysql')->table('mst_parameter_master')->where('inst_type_id','=',$inst_type_id)
            ->get();

         }
        $dataset2 = DB::connection('mysql')->table('mst_inst_type_master') -> get();
     return view('masterquestions.createque', compact(array('dataset2','inst_type_id',
        'parameter_dataset','master_institute_id','radio')))
        ->with(['inst_type_id' => $inst_type_id,'master_institute_id' => $master_institute_id,'radio' => $radio]);
   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$inst_type_id,$master_institute_id,$radio)
    {
        $parameter_id = Session::get('parameter_id');
        if (+$master_institute_id != -1){
            Config::set("tenant_db", [
                'driver' => 'mysql',
                'url' => '',
                'host' => '127.0.0.1',
                'port' => 3306,
                'database' => 'teaqip_tenant',
                'username' => 'root',
                'password' => '',
                'unix_socket' => '',
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
                'options' => [],
            ]);
            $parameter_dataset = DB::connection('mysql')->table('mst_parameter_master')->where('inst_type_id','=',$inst_type_id)
            ->get();
            $result = DB::connection('tenant_db')
            ->table('mst_question_master')
            ->insert([
                'questions' => $request->txt_questions,
                'option_1' => $request->text_option1,
                'option_2' => $request->text_option2,
                'option_3' => $request->text_option3,
                'option_4' => $request->text_option4,
                'parameter_id' => $request->input('dropdown_parameters'),
               
            ]);
            DB::disconnect('tenant_db');

        }else{
            $parameter_dataset = DB::connection('mysql')->table('mst_parameter_master')->where('inst_type_id','=',$inst_type_id)
            ->get();
            $master_institute_id = -1;
            $result = DB::connection('mysql')
            ->table('mst_question_master')
            ->insert([
                'questions' => $request->txt_questions,
                'option_1' => $request->text_option1,
                'option_2' => $request->text_option2,
                'option_3' => $request->text_option3,
                'option_4' => $request->text_option4,
                'parameter_id' => $request->input('dropdown_parameters'),
                
            ]);

         }
         $dataset3 = DB::connection('mysql')->table('oh_master_institute') -> get();
       
         $dataset2 = DB::connection('mysql')->table('mst_inst_type_master') -> get();
        if($result == 1)

       return redirect('masterquestions.index')->with(compact(array('dataset2','dataset3','parameter_dataset','inst_type_id',
        'radio','master_institute_id','parameter_id')))
        ->with(['inst_type_id' => $inst_type_id,'radio' => $radio,'master_institute_id' => $master_institute_id])
        ->with('success', 'Data inserted successfully.');
          
        else 
        return view('masterquestions.index', compact(array('dataset2','dataset3','parameter_dataset','inst_type_id','radio'
        ,'master_institute_id','parameter_id')))
        ->with(['inst_type_id' => $inst_type_id,'radio' => $radio,'master_institute_id' => $master_institute_id])
            ->with('error', 'Some error occurred while updating data. Please try again!');
   
   
    }

    public function tempMethod() {
      echo "in temp method";
    }

     public function getParameters(Request $request,$master_institute_id,$radio) {
        return back()->with(['inst_type_id' => $request->input('dropdown_institute_types'),
        'radio' => $radio,'master_institute_id' => $master_institute_id,'parameter_id' => $request->input('dropdown_parameters')]);
    }

    public function filterQuestions(Request $request ,$int_type_id,$master_institute_id,$radio){
        return back()->with(['inst_type_id' => $int_type_id,'parameter_id' => $request->input('dropdown_parameters'),
        'radio' => $radio,'master_institute_id' => $master_institute_id]);
    } 

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,$inst_type_id,$mst_inst_id,$radio)
    {
        $master_institute_id = Session::get('master_institute_id');
        if (+$mst_inst_id != -1){
            Config::set("tenant_db", [
                'driver' => 'mysql',
                'url' => '',
                'host' => '127.0.0.1',
                'port' => 3306,
                'database' => 'teaqip_tenant',
                'username' => 'root',
                'password' => '',
                'unix_socket' => '',
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
                'options' => [],
            ]);
            $parameter_id = Session::get('parameter_id');
            $dataset2 = DB::connection('tenant_db')->table('mst_inst_type_master')-> get();
            $parameter_dataset =DB::connection('tenant_db')->table('mst_parameter_master')->where('inst_type_id','=',$inst_type_id)
            ->get();
    
            $questions_dataset = DB::connection('tenant_db')->table('mst_question_master')
                -> select('mst_question_master.*','mst_parameter_master.parameter_name as parameter_name')
                ->join('mst_parameter_master', 'mst_parameter_master.id', '=', 'parameter_id')
                ->where('mst_question_master.id','=',$id)
                -> get();
            DB::disconnect('tenant_db');

        }else{
            $mst_inst_id = -1;
            $parameter_id = Session::get('parameter_id');
            $dataset2 = DB::connection('mysql')->table('mst_inst_type_master')-> get();
            $parameter_dataset =DB::table('mst_parameter_master')->where('inst_type_id','=',$inst_type_id)
            ->get();
    
            $questions_dataset =DB::connection('mysql')->table('mst_question_master')
                -> select('mst_question_master.*','mst_parameter_master.parameter_name as parameter_name')
                ->join('mst_parameter_master', 'mst_parameter_master.id', '=', 'parameter_id')
                ->where('mst_question_master.id','=',$id)
                -> get();
        }

       
       return view('masterquestions.show',compact(array('inst_type_id','parameter_id','dataset2','parameter_dataset',
       'questions_dataset','mst_inst_id','radio')))
       ->with(['radio' => $radio,'master_institute_id' => $mst_inst_id]); 

    }

   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$inst_type_id,$master_institute_id,$radio)
{
     $parameter_id = Session::get('parameter_id'); 
    if (+$master_institute_id != -1){
        Config::set("tenant_db", [
            'driver' => 'mysql',
            'url' => '',
            'host' => '127.0.0.1',
            'port' => 3306,
            'database' => 'teaqip_tenant',
            'username' => 'root',
            'password' => '',
            'unix_socket' => '',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => [],
        ]);
      
        $questions_dataset =  DB::connection('tenant_db')->table('mst_question_master')
        -> select('mst_question_master.*','mst_parameter_master.parameter_name as parameter_name')
        ->join('mst_parameter_master', 'mst_parameter_master.id', '=', 'parameter_id')
        ->where('mst_question_master.id','=',$id)
        -> get();  
        $parameter_dataset = DB::connection('tenant_db')->table('mst_parameter_master')->where('inst_type_id','=',$inst_type_id)
        ->get();

         $dataset3 = DB::connection('mysql')->table('oh_master_institute') -> get();
       
        DB::disconnect('tenant_db');

    }else{
        $master_institute_id = -1;
        $parameter_dataset = DB::connection('mysql')->table('mst_parameter_master')->where('inst_type_id','=',$inst_type_id)
        ->get();
         $dataset3 = DB::connection('mysql')->table('oh_master_institute') -> get();
       
        $questions_dataset = DB::connection('mysql')->table('mst_question_master')
        -> select('mst_question_master.*','mst_parameter_master.parameter_name as parameter_name')
        ->join('mst_parameter_master', 'mst_parameter_master.id', '=', 'parameter_id')
        ->where('mst_question_master.id','=',$id)
        -> get();  

     }

         $parameter_id = Session::get('parameter_id');
         $dataset2 = DB::table('mst_inst_type_master')-> get();
      
     return view('masterquestions.edit', compact(array('dataset2','inst_type_id','parameter_id','questions_dataset',
     'parameter_dataset','master_institute_id','radio','dataset3')))
     ->with(['master_institute_id' => $master_institute_id,'radio' => $radio,'inst_type_id' => $inst_type_id]);
        }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id,$inst_type_id,$master_institute_id,$radio)
     {
        $parameter_id = Session::get('parameter_id');
        if (+$master_institute_id != -1 && +$master_institute_id != null){
            Config::set("tenant_db", [
                'driver' => 'mysql',
                'url' => '',
                'host' => '127.0.0.1',
                'port' => 3306,
                'database' => 'teaqip_tenant',
                'username' => 'root',
                'password' => '',
                'unix_socket' => '',
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
                'options' => [],
            ]);

            $result = DB::connection('tenant_db')
            ->table('mst_question_master')
            ->where('id', '=', $id) 
            ->update([
                'parameter_id' => $request->input('dropdown_parameters'),
                'questions' => $request->get('txt_question_desc'),
                'option_1' => $request->get('text_option1'),
                'option_2' => $request->get('text_option2'),
                'option_3' => $request->get('text_option3'),
                'option_4' => $request->get('text_option4'),
            ]);

            $questions_dataset = DB::connection('tenant_db')->table('mst_question_master') 
            -> select('mst_question_master.*','mst_parameter_master.parameter_name as parameter_name')
            ->join('mst_parameter_master', 'mst_parameter_master.id', '=', 'mst_question_master.parameter_id')
            ->where('parameter_id','=',$parameter_id)
            ->get();
             $dataset3 = DB::connection('mysql')->table('oh_master_institute') -> get();
       
            DB::disconnect('tenant_db');


        }else{

            $master_institute_id = -1;
            $result = DB::connection('mysql')
            ->table('mst_question_master')
            ->where('id', '=', $id) 
            ->update([
                'parameter_id' => $request->input('dropdown_parameters'),
                'questions' => $request->get('txt_question_desc'),
                'option_1' => $request->get('text_option1'),
                'option_2' => $request->get('text_option2'),
                'option_3' => $request->get('text_option3'),
                'option_4' => $request->get('text_option4'),
            ]);
            $questions_dataset = DB::connection('mysql')->table('mst_question_master') 
            -> select('mst_question_master.*','mst_parameter_master.parameter_name as parameter_name')
            ->join('mst_parameter_master', 'mst_parameter_master.id', '=', 'mst_question_master.parameter_id')
            ->where('parameter_id','=',$parameter_id)
            ->get();

        }
       
        $dataset2 = DB::table('mst_inst_type_master')-> get();
        $dataset3 = DB::connection('mysql')->table('oh_master_institute') -> get();
        $parameter_dataset =DB::connection('mysql')->table('mst_parameter_master')->where('inst_type_id','=',$inst_type_id)
        ->get();

        if($result == 1)
         return view('masterquestions.index', compact(array('dataset2','inst_type_id','parameter_id','questions_dataset',
         'parameter_dataset','master_institute_id','radio','dataset3')))
         ->with(['master_institute_id' => $master_institute_id,'inst_type_id' => $inst_type_id,'radio' => $radio])
         ->with('success', 'Data Inserted Successfully.');
      else
         return back()->with(['int_type_id' => $inst_type_id,'radio' => $radio,
         'master_institute_id' => $master_institute_id])
         ->with('error', 'Some Error occurred while updating data. Please try again!');
        
    }
     public function enable(Request $request, $id ,$int_type_id,$mst_inst_id,$radio)
    {
        if (+$mst_inst_id != -1){
            Config::set("tenant_db", [
                'driver' => 'mysql',
                'url' => '',
                'host' => '127.0.0.1',
                'port' => 3306,
                'database' => 'teaqip_tenant',
                'username' => 'root',
                'password' => '',
                'unix_socket' => '',
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
                'options' => [],
            ]);
            $result = DB::connection('tenant_db')
            ->table('mst_question_master')
            ->where('id', '=', $id) 
            ->update([
                'status' => 1
              
            ]);
            DB::disconnect('tenant_db');
        }else{
            $mst_inst_id = -1;

            $result = DB::connection('mysql')
            ->table('mst_question_master')
            ->where('id', '=', $id) 
            ->update([
                'status' => 1
              
            ]);
            // $result = questionsModel::where('id', $id)
            // ->update(['status' => 1]);
            // echo"$result";
        }
        // echo"$id";
       

         if($result == 1)
            return back()->with(['int_type_id' => $int_type_id,'radio' => $radio,
            'master_institute_id' => $mst_inst_id])
                         ->with('success', 'Record Enabled successfully.');
         else 
            return back()->with(['int_type_id' => $int_type_id,'radio' => $radio,
            'master_institute_id' => $mst_inst_id])
                         ->with('error', 'Some error occurred while updating data. Please try again!');


    }
      public function disable(Request $request, $id ,$int_type_id,$mst_inst_id,$radio)
    {

        if (+$mst_inst_id != -1){
            Config::set("tenant_db", [
                'driver' => 'mysql',
                'url' => '',
                'host' => '127.0.0.1',
                'port' => 3306,
                'database' => 'teaqip_tenant',
                'username' => 'root',
                'password' => '',
                'unix_socket' => '',
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
                'options' => [],
            ]);
            $result = DB::connection('tenant_db')
            ->table('mst_question_master')
            ->where('id', '=', $id) 
            ->update([
                'status' => 0
              
            ]);
            DB::disconnect('tenant_db');
        }else{
            $mst_inst_id = -1;

            $result = DB::connection('mysql')
            ->table('mst_question_master')
            ->where('id', '=', $id) 
            ->update([
                'status' => 0
              
            ]);
           
        }

        // echo"$id";
        // $result =questionsModel::where('id', $id)
        //         ->update(['status' => 0]);
        //  echo"$result";
        if($result == 1)
            return back()->with(['int_type_id' => $int_type_id,'radio' => $radio,
            'master_institute_id' => $mst_inst_id])
                         ->with('success', 'Record Disabled successfully.');
        else 
            return back()->with(['int_type_id' => $int_type_id,'radio' => $radio,
            'master_institute_id' => $mst_inst_id])
                         ->with('error', 'Some error occurred while updating data. Please try again!');
            
     }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $crud = questionsModel::find($id); 
        $crud->id = $id;  
        $crud->status = 0;
        $result= $crud->save();  
          if($result == 1)
             return back()->with('success', 'Data deleted successfully.');
          else 
            return back()->with('error', 'Some error occurred while deleting  data. Please try again!');
    
    }
}
