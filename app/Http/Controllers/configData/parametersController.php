<?php

namespace App\Http\Controllers\configData;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\parametersModel;
use App\Models\instituteTypesModel;
use App\Models\instituteModel;

use Illuminate\Support\ServiceProvides  ;
use Illuminate\Support\Facades\DB;
use Session;
use Config;
//use Illuminate\Support\Facades\Input::class;

class parametersController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $dataset2 = $dataset3 = $datasetFilterData = $datasetFilterData1 = null;
        $inst_type_id = Session::get('inst_id'); 
        $radio = Session::get('radio');
        $master_institute_id = Session::get('master_institute_id');
        // $inst_type_id_master = Session::get('inst_type_id');
        
        
        
        if((!isset($radio)) || $radio == '' || $radio == 'First'){
            $datasetFilterData = DB::connection('mysql')->table('mst_parameter_master')->where('inst_type_id', '=', $inst_type_id) ->get();
            $master_institute_id = -1;
            $dataset_master_institue_institute=[];
        }else{
            if (true){

                Config::set("tenant_db", [
                    'driver' => 'mysql',
                    'url' => '',
                    'host' => '127.0.0.1',
                    'port' => 3306,
                    'database' => 'teaqip_tenant',
                    'username' => 'root',
                    'password' => 'root',
                    'unix_socket' => '',
                    'charset' => 'utf8mb4',
                    'collation' => 'utf8mb4_unicode_ci',
                    'prefix' => '',
                    'prefix_indexes' => true,
                    'strict' => true,
                    'engine' => null,
                    'options' => [],
                ]);

              
            $dataset_master_institue_institute=DB::connection('mysql')->table('oh_master_institute')
                        ->join('mst_inst_type_master','mst_inst_type_master.id', '=','oh_master_institute.inst_type_id')
                       ->select(['oh_master_institute.inst_type_id','mst_inst_type_master.inst_type_name'])
                       ->where('oh_master_institute.inst_type_id','=',Session::get('master_institute_id'))
                        ->get();
                       
            // if (count($dataset_master_institue_institute)>0){
            //     $inst_type_id = $dataset_master_institue_institute[0]->inst_type_id;
                $datasetFilterData = DB::connection('tenant_db')->table('mst_parameter_master')
                                          ->where('inst_type_id', '=', $inst_type_id) 
                                          ->get();         
            // }            
            DB::disconnect('tenant_db');
               
                        
               
            }
        }
          $dataset_master_institue_institute=DB::connection('mysql')->table('oh_master_institute')
                        ->join('mst_inst_type_master','mst_inst_type_master.id', '=','oh_master_institute.inst_type_id')
                       ->select(['oh_master_institute.inst_type_id','mst_inst_type_master.inst_type_name'])
                       ->where('oh_master_institute.inst_type_id','=',Session::get('master_institute_id'))
                        ->get();   
          
        //      if($radio =='First'){
        //     session(['radio' => 'First']);
        // }else{
        //       session(['radio' => 'Second']);
        // }   

        $dataset3 = DB::connection('mysql')->table('oh_master_institute') -> get();
        $dataset2 = DB::connection('mysql')->table('mst_inst_type_master') -> get();
        $datasetFilterData1 = DB::connection('mysql')->table('oh_master_institute')->where('id', '=', $master_institute_id) ->get();
 
        return view('/configData/parameters', compact(array('dataset2','datasetFilterData','datasetFilterData1','inst_type_id','radio','dataset3','master_institute_id','dataset_master_institue_institute')));

        
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        
    }
    
    public function institutetypes(Request $request){
        $dataset = DB::table('mst_parameter_master')
        ->select(['mst_parameter_master.inst_type_id','inst_type_master.inst_type_name'])
        ->join('mst_inst_type_master','mst_inst_type_master.inst_type_id', '=','mst_parameter_master.inst_type_id')
        ->GROUPBY ('mst_parameter_master.inst_type_id')
        ->get();
        
    }
    
    public function showfilterdata(Request $request,$myRadio,$mst_inst_id) {
       
        return back()
        ->with(['radio' => $request->input('_radioValue'),
        'master_institute_id' => $request->input('_mst_inst_id'),
        'inst_id' => $request->input('dropdown_institute_types')]);      
    }

    public function showfilterdata1(Request $request) {
         
          return back()
         ->with(['master_institute_id' => $request->input('master_institute_dropdown'),
         'radio' => $request->input('MyRadio'),
         'inst_id' => $request->input('dropdown_institute_types')]);
    }
    
    public function radiobuttonparameterchange(Request $request){
        echo '<br>';
        echo '<br>';
         echo $request->input('MyRadio');
       
        if($request->input('MyRadio') == 'First'){
            return back()->with(['radio' => $request->input('MyRadio'),'inst_id' => $request->input('dropdown_institute_types')]);      
        }
        else{
            return back()->with(['radio' => $request->input('MyRadio')])->with(['master_institute_id' => $request->input('master_institute_dropdown')]);
        }
    }
    
    private function setConfigWithConnection($connection)
    {
        $connectionDatabase = mysqli_connect($connection['host'],$connection['username'],$connection['password'], $connection['dbname']) or die('Unable to Connect');
        
        return $connectionDatabase;
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $int_type_id
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request,$int_type_id,$mst_inst_id,$radio)
    {

           $validatedData = $request->validate([
        'txt_name' => 'required|unique:mst_parameter_master,parameter_name'
       ]);


        if (+$mst_inst_id != -1){
            Config::set("tenant_db", [
                'driver' => 'mysql',
                'url' => '',
                'host' => '127.0.0.1',
                'port' => 3306,
                'database' => 'teaqip_tenant',
                'username' => 'root',
                'password' => '',
                'unix_socket' => '',
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
                'options' => [],
            ]);

            $result = DB::connection('tenant_db')
            ->table('mst_parameter_master')
            ->insert([
                'parameter_name' => $request->txt_name,
                'inst_type_id' => $request->input('modal_dropdown_institute_types'),
                'para_desc' => $request->txt_desc
            ]);
            DB::disconnect('tenant_db');

        }else{
            $result = DB::connection('mysql')
            ->table('mst_parameter_master')
            ->insert([
                'parameter_name' => $request->txt_name,
                'inst_type_id' => $request->get('modal_dropdown_institute_types'),
                'para_desc' => $request->txt_desc
            ]);

        }

        if($result == 1)
        return back()->with(['inst_id' => $int_type_id,'radio' => $radio,
        'master_institute_id' => $mst_inst_id])
        ->with('success', 'Data inserted successfully.');
        else 
        return back()->with(['inst_id' => $int_type_id,'radio' => $radio,
        'master_institute_id' => $mst_inst_id])
        ->with('error', 'Some error occurred while updating data. Please try again!');
        
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @param  int  $int_type_id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id, $int_type_id,$mst_inst_id,$radio)
    {
        if (+$mst_inst_id != -1){
            Config::set("tenant_db", [
                'driver' => 'mysql',
                'url' => '',
                'host' => '127.0.0.1',
                'port' => 3306,
                'database' => 'teaqip_tenant',
                'username' => 'root',
                'password' => '',
                'unix_socket' => '',
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
                'options' => [],
            ]);

            $result = DB::connection('tenant_db')
            ->table('mst_parameter_master')
            ->where('id', '=', $id) 
            ->update([
                'parameter_name' => $request->get('txt_name'),
                'para_desc' => $request->get('txt_desc')
            ]);
            DB::disconnect('tenant_db');

        }else{
            $result = DB::connection('mysql')
            ->table('mst_parameter_master')
            ->where('id', '=', $id) 
            ->update([
                'parameter_name' => $request->get('txt_name'),
                'para_desc' => $request->get('txt_desc')
            ]);

        }

        if($result == 1)
        return back()->with(['inst_id' => $int_type_id,'radio' => $radio,
        'master_institute_id' => $mst_inst_id])
        ->with('success', 'Data updated successfully.');
        else 
        return back()->with(['inst_id'=> $int_type_id,'radio' => $radio,
        'master_institute_id' => $mst_inst_id])
        ->with('error', 'Some error occurred while updating data. Please try again!');
    }
    
    /**
    * Enables the specified resource in Database.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @param  int  $int_type_id
    * @return \Illuminate\Http\Response
    */
    public function enable(Request $request, $id, $int_type_id,$mst_inst_id,$radio)
    {
        if (+$mst_inst_id != -1){
            Config::set("tenant_db", [
                'driver' => 'mysql',
                'url' => '',
                'host' => '127.0.0.1',
                'port' => 3306,
                'database' => 'teaqip_tenant',
                'username' => 'root',
                'password' => '',
                'unix_socket' => '',
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
                'options' => [],
            ]);

            $result = DB::connection('tenant_db')
            ->table('mst_parameter_master')
            ->where('id', '=', $id) 
            ->update([
                'status' => 1
              
            ]);
            DB::disconnect('tenant_db');
        }else{
            $mst_inst_id = -1;
            $result = DB::connection('mysql')
            ->table('mst_parameter_master')
            ->where('id', '=', $id) 
            ->update([
                'status' => 1
            ]);

        }

        if($result == 1)
        return back()->with(['inst_id' => $int_type_id,'radio' => $radio,
        'master_institute_id' => $mst_inst_id])
        ->with('success', 'Record Enabled successfully.');
        else 
        return back()->with(['inst_id' => $int_type_id,'radio' => $radio,
        'master_institute_id' => $mst_inst_id])
        ->with('error', 'Some error occurred while updating data. Please try again!');
        
    }
    
    /**
    * Disables the specified resource in Database.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @param  int  $int_type_id
    * @return \Illuminate\Http\Response
    */
    public function disable(Request $request, $id, $int_type_id,$mst_inst_id,$radio)
    {
        if (+$mst_inst_id != -1){
            Config::set("tenant_db", [
                'driver' => 'mysql',
                'url' => '',
                'host' => '127.0.0.1',
                'port' => 3306,
                'database' => 'teaqip_tenant',
                'username' => 'root',
                'password' => '',
                'unix_socket' => '',
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
                'options' => [],
            ]);
            $result = DB::connection('tenant_db')
            ->table('mst_parameter_master')
            ->where('id', '=', $id) 
            ->update([
                'status' => 0
            ]);
            DB::disconnect('tenant_db');
        }else{
            $mst_inst_id = -1;
            $result = DB::connection('mysql')
            ->table('mst_parameter_master')
            ->where('id', '=', $id) 
            ->update([
                'status' => 0
            ]);
        }

        if($result == 1)
        return back()->with(['inst_id' => $int_type_id,'radio' => $radio,
        'master_institute_id' => $mst_inst_id])
        ->with('success', 'Record Disabled successfully.');
        else 
        return back()->with(['inst_id' => $int_type_id,'radio' => $radio,
        'master_institute_id' => $mst_inst_id])
        ->with('error', 'Some error occurred while updating data. Please try again!');
        
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @param  int  $int_type_id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id, $int_type_id)
    {
        $crud = parametersModel::find($id); 
        $crud->id = $id;  
        $crud->status = 0;
        $result= $crud->save();  
        
        if($result == 1)
        return back()->with('inst_id', $int_type_id)
        ->with('success', 'Data deleted successfully.');
        else 
        return back()->with('inst_id', $int_type_id)
        ->with('error', 'Some error occurred while updating data. Please try again!');
        
    }
    
}
