<?php

namespace App\Http\Controllers\configData;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\instituteTypesModel;
use Illuminate\Support\ServiceProvides  ;
use Illuminate\Support\Facades\DB;
use Session;

class instituteTypesController extends Controller
{
    public function index()
    {

        $dataset = instituteTypesModel::get();

        
       return view('/configData/institutetypes',['dataset'=>$dataset]); 
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     
      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       



        $store = new instituteTypesModel();

      
        $store->inst_type_name = $request->txt_name;
        $store->inst_desc = $request->txt_desc;
        $result = $store->save();   
        
        if($result == 1)
        return back()->with('success', 'Data insert successfully.');
        else 
        return back()->with('error', 'Some error occurred while inserting data. Please try again!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $validatedData = $request->validate([
        'txt_name' => 'required|unique:mst_inst_type_master,inst_type_name',
        'inst_desc' => 'required'
        ]);
        $crud = instituteTypesModel::find($id); 
        $crud->id = $id;  
        $crud->inst_type_name =  $request->get('txt_name');  
        $crud->inst_desc = $request->get('txt_desc');  
        $result= $crud->save();

         if($result == 1)
         return back()->with('success', 'Data updated successfully.');
         else 
         return back()->with('error', 'Some error occurred while updating data. Please try again!');


       
    }
     /**
     * Enables the specified resource in Database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @param  int  $int_type_id
     * @return \Illuminate\Http\Response
     */
    public function enable(Request $request, $id)
    {
        
        $result = instituteTypesModel::where('id', $id)
                ->update(['status' => 1]);

         if($result == 1)
            return back()->with('success', 'Record enabled successfully.');
         else 
            return back()->with('error', 'Some error occurred while updating data. Please try again!');


    }

    /**
     * Disables the specified resource in Database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @param  int  $int_type_id
     * @return \Illuminate\Http\Response
     */
    public function disable(Request $request, $id)
    {
        $result =instituteTypesModel::where('id', $id)
                ->update(['status' => 0]);

        if($result == 1)
            return back()->with('success', 'Record disabled successfully.');
        else 
            return back()->with('error', 'Some error occurred while updating data. Please try again!');
            
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $crud = instituteTypesModel::find($id); 
        $crud->id = $id;  
        $crud->status = 0;
        $result= $crud->save();  
          if($result == 1)
             return back()->with('success', 'Data deleted successfully.');
          else 
            return back()->with('error', 'Some error occurred while deleting  data. Please try again!');
          
     }
}
