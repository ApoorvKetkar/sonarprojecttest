<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\loginModel;
use Session;

class loginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         return redirect('/dashboard');
       $check_username = $request->username;
       $check_password = $request->password;
        $logindata= DB::table('user_credentials')
                    ->select('user_name')
                    ->where(['user_name' =>$request->username, 'IsActive' => 1])
                    ->get();
        $json = json_decode($logindata, true);
          if(empty($json)){
            return back()->with('login-error', 'Either Provided User ID is not present in the system OR its not active');
        }else{
             $checkpassword=DB::table('user_credentials')
                    ->select('password')
                    ->where(['password'=> $request->password, 'IsActive' => 1])
                    ->get();
            $checkpasswordjson = json_decode($checkpassword, true);
                if(empty($checkpasswordjson)){
                    return back()->with('login-error', 'You have entered incorrect login credentials.');
                     return redirect('/dashboard');   
        } 
      // if($json == $check_username &&  $checkpasswordjson == $check_password){
        
            // return redirect('/dashboard'); 
          // }
               
                        

        
        
       
    }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  //   public function show(Request $request)
  // {
       

  //         $logindata= DB::table('user_credentials')
  //                   ->select('id')
  //                   ->where(['user_name' => $request->username, 'IsActive' => 1])
  //                   ->get();
  //       $json = json_decode($logindata, true);
  //       if(empty($json)){
  //           return back()->with('login-error', 'Either Provided User ID is not present in the system OR its not active');
  //       } else {
            
  //           $checkpassword= DB::table('user_credentials')
  //           ->select('*')
  //           -> join('user_master','user_master.user_id','=','user_credentials.id')
  //           -> where(['user_name' => $request->username, 'password'=> $request->password])
  //           ->get();
  //           // echo  $checkpassword;

  //           $checkpasswordjson = json_decode($checkpassword, true);
  //               if(empty($checkpasswordjson)){
  //                   return back()->with('login-error', 'You have entered incorrect login credentials.');
  //               }else {
                    
  //                   // $OrdID = $InstID = $masterInstID = 0;
  //                   //echo $checkpassword;
  //                   if ( $checkpassword[0]->user_type=='SUPER_ADMIN'){
  //                       // $masterInst = DB::table('oh_master_institute')
  //                       // -> select('*')
  //                       // -> where(['user_id' => $checkpassword[0]->user_id])
  //                       // -> get();
  //                       // $masterInstjson =  json_decode($masterInst, true);

  //                       // if(!empty($masterInstjson)) {
  //                       //     $masterInstID = $masterInst[0]->id;
                          
  //                       // }

  //                   }
  //                   return redirect('/dashboard'); 
                   
  //                   //Full Name,OrgID,InstID,MastInstID,username,UserType
  //                   // Session::put('OrgID', 'OrgID');
  //                   // Session::put('masterInstID', $masterInstID);
  //                   // Session::put('InstID', $InstID);
  //                   // Session::put('UserID', $checkpassword[0]->user_id);
  //                   // Session::put('UserType', $checkpassword[0]->user_type);
  //                   // Session::put('UserFirstName', $checkpassword[0]->first_name);
  //                   // Session::put('UserLastName', $checkpassword[0]->last_name);
  //                   // // Add Following In Session - End
                     
  //                       if($checkpassword[0]->user_type == 'PRINCIPAL'){
  //                           return redirect('/principal_dashboard'); 
  //                       }
  //                       if($checkpassword[0]->user_type == 'DIRECTOR'){
  //                           return redirect('/director_dashboard');    
  //                       }
                        
  //                       if($checkpassword[0]->user_type =='INST_HEAD'){
  //                           return redirect('/institutehead_dashboard'); 
  //                       }
  //                       if($checkpassword[0]->user_type =='TEACHER'){
  //                            return redirect('/teacher_dashboard'); 
  //                       }

  //               }
  //           }

  //   }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
