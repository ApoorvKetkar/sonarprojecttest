<?php

namespace App\Http\Controllers\institute;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\instituteModel;
use App\Models\userCredModel;
use App\Models\userDetailModel;
use App\Models\instituteTypesModel;
use Illuminate\Support\ServiceProvides  ;
use Illuminate\Support\Facades\DB;
use Session;


class instituteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        
        $datasetFilterData = null;
        $inst_type_id = Session::get('inst_id'); 
       
     
        if ($inst_type_id != null && +$inst_type_id != -1){
         //$datasetFilterData = instituteModel::where('inst_type_id', '=', $inst_type_id) ->get();
         $datasetFilterData = DB::table('oh_master_institute')->where('inst_type_id', '=', $inst_type_id) -> get();
        //  $datasetFilterData = json_decode($data);
        } else{
            $inst_type_id = -1;
        }
        $dataset2 = DB::table('mst_inst_type_master') -> get();
        return view('masterinstitute.index', compact(array('dataset2','datasetFilterData','inst_type_id')));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($inst_type_id)
    {
        echo  '$inst_type_id : ' . $inst_type_id;   
        $dataset2 = DB::table('mst_inst_type_master') -> get();
        $selected_salutation = '';
        return view('masterinstitute.create', compact(array('dataset2','inst_type_id','selected_salutation')));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$int_type_id)
    {
        $store = new instituteModel();
      
        $user_id = DB::table('user_credentials')->insertGetId(['user_name'=> $request->txt_username , 'password'=> $request->txt_password]);

        $user_detail = new userDetailModel();
        $user_detail->user_id = $user_id;
        $user_detail->salutation = $request->dropdown_institute_types;
        $user_detail->first_name = $request->txt_firstname;
        $user_detail->last_name = $request->txt_lastname;
        $user_detail->email = $request->txt_email;
        $user_detail->contact = $request->txt_contact;
        $user_detail_result =$user_detail->save();

        $store->user_id = $user_id;
        $store->master_institute_name = $request->txt_name;
        $store->address = $request->txt_address;
        $store->state = $request->txt_state;
        $store->city = $request->txt_city;
        $store->country = $request->txt_country;
        $store->pincode = $request->txt_pincode;
        $store->inst_type_id = $request->modal_dropdown_institute_types;
        $user_detail_result1 =$store->save();
      
        
        if($user_detail_result1 == 1 )
        return back()->with('int_type_id', $int_type_id)
                     ->with('success', 'Data added successfully.');
        else 
        return back()->with('int_type_id', $int_type_id)
                    ->with('error', 'Some error occurred while inserting data. Please try again!');
    }

    public function institutetypes(){
        $dataset = DB::table('oh_master_institute')
        ->select(['oh_master_institute.inst_type_id','mst_inst_type_master.inst_type_name'])
        ->join('mst_inst_type_master','mst_inst_type_master.id', '=','oh_master_institute.inst_type_id')
       ->GROUPBY ('oh_master_institute.inst_type_id')
        ->get();
    }

    public function userdetails(){
        $dataset1 = DB::table('oh_master_institute')
        ->select(['oh_master_institute.user_id','user_master.user_type','user_master.salutation','user_master.first_name',
        'user_master.last_name','user_master.email','user_master.contact','user_credentials.password'])
        ->join('user_master','user_master.user_id', '=','oh_master_institute.user_id')
        ->join('user_credentials','user_credentials.id', '=','user_master.user_id')
       ->GROUPBY ('oh_master_institute.inst_type_id')
        ->get();
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $inst_type_id = Session::get('inst_id'); 
    
        $crud = DB::table('oh_master_institute')
        ->select('*')
        ->where(['id'=>$id])
        ->get();

        $user_details = DB::table('user_master')
        ->select('*')
        ->where(['user_id'=>$crud[0]->user_id])
        ->get();

        $inst_type = DB::table('mst_inst_type_master')
        ->select('*')
        ->where(['id'=>$crud[0]->inst_type_id])
        ->get();

        $user_credential = DB::table('user_credentials')
        ->select('*')
        ->where(['id'=>$user_details[0]->user_id])
        ->get();
        $dataset2 = DB::table('mst_inst_type_master') -> get();

        return view('masterinstitute.show',compact(array('inst_type','crud','dataset2','user_details','user_credential')));   
       
    }

    public function close(){
        window.close();
    }


    public function showfilterdata(Request $request) {
        // echo "Selected Value is : " . $request->input('dropdown_institute_types'); 
        //$dataset2 =DB::table('inst_type_master') -> get();
        //$datasetFilterData = parametersModel::where('inst_type_id', '=', $request->input('dropdown_institute_types')) ->get();

         return back()->with(['inst_id' => $request->input('dropdown_institute_types')]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
echo $id;


$user = instituteModel::find($id);
$inst_type_id = Session::get('inst_id'); 
$dataset2 = DB::table('mst_inst_type_master') -> get();
$inst_type = DB::table('mst_inst_type_master')
->select('*')
->where(['id'=>$user->inst_type_id])
->get();

$user_detail = DB::table('user_master')
->select('*')
->where(['user_id'=>$user->user_id])
->get();

$user_credintials = DB::table('user_credentials')
->select('*')
->where(['id'=>$user->user_id])
->get();


echo $user_credintials;



        return view('masterinstitute.edit', compact(array('user','dataset2','inst_type_id','inst_type','user_detail','user_credintials')));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        echo  $request;
        echo '/br';
        echo $id;
        $inst_type_id = Session::get('inst_id'); 
        $dataset2 = DB::table('mst_inst_type_master') -> get();
        $users = instituteModel::find($id); 
        $users->master_institute_name =  $request->get('txt_name');  
        $users->address =  $request->get('txt_address');  
        $users->city =  $request->get('txt_city');  
        $users->state =  $request->get('txt_state');  
        $users->country =  $request->get('txt_country');  
        $users->pincode =  $request->get('txt_pincode');  

       
$result= $users->save();

if($result == 1)
return redirect('institute/manageinstitutes')->with('success', 'Data updated successfully.');
else 
return back()->with('error', 'Some error occurred while updating data. Please try again!');

       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     //
    // }
    public function enable(Request $request, $id, $int_type_id)
    {
        
        $result = instituteModel::where('id', $id)
                ->update(['status' => 1]);

         if($result == 1)
            return back()->with('inst_id', $int_type_id)
                         ->with('success', 'Record Enabled successfully.');
         else 
            return back()->with('inst_id', $int_type_id)
                         ->with('error', 'Some error occurred while updating data. Please try again!');


    }

    /**
     * Disables the specified resource in Database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @param  int  $int_type_id
     * @return \Illuminate\Http\Response
     */
    public function disable(Request $request, $id, $int_type_id)
    {
        $result = instituteModel::where('id', $id)
                ->update(['status' => 0]);

        if($result == 1)
            return back()->with('inst_id', $int_type_id)
                         ->with('success', 'Record Disabled successfully.');
        else 
            return back()->with('inst_id', $int_type_id)
                         ->with('error', 'Some error occurred while updating data. Please try again!');
            
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param  int  $int_type_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $int_type_id)
    {
        $crud = instituteModel::find($id); 
        $crud->id = $id;  
        $crud->status = 0;
        $result= $crud->save();  
        
        if($result == 1)
            return back()->with('id', $int_type_id)
                     ->with('success', 'Data deleted successfully.');
        else 
            return back()->with('id', $int_type_id)
                     ->with('error', 'Some error occurred while updating data. Please try again!');
    
    }
}
