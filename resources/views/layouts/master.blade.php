<!DOCTYPE html>

<!--
Project: TeaQIP
Author: Inspiring Souls
Website: https://teaqip.com
Contact: hello@teaqip.com
@ NeerayTech Solutions Pvt. Ltd.
-->

<html lang="en">

	<!-- begin::Head -->
	<head>
		<base href="../">
		<meta charset="utf-8" />
		<title>@yield('title')TeaQIP Admin | Teachers Quality Improvement Program</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<!--begin::Fonts -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">

		<!--end::Fonts -->

		<!--begin::Page Vendors Styles(used by this page) -->
		<link href="{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
		<!--end::Page Vendors Styles -->

		<!--begin::Global Theme Styles(used by all pages) -->
		<link href="{{asset('assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
		<!--end::Global Theme Styles -->

		<!--begin::Layout Skins(used by all pages) -->
		<link href="{{asset('assets/css/skins/header/base/brand.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/css/skins/header/menu/light.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/css/skins/brand/brand.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/css/skins/aside/light.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/css/custom.css')}}" rel="stylesheet" type="text/css" />

		<!--end::Layout Skins -->
		<link rel="shortcut icon" href="{{asset('media/logos/favicon.png')}}" />
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-aside-secondary--enabled kt-page--loading">

        <div class="spinner-grow text-dark" style="width:5rem;height:5rem;" role="status">
            <span class="sr-only">Loading...</span>
        </div>
        
		<!-- begin:: Header Mobile -->
		@include('partials.header-mobile')
		<!-- end:: Header Mobile -->

		<!-- begin:: Root -->
		<div class="kt-grid kt-grid--hor kt-grid--root">

			<!-- begin:: Page -->
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

				<!-- begin:: Aside -->
				@include('partials.aside')
				<!-- end:: Aside -->

				<!-- begin:: Wrapper -->
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

					<!-- begin:: Header -->
					@include('partials.header')
					<!-- end:: Header -->
					
					<!-- begin:: Content -->
					@yield('page-content')
					<!-- end:: Content -->
					
					<!-- begin:: Footer -->
					@include('partials.footer')
					<!-- end:: Footer -->
					
				</div>

				<!-- end:: Wrapper -->

				<!-- begin:: Aside Secondary -->
                @include('partials.aside2')
				<!-- end:: Aside Secondary -->
				
			</div>
			<!-- end:: Page -->
			
		</div>
		<!-- end:: Root -->

		@if (session('success'))
		<!--begin:: Toast markup-->
		<div class="toast toast-custom toast-fill fade" role="alert" aria-live="assertive" aria-atomic="true">
			<div class="toast-header">
				<i class="toast-icon fa fa-check-circle kt-font-success"></i><span class="toast-title">SUCCESS</span>
				<button type="button" class="toast-close" data-dismiss="toast" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="toast-body">{{ session('success') }}</div>
		</div>
		<!--end:: Toast markup-->   
		@endif
		@if (session('error'))
		<!--begin:: Toast markup-->
		<div class="toast toast-custom toast-fill fade" role="alert" aria-live="assertive" aria-atomic="true">
			<div class="toast-header">
				<i class="toast-icon fa fa-times-circle kt-font-danger"></i><span class="toast-title">ERROR</span>
				<button type="button" class="toast-close" data-dismiss="toast" aria-label="Close"><i class="fa fa-times"></i></button>
			</div>
			<div class="toast-body">{{ session('error') }}</div>
		</div>
		<!--end:: Toast markup-->
		@endif
		
		<!-- begin:: Modals -->
		@include('partials.modal')
		@include('partials.confirm')
		<!-- end:: Modals -->

		<!-- begin:: Scrolltop -->
		<div id="kt_scrolltop" class="kt-scrolltop">
			<i class="la la-arrow-up"></i>
		</div>
		<!-- end:: Scrolltop -->

		<!-- begin::Global Config(global config for global JS sciprts) -->
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"metal": "#c4c5d6",
						"light": "#ffffff",
						"accent": "#00c5dc",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995",
						"focus": "#9816f4"
					},
					"base": {
						"label": [
							"#c5cbe3",
							"#a1a8c3",
							"#3d4465",
							"#3e4466"
						],
						"shape": [
							"#f0f3ff",
							"#d9dffa",
							"#afb4d4",
							"#646c9a"
						]
					}
				}
			};
		</script>

		<!-- end::Global Config -->

		<!--begin::Global Theme Bundle(used by all pages) -->
		<script src="{{asset('assets/plugins/global/plugins.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/js/scripts.bundle.js')}}" type="text/javascript"></script>
		<!--end::Global Theme Bundle -->

		<!--begin::Page Vendors(used by this page) -->
		<script src="{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
		<!--end::Page Vendors -->

		<!--begin::Page Scripts(used by this page) -->
        <script src="{{asset('assets/js/master.config.js')}}" type="text/javascript"></script>
		<!--end::Page Scripts -->
		
		<!--begin::Page Scripts(used by this page) -->
		<script src="{{asset('assets/validation/jquery.validate.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/validation/additional.validate.js')}}" type="text/javascript"></script>
		<!--end::Page Scripts -->
		
		<!-- begin:: Custom Script Content -->
        @yield('custom-script-content')
        <!-- end:: Custom Script Content -->
		
	</body>

	<!-- end::Body -->
</html>