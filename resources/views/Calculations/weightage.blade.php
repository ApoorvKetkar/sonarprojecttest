@extends('layouts.master')

@section('title', 'Weightage - ')
 
@section('page-content')
<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

<!-- begin:: Subheader -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid kt-margin-t-10 kt-margin-b-10">
       
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Weightage</h3>
        </div>
        
    
        
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <form class="kt-form" id="form-modal-info"  action="weightagecreate"  method="POST">
              {{ csrf_field() }}
            <div class="kt-portlet__body">
               
            
                <div class="form-group row">
                    
                    <div class="col-sm-4">
                           
                        <label class="col-form-label" name="myLabel">Option&nbsp;&nbsp;{{$datasetweightage[0]->option_value}} <span class="kt-font-danger">*</span></label>
                        <input id="txt_weightage1" name="txt_weightage1" type="text" autocomplete="off" value="{{$datasetweightage[0]-> 
 weightage}}" class="form-control"  required>
                         
                    </div>
                    <div class="col-sm-2"></div>
                    <div class="col-sm-4">
                        
                        <label class="col-form-label">Option &nbsp;&nbsp;{{$datasetweightage[1]->option_value}}   <span class="kt-font-danger">*</span></label>
                        <input id="txt_weightage2" name="txt_weightage2" type="text" autocomplete="off" value="{{$datasetweightage[1]-> weightage}}"  class="form-control" required>
                       
                    </div>
                    <div class="col-sm-2"></div>
                </div>
                    <div class="form-group row">
                    
                    <div class="col-sm-4">
                     
                                    
                        <label class="col-form-label">Option &nbsp;&nbsp;{{$datasetweightage[2]->option_value}} <span class="kt-font-danger">*</span></label>
                        <input id="txt_weightage3" name="txt_weightage3" type="text" autocomplete="off" value="{{$datasetweightage[2]-> 
 weightage}}" class="form-control"  required>
                         
                    </div>
                    <div class="col-sm-2"></div>
                    <div class="col-sm-4">
                        
                        <label class="col-form-label">Option&nbsp;&nbsp;{{$datasetweightage[3]->option_value}}   <span class="kt-font-danger">*</span></label>
                        <input id="txt_weightage4" name="txt_weightage4" type="text" autocomplete="off" value="{{$datasetweightage[3]-> weightage}}"  class="form-control"  required>
                       
                    </div>
                    <div class="col-sm-2"></div>
                </div>
              
              
            </div>
            <div class="kt-portlet__foot">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10 text-right">
                        <button type="submit"id="btnSave"  class="btn btn-primary ">Save Info</button>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </div>

        </form>

    </div>
</div>
<!-- end:: Content -->

</div>

@endsection



@section('custom-script-content')

<script>
"use strict";
jQuery(document).ready(function() {

   // CRUD - Reset Form
    $('#modal-CRUD').on('hide.bs.modal', function() {
        $("#form-modal-info").validate().resetForm();
    });
  
    // Form validations (client side)
    $('#form-modal-info').validate({
        rules: {
           txt_weightage4: {
                required:true,
                min:0,
                
                digit:true,
                number:true,
                
            },
           txt_weightage3: {
                required:true,
                 min:0,
                 digit:true,
                 number:true,
                
            },
              txt_weightage2: {
                required:true,
                 min:0,
                 digit:true,
                 number:true,
                 

            },
              txt_weightage1: {
                required:true,
                 min:0,
                 digit:true,
                 number:true,
                
            },

        }
    });


</script>


@endsection
