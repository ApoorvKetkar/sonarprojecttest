@extends('layouts.master')

@section('title', 'Grade - ')
 
@section('page-content')
<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

<!-- begin:: Subheader -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid kt-margin-t-10 kt-margin-b-10">
       
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Grades</h3>
        </div>
        
         <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a class="btn btn-icon- btn btn-label btn-label-primary btn-upper btn-sm btn-bold add-button" >
                <i class="fa fa-plus"></i>&nbsp;&nbsp;Add New</a>
            </div>
        </div>
        
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">

<form action="grades/gradechange" class="getValue" method="POST">

@csrf

<div class="form-group row mb-0">
    <div class="col-sm-12 kt-align-center" >
	    <label class="kt-radio kt-radio--bold kt-radio--primary mr-5">
	        <input type="radio" name="MyRadio" class="RadioChange" id="MyRadio1" value="First"   checked {{ ( $radio == 'First' ) ? 'checked' : '' }} >Gold Copy
	        <span></span>
	    </label>
	    <label class="kt-radio kt-radio--bold kt-radio--primary">
	        <input type="radio" name="MyRadio" class="RadioChange" id="MyRadio2" value="Second"  {{ ( $radio == 'Second' ) ? 'checked' : '' }}  >Master Institute
	        <span></span>
	    </label>
	</div>
    @if(  ( $radio == 'Second' ) )
    <form  action="grades/institutefilterdata/" id="dropdown" method="POST">
    @csrf
    	<div class="col-sm-3"></div>
        <div class="col-sm-6" >
            <select class="form-control kt-font-bold mt-2 mb-3" name="master_institute_dropdown" id="master_institute_dropdown">
            <option value="-1" {{ ( +$master_institute_id == -1 ) ? 'selected' : '' }}>-- Select Institute Name --</option>
            <?php for($i = 0; $i < count($dataset3); $i++) : ?>                           
                <option value="{{ $dataset3[$i]->id }}" 
                {{ ( $dataset3[$i]->id == $master_institute_id ) ? 'selected' : '' }}
            >{{$dataset3[$i]->master_institute_name}}</option>
            <?php endfor ?>
            </select>   
        </div>
        <div class="col-sm-3"></div>
    </form>
    @endif
</div>

<div class="kt-separator kt-separator--border-dashed kt-separator--space-lg kt-separator--portlet-fit mt-3"></div>
    
</form>

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable basicDatatables">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Grade</th>
                        <th>Min Value</th>
                        <th>Max Value</th>
                        <th style="width:15%"  class="no-sort">Actions</th>
                       <th style="width:23%"  class="no-sort">Active / Inactive</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($datasetgrade))
                    <?php for($i = 0; $i < count($datasetgrade); $i++) : ?> 
                        <tr>
                            <td> <?= $i + 1 ?></td>
                            <td><?= $datasetgrade[$i]->grade_desc?></td>
                            <td><?= $datasetgrade[$i]->min_score?></td>
                            <td><?= $datasetgrade[$i]->max_score?></td>
                            <td>
                                 <a class="btn btn-sm btn-secondary btn-bold show-button"
                                    data-id="{{ json_encode($datasetgrade[$i],TRUE) }}">
                                    <span class="kt-font-secondary">Show</span></a>
                                <a class="btn btn-sm btn-secondary btn-bold update-button"
                                data-id="{{ json_encode($datasetgrade[$i],TRUE) }}">
                                <span class="kt-font-primary">Edit</span></a>
                               
                            </td>
                           
                            @if ($datasetgrade[$i]->status === 1)
                    <form action="grade/disable/{{$datasetgrade[$i]->id}}/{{$master_institute_id}}/{{session('radio')}}" method="POST">
                                @csrf
                             <td style="width:23%;vertical-align:middle;">
                                    <span class="badge badge-success mr-2">Active</span>
                                    <input type="submit" Value="Mark Inactive"
                                    class="btn btn-sm btn-secondary btn-bold disable-button" name="Disable">
                                </td>
                            </form>
                            @else
                    <form action="grade/enable/{{$datasetgrade[$i]->id}}/{{$master_institute_id}}/{{session('radio')}}" method="POST">    
                                @csrf
                               <td style="width:23%;vertical-align:middle;">
                                    <span class="badge badge-danger mr-2">Inactive</span>
                                    <input type="submit" Value="Mark Active"
                                    class="btn btn-sm btn-secondary btn-bold disable-button" name="Enable">
                                </td> 
                            </form>
                            @endif
                            
                        </tr>
                    <?php endfor ?> 
                    @endif
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
</div>
<!-- end:: Content -->

</div>

@endsection
<!--begin:: Layout for Modal-->
@section('modal-CRUD-header-content')

<h5 id="txTitle" class="modal-title"></h5>

@endsection

@section('modal-CRUD-body-content')

<!--begin::Form-->
<div class="form-group row">
   


    <div class="col-sm-12">
        @csrf
        <label class="col-form-label">Grade <span class="kt-font-danger">*</span></label>
        <input id="txt_name" name="txt_name" type="text" autocomplete="off" class="form-control"  pattern = "^(?![\s.]+$)[.0-9a-zA-Z\s,-/']+$" unique>
    </div>
    <div class="col-sm-12">
    <label class="col-form-label">Min Value <span class="kt-font-danger">*</span></label>
    <input id="txt_min_values" name="txt_min_values" type="text" class="form-control"unique  >
    
        
    </div>
    <div class="col-sm-12">
    <label class="  col-form-label">Max Value <span class="kt-font-danger">*</span></label>
    <input id="txt_max_values" name="txt_max_values" type="text" class="form-control" unique>
    
        
    </div>
</div>
<!--end::Form-->

@endsection

@section('modal-CRUD-footer-content')

<button type="button"id="btnClose" class="btn btn-secondary" data-dismiss="modal">Close</button>
<button type="submit"id="btnSave" class="btn btn-primary">Save Info</button>

@endsection

@section('modal-CONFIRM-footer-content')

<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
<button type="submit" class="btn btn-danger">Yes! Delete This Data</button>

@endsection

@section('custom-script-content')

<script>
"use strict";
jQuery(document).ready(function() {

    $('#master_institute_dropdown').on('change', function(e){
        $(this).closest('form').submit();
    });

    $("input[name='MyRadio']").on('click',function(){
        var selValue = $(this).attr("value");
        var targetBox = $("."+ selValue);
        console.log(selValue+"  "+targetBox);
        $(".box").not(targetBox).hide();
        $(targetBox).show();
        console.log(selValue);
             var FIrst = "<?php echo session('first')?>";
             var SEcond = "<?php echo session('second')?>";
            $("#MyRadio").val(selValue).change();
            $(this).closest('form').submit();

    });


    // CRUD - Add info
    $('.add-button').click(function(e) {
       

        var mst_inst_id = "<?php echo session('master_institute_id')?>";;
        var radio = "<?php echo session('radio')?>";;
        var mst_inst_name = "";;
        if (mst_inst_id != null && mst_inst_id != ''){
            mst_inst_id = "<?php echo session('master_institute_id')?>";
            mst_inst_name = $("#master_institute_dropdown option:selected").text();
        }
        else{
            mst_inst_id = -1;
        }
        if (mst_inst_name != ''){
            $("#txTitle").text('Add New Grades to ' + mst_inst_name);
        }else{
            $("#txTitle").text('Add New Grades to Gold Copy');
        }
        $("#txt_name").val("");
        $('#txt_name').prop('disabled', false);
        $("#txt_min_values").val("");
        $('#txt_min_values').prop('disabled', false);
        $("#txt_max_values").val("");
        $('#txt_max_values').prop('disabled', false);
        $('#btnSave').show();
        $('#form-modal-info').attr('action', 'gradecreate/' +  mst_inst_id + '/' +  radio);
        $('#modal-CRUD').modal('show');

    });

    // CRUD - Delete info
    $('.delete-button').click(function(e) {
        var record = $(this).data('id');
        $('#form-modal-confirm').attr('action', 'gradedelete/' + record.id);
        $('#modal-CONFIRM').modal('show');
    });

    // CRUD - Update info
    //$('.update-button').click(function(e) {
    $('.basicDatatables').on('click', '.update-button', function(e) {
        var record = $(this).data('id');
        var mst_inst_id = "<?php echo session('master_institute_id')?>";;
        var radio = "<?php echo session('radio')?>";;
        var mst_inst_name = "";;
        if (mst_inst_id != null && mst_inst_id != '' && +mst_inst_id != 0 && +mst_inst_id != -1){
            mst_inst_id = "<?php echo session('master_institute_id')?>";
            mst_inst_name = $("#master_institute_dropdown option:selected").text();
        }else{
            mst_inst_id = '-1';
        }
        if (mst_inst_name != ''){
            $("#txTitle").text('Update Grades Details For ' + mst_inst_name);
        }else{
            $("#txTitle").text('Update Grades Details For Gold Copy');
        }
        $("#txt_name").val(record.grade_desc);
         $('#txt_name').prop('disabled', false);
        $("#txt_min_values").val(record.min_score);
         $('#txt_min_values').prop('disabled', false);
        $("#txt_max_values").val(record.max_score);
         $('#txt_max_values').prop('disabled', false);
        //  $("#txTitle").text('Update Grade Details');
        $('#btnSave').show();
        $('#form-modal-info').attr('action', 'gradeupdate/' + record.id + '/' + mst_inst_id + '/' +  radio);
        $('#modal-CRUD').modal('show');
    });
     // CRUD - Show info
    $('.show-button').click(function(e) {
        var record = $(this).data('id');
        var inst_id = "<?php echo session('inst_id')?>";
        var mst_inst_id = "<?php echo session('master_institute_id')?>";;
        var mst_inst_name = "";;
        if (mst_inst_id != null && mst_inst_id != ''){
            mst_inst_id = "<?php echo session('master_institute_id')?>";
            mst_inst_name = $("#master_institute_dropdown option:selected").text();
        }
        if (mst_inst_name != ''){
            $("#txTitle").text('View Grades Details For ' + mst_inst_name);
        }else{
            $("#txTitle").text('View Grades Details For Gold Copy');
        }
        $("#txt_name").val(record.grade_desc);
        $('#txt_name').prop('disabled', true);
        $("#txt_min_values").val(record.min_score);
        $('#txt_min_values').prop('disabled', true);
        $("#txt_max_values").val(record.max_score);
        $('#txt_max_values').prop('disabled', true);
        $('#btnSave').hide();
        // $("#txTitle").text('View Grade Details');
        $('#modal-CRUD').modal('show');
    });

    $('#modal-CRUD').on('hide.bs.modal', function() {
        $("#form-modal-info").validate().resetForm();
    });

    $.validator.addMethod('decimal', function(value, element) {
  return this.optional(element) || /^((\d+(\\.\d{0,2})?)|((\d*(\.\d{1,2}))))$/.test(value);
}, "Please enter a correct number, format 0.00");

    $.validator.addMethod('le', function (value, element, param) {
        return this.optional(element) || parseInt(value) < parseInt($(param).val());
    },'Must be less than   Max value');
     $.validator.addMethod('ge', function (value, element, param) {
        return this.optional(element) || parseInt(value) > parseInt($(param).val());
    },'Must be greater than  Min value');

     $.validator.addMethod("unique", function(value, element, params) {
    var prefix = params;
    var selector = jQuery.validator.format("[name!='{0}'][name^='{1}'][unique='{1}']", element.name, prefix);
    var matches = new Array();
    $(selector).each(function(index, item) {
        if (value == $(item).val()) {
            matches.push(item);
        }
    });

    return matches.length == 0;
}, "Value is not unique.");


       



 // Form validations (client side)
    $('#form-modal-info').validate({
        rules: {
            txt_name: {
                required:true,
                minlength:1,
                maxlength:45,
            
            },
            txt_min_values: {
                required:true,
                 min:1,
                 max:100,
                 le:'#txt_max_values',
                 decimal:true,
                
                               
            },
            txt_max_values: {
                required:true,
                max:100,
                ge:'#txt_min_values',
                decimal:true,

               
            },

        }
    });

});


</script>

   
@endsection