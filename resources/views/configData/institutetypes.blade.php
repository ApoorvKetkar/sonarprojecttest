@extends('layouts.master')

@section('title', 'Institute Types - ')
 
@section('page-content')
<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

<!-- begin:: Subheader -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid kt-margin-t-10 kt-margin-b-10">
       
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Entity Types</h3>
        </div>
        
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a class="btn btn-icon- btn btn-label btn-label-primary btn-upper btn-font-sm btn-bold add-button">
                <i class="fa fa-plus"></i> Add New</a>
            </div>
        </div>
        
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable basicDatatables">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Type Name</th>
                        <th>Description</th>
                        <th style="width:17%"  class="no-sort">Actions</th>
                       <th style="width:23%"  class="no-sort">Active / Inactive</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($dataset))
                    <?php for($i = 0; $i < count($dataset); $i++) : ?> 
                        <tr>
                            <td> <?= $i + 1 ?></td>
                            <td><?= $dataset[$i]->inst_type_name?></td>
                            <td><?= $dataset[$i]->inst_desc?></td>
                         
                             <td style="width:15%">
                            <a class="btn btn-sm btn-secondary btn-bold show-button"
                                    data-id="{{$dataset[$i]}}">
                                    <span class="kt-font-secondary">Show</span></a>
                            <a class="btn btn-sm btn-secondary btn-bold update-button"
                                data-id="{{ $dataset[$i]}}">
                                <span class="kt-font-primary">Edit</span></a>
                            
                        </td>
                       
                            @if ($dataset[$i]->status === 1)
                            <form action="institutetype/disable/{{$dataset[$i]->id}}" method="POST">
                                @csrf
                                 <td style="width:23%;vertical-align:middle;">
                                    <span class="badge badge-success mr-2">Active</span>
                                    <input type="submit" Value="Mark Inactive"
                                    class="btn btn-sm btn-secondary btn-bold disable-button" name="Disable">
                                </td>
                                
                            </form>
                            @else
                            <form action="institutetype/enable/{{$dataset[$i]->id}}" method="POST">    
                                @csrf
                                 <td style="width:23%;vertical-align:middle;">
                                    <span class="badge badge-danger mr-2">Inactive</span>
                                    <input type="submit" Value="Mark Active"
                                    class="btn btn-sm btn-secondary btn-bold disable-button" name="Enable">
                                </td>
                                
                            </form>
                            @endif
                                               </tr>
                    <?php endfor ?> 
                    @endif
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
</div>
<!-- end:: Content -->

</div>

@endsection

<!--begin:: Layout for Modal-->
@section('modal-CRUD-header-content')

<h5 class="modal-title" id="txTitle">Add / Update Institute Type</h5>

@endsection

@section('modal-CRUD-body-content')

<!--begin::Form-->
<div class="form-group row">
    <div class="col-sm-12">
        @csrf
        <label class="col-form-label">Name of the Institute Type <span class="kt-font-danger">*</span></label>
        <input id="txt_name" name="txt_name" type="text" class="form-control"  pattern = "^(?![\s.]+$)[.0-9a-zA-Z\s,-/']+$" >
    </div>
    <div class="col-sm-12">
        <label class="col-form-label">Description<span class="kt-font-danger">*</span></label>
        <textarea id="txt_desc" name="txt_desc" type="textarea" class="form-control" rows="4"  pattern = "^(?![\s.]+$)[.0-9a-zA-Z\s,-/']+$"></textarea>
    </div>
</div>
<!--end::Form-->

@endsection

@section('modal-CRUD-footer-content')

<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
<button type="submit" class="btn btn-primary" id="btnSave">Save Info</button>

@endsection

@section('modal-CONFIRM-footer-content')

<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
<button type="submit" class="btn btn-danger">Yes! Delete This Data</button>

@endsection

@section('custom-script-content')
<script>
"use strict";
jQuery(document).ready(function() {

   // CRUD - Add info
    $('.add-button').click(function(e) {
         $("#txTitle").text('Add New Institute Types');
        $('#form-modal-info').attr('action', 'institutetypescreate');
        $('#modal-CRUD').modal('show');
    });

    // CRUD - Delete info
    $('.basicDatatables').on('click', '.delete-button', function(e) {
        var record = $(this).data('id');
        var inst_id = "<?php echo session('inst_id')?>";
        $('#form-modal-confirm').attr('action', 'institutetypedelete/' + record.id );
        $('#modal-CONFIRM').modal('show');
    });

    // CRUD - Update info
    $('.basicDatatables').on('click', '.update-button', function(e) {
        var record = $(this).data('id');
         $("#txt_name").val(record.inst_type_name);
        $('#txt_name').prop('disabled', false);
        $("#txt_desc").val(record.inst_desc);
        $('#txt_desc').prop('disabled', false);
       
        $("#txTitle").text('Update InstituteTypes Details');
        $('#btnSave').show();
        $('#form-modal-info').attr('action', 'institutetypesupdate/' + record.id );
        $('#modal-CRUD').modal('show');
new XMLHttpRequest()    });

    // CRUD - Show info
    $('.basicDatatables').on('click', '.show-button', function(e) {
        var record = $(this).data('id');
       
        $("#txt_name").val(record.inst_type_name);
        $('#txt_name').prop('disabled', true);
        $("#txt_desc").val(record.inst_desc);
        $('#txt_desc').prop('disabled', true);
       
        $('#btnSave').hide();
        $("#txTitle").text('View Institute Types Details');
        $('#modal-CRUD').modal('show');
    });

    // CRUD - Reset Form
    $('#modal-CRUD').on('hide.bs.modal', function() {
        $("#form-modal-info").validate().resetForm();
    });
    


    // Form validations (client side)
    $('#form-modal-info').validate({
        rules: {
            txt_name: {
                required:true,
                minlength:2,
                maxlength:1000
               
               
            },
            txt_desc: {
                maxlength:1000,
               required:true
               
            }
        }
    });
   });

</script>

@endsection


 
