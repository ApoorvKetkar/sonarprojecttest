@extends('layouts.master')

@section('title', 'Parameters - ')
 
@section('page-content')

<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">


 
 <!-- begin:: Subheader -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">

    <div class="kt-container kt-container--fluid kt-margin-t-10 kt-margin-b-10">
    
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Parameters</h3>

        </div>
        
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a class="btn btn-icon- btn btn-label btn-label-primary btn-upper btn-sm btn-bold add-button">
                <i class="fa fa-plus"></i>&nbsp;&nbsp;Add New</a>
            </div>
        </div>
        
        
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    
    <div class="kt-portlet kt-portlet--mobile">
       
        <div class="kt-portlet__body">

        <form action="parameters/parameterchange"  method="POST">

@csrf

<div class="form-group row mb-0">
    <div class="col-sm-12 kt-align-center" >
        <label class="kt-radio kt-radio--bold kt-radio--primary mr-5">
            <input type="radio" name="MyRadio" class="RadioChange" id="MyRadio1" value="First" checked  {{ ( $radio == 'First' ) ? 'checked' : '' }} >Gold Copy
            <span></span>
        </label>
        <label class="kt-radio kt-radio--bold kt-radio--primary">
            <input type="radio" name="MyRadio" class="RadioChange" id="MyRadio2" value="Second"  {{ ( $radio == 'Second' ) ? 'checked' : '' }}  >Master Institute
            <span></span>
        </label>
    </div>
    @if(($radio == 'Second' ) )
    <form  action="parameters/institutefilterdata/" id="dropdown" method="POST" class="box">
    @csrf
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
            <select class="form-control kt-font-bold mt-2 mb-3" name="master_institute_dropdown" id="master_institute_dropdown">
            <option value="-1" {{ ( +$master_institute_id == -1 ) ? 'selected' : '' }}>-- Select Institute Name --</option>
            <?php for($i = 0; $i < count($dataset3); $i++) : ?>                           
                <option value="{{ $dataset3[$i]->id }}" 
                {{ ( $dataset3[$i]->id == $master_institute_id ) ? 'selected' :''}}
            >{{$dataset3[$i]->master_institute_name}}</option>
            <?php endfor ?>
            </select>   
        </div>
        <div class="col-sm-3"></div>
    </form>
    @endif
              </div>
              
              <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg kt-separator--portlet-fit mt-3"></div>
    </form>


         @if(($radio == 'First' ) )
            <form class="kt-form kt-form--label-right" action="parameters/filterdata/{{$radio}}/{{$master_institute_id}}" method="POST">
                @csrf
                <input type="hidden" name="_radioValue" value="{{$radio}}">
                <input type="hidden" name="_mst_inst_id" value="{{$master_institute_id}}">
                <div class="form-group row">
                    <div class="col-sm-4 col-6 kt-align-right">
                        <label class="col-form-label pt-0 kt-font-bold mt-2">Choose Institute Type <span class="kt-font-danger">*</span></label>
                    </div>
                    <div class="col-sm-6 col-6">
                        <select class="form-control" name="dropdown_institute_types" id="dropdown_institute_types">
                        <option value="-1" {{ ( +$inst_type_id == -1 ) ? 'selected' : '' }}>-- Select Institute Type --</option>
                        <?php for($i = 0; $i < count($dataset2); $i++) : ?>                           
                            <option value="{{ $dataset2[$i]->id }}" 
                            {{ ( $dataset2[$i]->id == $inst_type_id ) ? 'selected' : '' }}
                            >{{$dataset2[$i]->inst_type_name}}</option>
                        <?php endfor ?>
                        </select>   
                    </div>
                </div>
                <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg kt-separator--portlet-fit"></div>
            </form>
               @else
                 <form class="kt-form kt-form--label-right" action="parameters/filterdata/{{$radio}}/{{$master_institute_id}}" method="POST">
                @csrf
                <input type="hidden" name="_radioValue" value="{{$radio}}">
                <input type="hidden" name="_mst_inst_id" value="{{$master_institute_id}}">
                <div class="form-group row">
                    <div class="col-sm-4 col-6 kt-align-right">
                        <label class="col-form-label pt-0 kt-font-bold mt-2">Institute Type <span class="kt-font-danger">*</span></label>
                    </div>
                    <div class="col-sm-6 col-6">
                        <select class="form-control" name="dropdown_institute_types" id="dropdown_institute_types">
                     
                            
                    <option value="-1" {{ ( +$inst_type_id == -1 ) ? 'selected' : '' }}>-- Select Institute Type --</option> 
                              <?php for($i = 1; $i < count($dataset_master_institue_institute); $i++) : ?>                     <option value="{{ $dataset_master_institue_institute[$i]->inst_type_id }}" 
                            {{ ($dataset_master_institue_institute[$i]->inst_type_id == $inst_type_id ) ? 'selected' : '' }}>{{$dataset_master_institue_institute[$i]->inst_type_name}}</option>
                        <?php endfor ?>
                        </select>   
                    </div>
                </div>
                <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg kt-separator--portlet-fit"></div>
            </form>
                  

              @endif
         
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable basicDatatables">
                <thead>
                    <tr>
                        <th style="width:5%">#</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th style="width:17%">Actions</th>
                        <th style="width:23%">Active / Inactive</th>
                    </tr>
                </thead>
                <tbody>
                        @if(!empty($datasetFilterData))
                    <?php for($i = 0; $i < count($datasetFilterData); $i++) : ?> 
                        <tr>
                            <td style="width:5%"> <?= $i + 1 ?></td>
                            <td><?= $datasetFilterData[$i]->parameter_name?></td>
                            <td><?= $datasetFilterData[$i]->para_desc?></td>
                        <td style="width:15%">
                            <a class="btn btn-sm btn-secondary btn-bold show-button"
                                    data-id="{{ json_encode($datasetFilterData[$i],TRUE) }}">
                                    <span class="kt-font-secondary">Show</span></a>
                            <a class="btn btn-sm btn-secondary btn-bold update-button"
                                data-id="{{ json_encode($datasetFilterData[$i],TRUE) }}">
                                <span class="kt-font-primary">Edit</span></a>
                        </td>
                      
                            @if ($datasetFilterData[$i]->status === 1)
                            <form action="parameters/disable/{{$datasetFilterData[$i]->id}}/{{session('inst_id')}}/{{session('master_institute_id')}}/{{session('radio')}}" method="POST">
                                @csrf
                               <td style="width:23%;vertical-align:middle;">
                                    <span class="badge badge-success mr-2">Active</span>
                                    <input type="submit" Value="Mark Inactive"
                                    class="btn btn-sm btn-secondary btn-bold disable-button" name="Disable">
                                </td>
                            </form>
                            @else
                            <form action="parameters/enable/{{$datasetFilterData[$i]->id}}/{{session('inst_id')}}/{{session('master_institute_id')}}/{{session('radio')}}" method="POST">    
                                @csrf
                                <td style="width:23%;vertical-align:middle;">
                                    <span class="badge badge-danger mr-2">Inactive</span>
                                    <input type="submit" Value="Mark Active"
                                    class="btn btn-sm btn-secondary btn-bold disable-button" name="Enable">
                                </td>
                            </form>
                            @endif
                       
                    </tr>
                    <?php endfor; ?> 
                    @endif
                   
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
</div>
<!-- end:: Content -->
</div>

@endsection

<!--begin:: Layout for Modal-->
@section('modal-CRUD-header-content')

<h5 id="txTitle" class="modal-title"></h5>

@endsection

@section('modal-CRUD-body-content')

<!--begin::Form-->
<div class="form-group row">
    @csrf
    <div class="col-sm-12">
        <label class="col-form-label">Institute Type</label>
        <select class="form-control" name="modal_dropdown_institute_types" id="modal_dropdown_institute_types">
            <option value="-1">-- Select Intitute Type --</option>
            <?php for($i = 0; $i < count($dataset2); $i++) : ?>                           
                <option value="{{ $dataset2[$i]->id }}" >{{$dataset2[$i]->inst_type_name}}</option>
            <?php endfor ?>
        </select>   
    </div>
    <div class="col-sm-12">
        <label class="col-form-label">Parameter Name <span class="kt-font-danger">*</span></label>
        <input id="txt_name" name="txt_name" type="text" class="form-control">
    </div>
    <div class="col-sm-12">
        <label class="col-form-label">Description<span class="kt-font-danger">*</span></label>
        <textarea id="txt_desc" name="txt_desc" type="textarea" class="form-control" rows="4" ></textarea>
    </div>
</div>
<!--end::Form-->

@endsection

@section('modal-CRUD-footer-content')

<button id="btnClose" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
<button id="btnSave" type="submit" class="btn btn-primary">Save Info</button>

@endsection

@section('modal-CONFIRM-footer-content')

<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
<button type="submit" class="btn btn-danger">Yes! Delete This Data</button>

@endsection

@section('custom-script-content')

<script>
"use strict";
jQuery(document).ready(function() {

    // DropDown Change for filter 
    $('#dropdown_institute_types').on('change', function(e){

        var inst_id = "<?php echo session('inst_id')?>";
        if (inst_id != null && inst_id != ''){
            inst_id = "<?php echo session('inst_id')?>";
        }
        $(this).closest('form').submit();
    });

    // $('#master_institute_dropdown').on('change', function(e){
    //     var inst_id = "<?php echo session('inst_id')?>";


    //     $(this).closest('form').submit();
    // });


    $('#master_institute_dropdown').on('change',function(){
         var inst_id = "<?php echo session('inst_id')?>";
        $.getJSON("{{ url('parameters/filterdata/Second/1')}}", 
        { option: $(this).val() }, 
        function(data) {
            var model = $('#dropdown_institute_types');
            model.empty();
            $.each(data, function(index, element) {
                model.append("<option value='"+element.id+"'>" + element.inst_type_name + "</option>");
            });
        });
        $(this).closest('form').submit();
    });


    $("input[name='MyRadio']").on('click',function(){
        var selValue = $(this).attr("value");
        var targetBox = $("."+ selValue);
        console.log(selValue+"  "+targetBox);
        $(".box").not(targetBox).hide();
        $(targetBox).show();
        console.log(selValue);
             var FIrst = "<?php echo session('first')?>";
             var SEcond = "<?php echo session('second')?>";
            $("#MyRadio").val(selValue).change();
            $(this).closest('form').submit();

    });

    // CRUD - Add info
    $('.add-button').click(function(e) {
        var inst_id = "<?php echo session('inst_id')?>";
        var mst_inst_id = "<?php echo session('master_institute_id')?>";;
        var radio = "<?php echo session('radio')?>";;
        var mst_inst_name = "";;
        if (mst_inst_id != null && mst_inst_id != ''){
            mst_inst_id = "<?php echo session('master_institute_id')?>";
            mst_inst_name = $("#master_institute_dropdown option:selected").text();
        }
        if (mst_inst_name != ''){
            $("#txTitle").text('Add New Parameter to ' + mst_inst_name);
        }else{
            $("#txTitle").text('Add New Parameter to Gold Copy');
        }
        $("#txt_name").val("");
        $('#txt_name').prop('disabled', false);
        $("#txt_desc").val("");
        $('#txt_desc').prop('disabled', false);
        $("#modal_dropdown_institute_types").val(inst_id).change();
        $("#modal_dropdown_institute_types").prop('disabled', false);
        $('#btnSave').show();
        $('#form-modal-info').attr('action', 'parameters/store/' + inst_id+ '/' +  mst_inst_id+ '/' +  radio);
        $('#modal-CRUD').modal('show');
    });

    // CRUD - Delete info
    $('.delete-button').click(function(e) {
        var record = $(this).data('id');
        var inst_id = "<?php echo session('inst_id')?>";
        $('#form-modal-confirm').attr('action', 'parameters/delete/' + record.id + '/' +  inst_id);
        $('#modal-CONFIRM').modal('show');
    });

    // CRUD - Update info
    $('.update-button').click(function(e) {
        var record = $(this).data('id');
        var inst_id = "<?php echo session('inst_id')?>";
        var mst_inst_id = "<?php echo session('master_institute_id')?>";;
        var radio = "<?php echo session('radio')?>";;
        var mst_inst_name = "";;
        if (mst_inst_id != null && mst_inst_id != '' && +mst_inst_id != 0 && +mst_inst_id != -1){
            mst_inst_id = "<?php echo session('master_institute_id')?>";
            mst_inst_name = $("#master_institute_dropdown option:selected").text();
        }else{
            mst_inst_id = '-1';
        }
        if (mst_inst_name != ''){
            $("#txTitle").text('Update Parameter Details For ' + mst_inst_name);
        }else{
            $("#txTitle").text('Update Parameter Details For Gold Copy');
        }
        $("#txt_name").val(record.parameter_name);
        $('#txt_name').prop('disabled', false);
        $("#txt_desc").val(record.para_desc);
        $('#txt_desc').prop('disabled', false);
        $("#modal_dropdown_institute_types").val(inst_id).change();
        $("#modal_dropdown_institute_types").prop('disabled', true);
        $('#btnSave').show();
        $('#form-modal-info').attr('action', 'parameters/update/' + record.id + '/' +  inst_id + '/' +  mst_inst_id + '/' +  radio);
        $('#modal-CRUD').modal('show');
    });

    // CRUD - Show info
    $('.show-button').click(function(e) {
        var record = $(this).data('id');
        var inst_id = "<?php echo session('inst_id')?>";
        var mst_inst_id = "<?php echo session('master_institute_id')?>";;
        var mst_inst_name = "";;
        if (mst_inst_id != null && mst_inst_id != ''){
            mst_inst_id = "<?php echo session('master_institute_id')?>";
            mst_inst_name = $("#master_institute_dropdown option:selected").text();
        }
        if (mst_inst_name != ''){
            $("#txTitle").text('View Parameter Details For ' + mst_inst_name);
        }else{
            $("#txTitle").text('View Parameter Details For Gold Copy');
        }
        $("#txt_name").val(record.parameter_name);
        $('#txt_name').prop('disabled', true);
        $("#txt_desc").val(record.para_desc);
        $('#txt_desc').prop('disabled', true);
        $("#modal_dropdown_institute_types").val(inst_id).change();
        $("#modal_dropdown_institute_types").prop('disabled', true);
        $('#btnSave').hide();
        $('#modal-CRUD').modal('show');
    });

    // CRUD - Reset Form
    $('#modal-CRUD').on('hide.bs.modal', function() {
        $("#form-modal-info").validate().resetForm();
    });

    // Form validations (client side)
    $('#form-modal-info').validate({
        rules: {
           
            txt_name: {
                required:true,
                minlength:2,
                maxlength:45
            },
            txt_desc: {
                required: true,
                 minlength:2,
                maxlength:1000
            }
        }
    });
   });

</script>

@endsection


