@extends('layouts.master')

@section('title', 'Questions - ')
 
@section('page-content')

<!-- begin:: Top Div -->
<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

<!-- begin:: Subheader -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid kt-margin-t-10 kt-margin-b-10">
       
        <!-- <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Add/Update Master Institute</h3>

        </div> -->
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" >
   <!--begin::Form-->
    <form class="kt-form kt-form--label-right" id="form-modal-info" action="../../../questions/questionsstore/{{$inst_type_id}}/{{$master_institute_id}}/{{$radio}}" method="POST">
  
       @csrf
       <div class="col-sm-12">
               <label class="col-form-label">Institute Type</label>
                <select class="form-control" name="modal_dropdown_institute_types" id="modal_dropdown_institute_types" >
                <?php for($i = 0; $i < count($dataset2); $i++) : ?>                           
                        <option value="{{ $dataset2[$i]->id }}" 
                        {{( $dataset2[$i]->id == $inst_type_id ) ? 'selected' : '' }}>{{$dataset2[$i]->inst_type_name}}
                        </option>
                        <?php endfor ?>
                </select>   
            </div>

             <div class="col-sm-12">
                <label class="col-form-label">Choose Parameter Name</label>
                <select class="form-control" name="dropdown_parameters" id="dropdown_parameters">
                            <option value="-1">-- All Paramters --</option>
                            <?php for($i = 0; $i < count($parameter_dataset); $i++) : ?>
                             <option value="{{ $parameter_dataset[$i]->id }}"  
                           >{{$parameter_dataset[$i]->parameter_name}}
                                </option>
                            <?php endfor ?>
                </select> 
            </div>
        <div class="form-group row">
    
            <div class="col-sm-12"> 
                <h5 id="txTitle" class="kt-subheader__title" >Add Questions Details </h5>
            </div>

          
            <div class="col-sm-12">
            <label class="col-form-label">Question Details</label>
             <input id="txt_questions" name="txt_questions" type="text" class="form-control" maxlength="50" autocomplete="off"pattern = "^(?![\s.]+$)[.0-9a-zA-Z\s,-/']+$">
          </div>
     <div class="col-sm-12">
        <label class="col-form-label">Option 1</label>
         <input id="text_option1" name="text_option1" autocomplete="off" type="text" class="form-control" maxlength="50" pattern = "^(?![\s.]+$)[.0-9a-zA-Z\s,-/']+$">
    </div>
     <div class="col-sm-12">
        <label class="col-form-label">Option 2</label>
         <input id="text_option2" name="text_option2" autocomplete="off" type="text" class="form-control" maxlength="50" pattern = "^(?![\s.]+$)[.0-9a-zA-Z\s,-/']+$">
    </div>
     <div class="col-sm-12">
        <label class="col-form-label">Option 3</label>
         <input id="text_option3" name="text_option3" autocomplete="off" type="text" class="form-control" maxlength="50" pattern = "^(?![\s.]+$)[.0-9a-zA-Z\s,-/']+$">
    </div>
     <div class="col-sm-12">
        <label class="col-form-label">Option 4</label>
        <input id="text_option4" name="text_option4" autocomplete="off" type="text" class="form-control" maxlength="50" pattern = "^(?![\s.]+$)[.0-9a-zA-Z\s,-/']+$">
    </div>
    
    <div class="col-sm-12" style="text-align:right; margin-top:10px; ">
    
     &nbsp; &nbsp; &nbsp; <button type="submit"  class="btn btn-primary">Save Info</button>
    </div>
  </div>
 </form>
    <!--end::Form-->
</div>
<!-- end:: Content -->

</div>
<!-- end:: Top Div -->
@endsection

@section('custom-script-content')

<script>
"use strict";
jQuery(document).ready(function() {

    $.validator.addMethod("noSpace", function(value, element) { 
  return value.indexOf(" ") < 0 && value != ""; 
 }, "No space please and don't leave it empty");

    Form validations (client side)
    $('#form-modal-info').validate({
        rules: {
            txt_name: {
                required: true,
                minlength:2,
                
            },
           txt_questions: {
                required:true,
                 minlength:2,
                
            },
            text_option1:{
                 required:true,
                  minlength:2,
                  

             },
              text_option2:{
                 required:true,
                  minlength:2,
                
            },
             text_option3:{
                 required:true,
                  minlength:2,
                  
            },
             text_option4:{
                 required:true,
                 minlength:2,
                
                 

            }




        }
    });
   });

</script>


@endsection





