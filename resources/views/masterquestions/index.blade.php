@extends('layouts.master')
@section('title', 'Questions - ')
@section('page-content')

<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">


    
    <!-- begin:: Subheader -->
    <div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid kt-margin-t-10 kt-margin-b-10">
    <div class="kt-subheader__main">
    <h3 class="kt-subheader__title">Questions</h3>
    </div>
    <form id="form-modal-info" action="questions/create/{{$inst_type_id}}/{{$master_institute_id}}/{{$radio}}" method="GET">
    
    <div class="kt-subheader__toolbar">
    <div class="kt-subheader__wrapper">
    <a class="btn btn-icon- btn btn-label btn-label-primary btn-upper btn-sm btn-bold add-button2">
    <i class="fa fa-plus"></i>&nbsp;&nbsp;Add New</a>
    </div>
    </div>
    </form>
    </div>
    </div><!-- end:: Subheader -->
    
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">

    <form action="questions/questionschange"  method="POST">

@csrf

<div  class="col-md-12 form-group row mb-0" >
<div class="col-sm-12 kt-align-center" >
<label class="kt-radio kt-radio--bold kt-radio--primary mr-5">
<input type="radio" name="MyRadio" class="RadioChange" id="MyRadio1" value="First" {{ ( $radio == 'First' ) ? 'checked' : '' }} >Gold Copy
<span></span>
</label>
<label class="kt-radio kt-radio--bold kt-radio--primary">
<input type="radio" name="MyRadio" class="RadioChange" id="MyRadio2" value="Second" {{ ( $radio == 'Second' ) ? 'checked' : '' }}  >Master Institute
<span></span>
</label>
</div>
@if(  ( $radio == 'Second' ) )
<form  action="questions/questionsfilterdata/" id="dropdown" method="POST" >
@csrf

<div class="col-sm-3"></div>
<div class="col-sm-6" >
<select class="form-control kt-font-bold mt-2 mb-3" name="master_institute_dropdown" id="master_institute_dropdown">
<option value="-1" {{ ( +$master_institute_id == -1 ) ? 'selected' : '' }}>-- Select Intitute Name --</option>
<?php for($i = 0; $i < count($dataset3); $i++) : ?>                           
    <option value="{{ $dataset3[$i]->id }}" 
    {{ ( $dataset3[$i]->id == $master_institute_id ) ? 'selected' : '' }}
    >{{$dataset3[$i]->master_institute_name}}</option>
    <?php endfor ?>
    </select>   
    </div>
    <div class="col-sm-3"></div>
    </form>
    @endif
    </div>
    
    </form>
    <div class="form-group row">
    <div class="col-sm-6 col-6">
    <form class="kt-form kt-form--label-right" action="questions/getParameters/{{$master_institute_id}}/{{$radio}}" method="POST">
    @csrf
    <label class="col-form-label pt-0 kt-font-bold" style="padding-top:6px !important;">Choose Institute Type <span class="kt-font-danger">*</span></label>
    <select class="form-control" name="dropdown_institute_types" id="dropdown_institute_types">
    <option value="-1" {{ ( +$inst_type_id == -1 ) ? 'selected' : '' }}>-- Select Intitute Name --</option>                         
    <?php for($i = 0; $i < count($dataset2); $i++) : ?>  
       
        <option value="{{ $dataset2[$i]->id }}"  
        {{( $dataset2[$i]->id == $inst_type_id ) ? 'selected' : '' }}
        >{{$dataset2[$i]->inst_type_name}}
        </option>
        <?php endfor ?>
        </select>
        </form>
        </div>
        <div class="col-sm-6 col-6">
        <form class="kt-form kt-form--label-right"  action="questions/filterQuestions/{{$inst_type_id}}/{{$master_institute_id}}/{{$radio}}"  method="POST">
        
        @csrf
        <label class="col-form-label pt-0 kt-font-bold" style="padding-top:6px !important;">Choose Parameter Name <span class="kt-font-danger">*</span></label>
        <select class="form-control" name="dropdown_parameters" id="dropdown_parameters">
        <option value="-1">-- Select Parameter Name --</option>
        @if(!empty($parameter_dataset))
        <?php for($i = 0; $i < count($parameter_dataset); $i++) : ?>  
            <option value="{{ $parameter_dataset[$i]->id }}"  
            {{( $parameter_dataset[$i]->id == $parameter_id ) ? 'selected' : '' }}>{{$parameter_dataset[$i]->parameter_name}}
            </option>
            <?php endfor ?>
            @endif
            </select> 
            </form>  
            </div>
            </div>
            </div>
            </div>
            
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable basicDatatables">
            <thead>
            <tr>
            <th style="width:5%">#</th>
            
            <th>Parameters Name</th>
            <th style="width:45%">Question Details</th>
            <th style="width:15%">Actions</th>
            <th style="width:20%">Enable/Disable</th>
            </tr>
            </thead>
            <tbody>
            @if(!empty($questions_dataset))
            <?php for($i = 0; $i < count($questions_dataset); $i++) : ?> 
                <tr>
                <td style="width:5%"> <?= $i + 1 ?></td>
                <td><?= $questions_dataset[$i]->parameter_name?></td>
                <td>
                <?= $questions_dataset[$i]->questions?> 
                <br><br>
                <div class="row">
                <div class="col-3"><strong>A.</strong>&nbsp;<label><?= $questions_dataset[$i]->option_1?></label></div>
                <div class="col-3"><strong>B.</strong>&nbsp;<label><?= $questions_dataset[$i]->option_2?></label></div>
                <div class="col-3"><strong>C.</strong>&nbsp;<label><?= $questions_dataset[$i]->option_3?></label></div>
                <div class="col-3"><strong>D.</strong>&nbsp;<label><?= $questions_dataset[$i]->option_4?></label></div>
                </div>
                </td>
                <td style="width:18%">
                <a class="btn btn-sm btn-secondary btn-bold "  href="{{ url('questions/show/'.$questions_dataset[$i]->id . '/' . $inst_type_id . '/' . $master_institute_id . '/' . $radio)}}" >
                <span class="kt-font-secondary">Show</span></a>
                <a class="btn btn-sm btn-secondary btn-bold" 
                href="{{ url('questions/edit/'.$questions_dataset[$i]->id . '/' . $inst_type_id . '/' . $master_institute_id . '/' . $radio)}}">
                <span class="kt-font-primary">Edit</span></a>
                <!-- <a class="btn btn-sm btn-secondary btn-bold delete-button"
                data-id="$datasetFilterData[$i]">
                <span class="kt-font-danger">Delete</span></a> -->
                
                </td>
                <td style="width:15%">
                @if ($questions_dataset[$i]->status === 1)
                
                <form action="questions/disable/{{$questions_dataset[$i]->id}}/{{session('inst_type_id')}}/{{$master_institute_id}}/{{$radio}}" method="POST">
                @csrf
                <span style="float:left;padding: 6px;" class="kt-font-success kt-font-bold"> &nbsp;Active  </span>  
                <span style="float:right"> <input type="submit" Value="Disable" class="btn btn-sm btn-danger btn-bold disable-button" 
                name="Disable">  </span>  
                </form>
                @else
                <form action="questions/enable/{{$questions_dataset[$i]->id}}/{{session('inst_type_id')}}/{{$master_institute_id}}/{{$radio}}" method="POST">    
                @csrf
                <span style="float:left;padding: 6px;" class="kt-font-danger kt-font-bold"> In-Active  </span>  
                <span style="float:right"> <input type="submit" Value="Enable" class="btn btn-sm btn-success btn-bold disable-button" name="Enable">  </span>  
                </form>
                @endif
                </td>
                </tr>
                <?php endfor ?> 
                @endif
                </tbody>
                </table> <!--end: Datatable -->
                </div>
                </div><!-- end:: Content -->
                
                @endsection
                @section('custom-script-content')
                
                <script>
                "use strict";
                jQuery(document).ready(function() {
                    
                    $('#master_institute_dropdown').on('change', function(e){
                        $(this).closest('form').submit();
                    });
                    
                    $("input[name='MyRadio']").on('click',function(){
                        var selValue = $(this).attr("value");
                        var targetBox = $("."+ selValue);
                        console.log(selValue+"  "+targetBox);
                        $(".box").not(targetBox).hide();
                        $(targetBox).show();
                        console.log(selValue);
                        var FIrst = "<?php echo session('first')?>";
                        var SEcond = "<?php echo session('second')?>";
                        $("#MyRadio").val(selValue).change();
                        $(this).closest('form').submit();
                    });
                    
                    // DropDown Change for filter 
                    $('#dropdown_institute_types').on('change', function(e){
                        console.log('hi dropdown_institute_types');
                        $(this).closest('form').submit();
                    });

                    // DropDown Change for filter 
                    $('#dropdown_parameters').on('change', function(e){
                        $(this).closest('form').submit();
                    });
                    
                      // CRUD - Add info
    $('.add-button2').click(function(e) {
        var inst_id = "<?php echo session('inst_type_id')?>";
        var param_id="<?php echo session('parameter_id')?>";
        var mst_inst_id = "<?php echo session('master_institute_id')?>";;
        var radio = "<?php echo session('radio')?>";;
        var mst_inst_name = "";;
        if (mst_inst_id != null && mst_inst_id != ''){
            mst_inst_id = "<?php echo session('master_institute_id')?>";
            mst_inst_name = $("#master_institute_dropdown option:selected").text();
        }
        if (mst_inst_name != ''){
            $("#txTitle").text('Add New Questions to ' + mst_inst_name);
        }else{
            $("#txTitle").text('Add New Questions to Gold Copy');
        }

        $(this).closest('form').submit();
    });
                   
                    // CRUD - Delete info
                    $('.delete-button').click(function(e) {
                        var record = $(this).data('id');
                        var inst_id = "<?php echo session('inst_id')?>";
                        $('#form-modal-confirm').attr('action', 'institute/delete/' + record. inst_id + '/' +  inst_id);
                        $('#modal-CONFIRM').modal('show');
                    });
                    
                    // CRUD - Update info
                    $('.update-button').click(function(e) {
                        var record = $(this).data('id');
                        var inst_id = "<?php echo session('inst_id')?>";
                        $("#txt_name").val(record.institute_name);
                        $('#txt_name').prop('disabled', false);
                        $("#txt_address").val(record.address);
                        $('#txt_address').prop('disabled', false);
                        $("#txt_city").val(record.city);
                        $('#txt_city').prop('disabled', false);
                        $("#txt_state").val(record.state);
                        $('#txt_state').prop('disabled', false);
                        $("#txt_country").val(record.country);
                        $('#txt_country').prop('disabled', false);
                        $("#txt_pincode").val(record.pincode);
                        $('#txt_pincode').prop('disabled', false);
                        
                        
                        $("#txt_firstname").val(record.first_name);
                        $('#txt_firstname').prop('disabled', false);
                        $("#txt_lastname").val(record.last_name);
                        $('#txt_lastname').prop('disabled', false);
                        $("#txt_email").val(record.email);
                        $('#txt_email').prop('disabled', false);
                        $("#txt_contact").val(record.contact);
                        $('#txt_contact').prop('disabled', false);
                        
                        
                        $("#modal_dropdown_institute_types").val(inst_id).change();
                        $("#modal_dropdown_institute_types").prop('disabled', true);
                        $("#txTitle").text('Update Institute Details');
                        $('#btnSave').show();
                        $('#form-modal-info').attr('action', 'institute/instituteupdate/' + record.inst_id + '/' +  inst_id);
                        $('#modal-CRUD').modal('show');
                    });
                    
                    // CRUD - Show info
                    
                    
                    // CRUD - Reset Form
                    $('#modal-CRUD').on('hide.bs.modal', function() {
                        $("#form-modal-info").validate().resetForm();
                    });
                    
                    // Form validations (client side)
                    $('#form-modal-info').validate({
                        rules: {
                            txt_name: {
                                required: true,
                                minlength:3
                            },
                            txt_username: {
                                required: true,
                                minlength:2
                            },
                            txt_email: {
                                required: true,
                                email:true
                            },
                            txt_mobile: {
                                required: true,
                                minlength:10,
                                maxlength:10
                                
                            },
                            txt_address: {
                                required: true,
                                
                            },
                            
                        }
                    });
                });
                
                </script>
                
                @endsection
                