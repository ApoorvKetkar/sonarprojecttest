@extends('layouts.master')

@section('title', 'Questions - ')

@section('page-content')

<!-- begin:: Top Div -->
<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

    <!-- begin:: Subheader -->
    <div class="kt-subheader kt-grid__item" id="kt_subheader">
        <div class="kt-container kt-container--fluid kt-margin-t-10 kt-margin-b-10">

           <!--  <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">Add/Update Master Institute</h3>

            </div> -->
        </div>
    </div>
    <!-- end:: Subheader -->

    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" >
        <form class="kt-form kt-form--label-right" id="form-modal-info" 
action="../../../../questions/questionupdate/{{$questions_dataset[0]->id}}/{{$inst_type_id}}/{{+$master_institute_id}}/{{$radio}}" method="POST">
            <!--begin::Form-->
            @csrf
            <div class="form-group row">

                <div class="col-sm-12"> 
                    <h5 class="kt-subheader__title"> Update Questions Details</h5>
                </div>

                <div class="col-sm-12">
                 <label class="col-form-label">Institute Type</label>
                 <select class="form-control" name="modal_dropdown_institute_types" id="modal_dropdown_institute_types" disabled>
                    <option value="-1" {{ ( +$inst_type_id  == -1 ) ? 'selected' : '' }}>-- Select Intitute Type --</option>
                    <?php for($i = 0; $i < count($dataset2); $i++) : ?>                           
                        <option value="{{ $dataset2[$i]->id }}" {{ ( +$inst_type_id  == $dataset2[$i]->id ) ? 'selected' : '' }}>{{$dataset2[$i]->inst_type_name}}
                        </option>
                    <?php endfor ?>
                </select>   
            </div>
            <div class="col-sm-12">

                <label class="col-form-label">Choose Parameter Name</label>
                <select class="form-control" name="dropdown_parameters" id="dropdown_parameters">
                   <!--  <option value="-1">-- All Paramters --</option> -->
                    <?php for($i = 0; $i < count($parameter_dataset); $i++) : ?> <option value="{{ $parameter_dataset[$i]->id }}"  
                    {{( $parameter_dataset[$i]->id == $parameter_id ) ? 'selected' : '' }} >{{$parameter_dataset[$i]->parameter_name}}
                 </option>
             <?php endfor ?>
         </select> 
     </div>
         @if(!empty($questions_dataset))
       <?php for($i = 0; $i < count($questions_dataset); $i++) : ?> 
    <div class="col-sm-12">
        <label class="col-form-label">Question Details</label>
        <input id="txt_question_desc" name="txt_question_desc" type="text" class="form-control"  autocomplete="off" value="{{$questions_dataset[$i]->questions}}" pattern = "^(?![\s.]+$)[.0-9a-zA-Z\s,-/']+$">
    </div>
    <div class="col-sm-12">
        <label class="col-form-label">Option 1</label>
        <input id="text_option1" name="text_option1" autocomplete="off" type="text" class="form-control"  value="{{$questions_dataset[$i]->option_1}}" pattern = "^(?![\s.]+$)[.0-9a-zA-Z\s,-/']+$">
    </div>
    <div class="col-sm-12">
        <label class="col-form-label">Option 2</label>
        <input id="text_option2" name="text_option2" autocomplete="off" type="text" class="form-control"  value="{{$questions_dataset[$i]->option_2}}" pattern = "^(?![\s.]+$)[.0-9a-zA-Z\s,-/']+$">
    </div>
    <div class="col-sm-12">
        <label class="col-form-label">Option 3</label>
        <input id="text_option3" name="text_option3" autocomplete="off" type="text" class="form-control" value="{{$questions_dataset[$i]->option_3}}" pattern = "^(?![\s.]+$)[.0-9a-zA-Z\s,-/']+$">
    </div>
    <div class="col-sm-12">
        <label class="col-form-label">Option 4</label>
        <input id="text_option4" name="text_option4" autocomplete="off" type="text" class="form-control"  value="{{$questions_dataset[$i]->option_4}}" pattern = "^(?![\s.]+$)[.0-9a-zA-Z\s,-/']+$">
    </div>
    <div class="col-sm-12" style="text-align:right; margin-top:10px; ">
        
      <button  type="button" class="btn btn-secondary" onclick="window.location='{{ url("questions/questions") }}'">Close</button>
       &nbsp; &nbsp; &nbsp;<button  type="submit" class="btn btn-primary">Save Info</button>
   </div>
     <?php endfor ?> 
        @endif 



</form>


<!--end::Form-->


</div>
<!-- end:: Content -->

</div>
<!-- end:: Top Div -->



@endsection

@section('custom-script-content')

<script>
"use strict";
jQuery(document).ready(function() {

    // DropDown Change for filter 
    // $('#dropdown_institute_types').on('change', function(e){
    //     console.log('hi dropdown_institute_types');

    //     $(this).closest('form').submit();
       
    // });
   // // DropDown Change for filter 
   //  $('#dropdown_parameters').on('change', function(e){

   //      $(this).closest('form').submit();
        
   //  });
     
  

    // Form validations (client side)
    $('#form-modal-info').validate({
        rules: {
            txt_name: {
                required: true,
                minlength:2,
                maxlength=200
               
            },
           txt_question_desc: {
                required:true,
                 minlength:2,
                  maxlength=200
               
            },
            text_option1:{
                 required:true,
                  minlength:2,
                 maxlength=200
                
             },
              text_option2:{
                 required:true,
                  minlength:2,
                  maxlength=200
                 
            },
             text_option3:{
                 required:true,
                  minlength:2,
                 maxlength=200
                  
            },
             text_option4:{
                 required:true,
                 minlength:2,
                maxlength=200
               

            }

        }
    });
   });

</script>


@endsection





