@extends('layouts.master')

@section('title', 'Questions - ')
 
@section('page-content')

<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">


<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    
   
        <div class="kt-portlet__body">
            
            <form class="kt-form kt-form--label-right" action="questions/show/{{$questions_dataset}}/{{session('master_institute_id')}}/{{session('radio')}}" method="GET">
                @csrf
 <!--begin::Form-->

        <div class="form-group row">
    
            <div class="col-sm-12"> 
                <h5 class="kt-subheader__title">View Questions Details</h5>
            </div>

             <div class="col-sm-12">
                 <label class="col-form-label">Institute Type</label>
                 <select class="form-control" name="modal_dropdown_institute_types" id="modal_dropdown_institute_types" disabled>
                    <option value="-1" {{ ( +$inst_type_id  == -1 ) ? 'selected' : '' }}>-- Select Intitute Type --</option>
                    <?php for($i = 0; $i < count($dataset2); $i++) : ?>                           
                        <option value="{{ $dataset2[$i]->id }}" {{ ( +$inst_type_id  == $dataset2[$i]->id ) ? 'selected' : '' }}>{{$dataset2[$i]->inst_type_name}}
                        </option>
                    <?php endfor ?>
                </select>   
            </div>
              <div class="col-sm-12">

                <label class="col-form-label">Choose Parameter Name</label>
                <select class="form-control" name="dropdown_parameters" id="dropdown_parameters" disabled>
                   
                    <?php for($i = 0; $i < count($questions_dataset); $i++) : ?> <option value="{{ $questions_dataset[$i]->id }}"  
                    {{( $questions_dataset[$i]->id ==$parameter_id ) ? 'selected' : '' }} >{{$questions_dataset[$i]->parameter_name}}
                 </option>
             <?php endfor ?>
         </select> 
     </div>
           

           

             @if(!empty($questions_dataset))
       <?php for($i = 0; $i < count($questions_dataset); $i++) : ?> 
    <div class="col-sm-12">
        <label class="col-form-label">Question Details</label>
        <input id="txt_question_desc" name="txt_question_desc" type="text" class="form-control" maxlength="50" autocomplete="off" value="{{$questions_dataset[$i]->questions}}" disabled>
    </div>
    <div class="col-sm-12">
        <label class="col-form-label">Option 1</label>
        <input id="text_option1" name="text_option1" autocomplete="off" type="text" class="form-control" maxlength="50" value="{{$questions_dataset[$i]->option_1}}" disabled>
    </div>
    <div class="col-sm-12">
        <label class="col-form-label">Option 2</label>
        <input id="text_option2" name="text_option2" autocomplete="off" type="text" class="form-control" maxlength="50" value="{{$questions_dataset[$i]->option_2}}" disabled>
    </div>
    <div class="col-sm-12">
        <label class="col-form-label">Option 3</label>
        <input id="text_option3" name="text_option3" autocomplete="off" type="text" class="form-control" maxlength="50" value="{{$questions_dataset[$i]->option_3}}" disabled>
    </div>
    <div class="col-sm-12">
        <label class="col-form-label">Option 4</label>
        <input id="text_option4" name="text_option4" autocomplete="off" type="text" class="form-control" maxlength="50" value="{{$questions_dataset[$i]->option_4}}" disabled>
    </div>
    <div class="col-sm-12" style="text-align:right; margin-top:10px; ">

       &nbsp; &nbsp; &nbsp;<button  type="button" class="btn btn-secondary" onclick="window.location='{{ url("questions/questions") }}'">Close</button>
   </div>
     <?php endfor ?> 
        @endif 


          

         
         
           

            
           

          

          
           
    
    </form>
    <!--end::Form-->

            </form>


        </div>
    </div>
</div>
<!-- end:: Content -->

</div>

@endsection
@section('custom-script-content')

<script>
"use strict";
jQuery(document).ready(function() {

    // DropDown Change for filter 
    $('#dropdown_institute_types').on('change', function(e){
        $(this).closest('form').submit();
    });

    // DropDown Change for filter 
    $('.add-button').click(function(e) {
        $(this).closest('form').submit();
    });

    // CRUD - Add info
    $('.add-button1').click(function(e) {
        var inst_id = "<?php echo session('inst_id')?>";

        //console.log("Inst Id : " + inst_id);
        $("#txTitle").text('Add New Institute');

// Institute Details
        $("#txt_name").val("");
        $('#txt_name').prop('disabled', false);
        $("#txt_address").val("");
        $('#txt_address').prop('disabled', false);
        $("#txt_city").val("");
        $('#txt_city').prop('disabled', false);
        $("#txt_state").val("");
        $('#txt_state').prop('disabled', false);
        $("#txt_country").val("");
        $('#txt_country').prop('disabled', false);
        $("#txt_pincode").val("");
        $('#txt_pincode').prop('disabled', false);


//  User Details
        $("#txt_firstname").val("");
        $('#txt_firstname').prop('disabled', false);
        $("#txt_lastname").val("");
        $('#txt_lastname').prop('disabled', false);
        $("#txt_email").val("");
        $('#txt_email').prop('disabled', false);
        $("#txt_contact").val("");
        $('#txt_contact').prop('disabled', false);

      
        $("#modal_dropdown_institute_types").val(inst_id).change();
        $("#modal_dropdown_institute_types").prop('disabled', true);
        $('#btnSave').show();
        $('#form-modal-info').attr('action', 'institute/institutestore/'+ inst_id);
        $('#modal-CRUD').modal('show');
        
    });

    // CRUD - Delete info
    $('.delete-button').click(function(e) {
        var record = $(this).data('id');
        var inst_id = "<?php echo session('inst_id')?>";
        $('#form-modal-confirm').attr('action', 'institute/delete/' + record. inst_id + '/' +  inst_id);
        $('#modal-CONFIRM').modal('show');
    });

    // CRUD - Update info
    $('.update-button').click(function(e) {
        var record = $(this).data('id');
        var inst_id = "<?php echo session('inst_id')?>";
        $("#txt_name").val(record.institute_name);
        $('#txt_name').prop('disabled', false);
        $("#txt_address").val(record.address);
        $('#txt_address').prop('disabled', false);
        $("#txt_city").val(record.city);
        $('#txt_city').prop('disabled', false);
        $("#txt_state").val(record.state);
        $('#txt_state').prop('disabled', false);
        $("#txt_country").val(record.country);
        $('#txt_country').prop('disabled', false);
        $("#txt_pincode").val(record.pincode);
        $('#txt_pincode').prop('disabled', false);
        

        $("#txt_firstname").val(record.first_name);
        $('#txt_firstname').prop('disabled', false);
        $("#txt_lastname").val(record.last_name);
        $('#txt_lastname').prop('disabled', false);
        $("#txt_email").val(record.email);
        $('#txt_email').prop('disabled', false);
        $("#txt_contact").val(record.contact);
        $('#txt_contact').prop('disabled', false);

        $("#txt_username").val(record.contact);
        $('#txt_username').prop('disabled', false);
        $("#txt_password").val(record.contact);
        $('#txt_password').prop('disabled', false);

       
        $("#modal_dropdown_institute_types").val(inst_id).change();
        $("#modal_dropdown_institute_types").prop('disabled', true);
        $("#txTitle").text('Update Institute Details');
        $('#btnSave').show();
        $('#form-modal-info').attr('action', 'institute/instituteupdate/' + record.inst_id + '/' +  inst_id);
        $('#modal-CRUD').modal('show');
    });

    // CRUD - Show info
    $('.show-button').click(function(e) {
        var record = $(this).data('id');
        var inst_id = "<?php echo session('inst_id')?>";
        $("#txt_name").val(record.institute_name);
        $('#txt_name').prop('disabled', true);
        $("#txt_address").val(record.address);
        $('#txt_address').prop('disabled', false);
        $("#txt_city").val(record.city);
        $('#txt_city').prop('disabled', false);
        $("#txt_state").val(record.state);
        $('#txt_state').prop('disabled', false);
        $("#txt_country").val(record.country);
        $('#txt_country').prop('disabled', false);
        $("#txt_pincode").val(record.pincode);
        $('#txt_pincode').prop('disabled', false);

        $("#txt_firstname").val(record.first_name);
        $('#txt_firstname').prop('disabled', false);
        $("#txt_lastname").val(record.last_name);
        $('#txt_lastname').prop('disabled', false);
        $("#txt_email").val(record.email);
        $('#txt_email').prop('disabled', false);
        $("#txt_contact").val(record.contact);
        $('#txt_contact').prop('disabled', false);

       

        $("#modal_dropdown_institute_types").val(inst_id).change();
        $("#modal_dropdown_institute_types").prop('disabled', true);
        $('#btnSave').hide();
        $("#txTitle").text('View Institute Details');
        $('#modal-CRUD').modal('show');
    });

    // CRUD - Reset Form
    $('#modal-CRUD').on('hide.bs.modal', function() {
        $("#form-modal-info").validate().resetForm();
    });

    // Form validations (client side)
    $('#form-modal-info').validate({
        rules: {
            txt_name: {
                required: true,
                minlength:2
            },
          
            txt_email: {
                required: true,
                email:true
            },
            txt_mobile: {
                required: true,
                minlength:10,
                maxlength:10
               
            },
            txt_address: {
                required: true,
               
            },
            txt_city: {
                required: true,
               
            },
            txt_state: {
                required: true,
               
            },
            txt_country: {
                required: true,
               
            },
            txt_pincode: {
                required: true,
               
            },
            txt_firstname: {
                required: true,
               
            },
            txt_lastname: {
                maxlength:30,
               
            },
            txt_email: {
                required: true,
               
            },
            txt_contact: {
                required: true,
               
            },
            txt_username: {
                required: true,
               
            },
            txt_password: {
                required: true,
               
            },
           
        }
    });
   });

</script>

@endsection


