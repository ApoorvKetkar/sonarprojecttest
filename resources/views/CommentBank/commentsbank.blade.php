@extends('layouts.master')

@section('title', 'Comments Bank - ')
 
@section('page-content')

<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

<!-- begin:: Subheader -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid kt-margin-t-10 kt-margin-b-10">
       
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Comments Bank</h3>
        </div>
        
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            
           
                <div class="form-group row">

                    
                    <div class="col-sm-6">
                         <form class="kt-form kt-form--label-right" action="{{url('commentbank/getParameters')}}"
                         method="POST">
                          @csrf
     <label class="col-form-label pt-0 kt-font-bold" style="padding-top:6px !important;">Choose Entity Type <span class="kt-font-danger">*</span></label>
                          <select class="form-control" name="dropdown_institute_types" id="dropdown_institute_types">
                          <option value="-1" {{ ( +$inst_type_id == -1 ) ? 'selected' : '' }}>-- Select Entity Types --</option>
                          <?php
for ($i = 0; $i < count($insttype_dataset); $i++):
?>                           
                            <option value="{{ $insttype_dataset[$i]->id }}"  
                                {{( $insttype_dataset[$i]->id == $inst_type_id ) ? 'selected' : '' }}
                                >{{$insttype_dataset[$i]->inst_type_name}}
                            </option>
                        <?php
endfor;
?>
                    </select>
                </form> 
                    </div>
                    <div class="col-sm-6">
                        <form  action="{{url('commentbank/filtercommentsbank/'.$inst_type_id)}}"  method="POST" class="kt-form kt-form--label-right">


              @csrf
                        <label class="col-form-label pt-0 kt-font-bold" style="padding-top:6px !important;">Choose Parameter Name <span class="kt-font-danger">*</span></label>
                        <select class="form-control" name="dropdown_parameters" id="dropdown_parameters">
                <option value="-1">-- Select Parameter Name --</option>
                <?php
for ($i = 0; $i < count($parameter_dataset); $i++):
?>    <option value="{{ $parameter_dataset[$i]->id }}"  
                    {{( $parameter_dataset[$i]->id == $parameter_id ) ? 'selected' : '' }}>{{$parameter_dataset[$i]->parameter_name}}
                </option>
            <?php
endfor;
?>
        </select> 
                    </div>
                </div>
                   
            </form>
            @if(empty($show_comments) || $show_comments == 0)
            <ul class="nav nav-tabs" role="tablist" style="display:none;">
            @else
            <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg kt-separator--portlet-fit mt-0"></div>
            <ul class="nav nav-tabs" role="tablist">
            @endif
                <li class="nav-item">
                  @if($show_comments == 1)
                  <a class="nav-link active kt-shape-font-color-4" data-toggle="tab" href="#tab_basecomments">
                    <i class="fa fa-comment-alt kt-shape-font-color-4"></i> Base Comments
                  </a>
                  @else
                  <a class="nav-link kt-shape-font-color-4" data-toggle="tab" href="#tab_basecomments">
                    <i class="fa fa-comment-alt kt-shape-font-color-4"></i> Base Comments
                  </a>
                  @endif
                </li>
                <li class="nav-item">
                  @if($show_comments == 2)
                  <a class="nav-link active kt-shape-font-color-4" data-toggle="tab" href="#tab_relatedcomments">
                    <i class="fa fa-comments kt-shape-font-color-4"></i> Related Parameters Comments
                  </a>
                  @else
                  <a class="nav-link kt-shape-font-color-4" data-toggle="tab" href="#tab_relatedcomments">
                    <i class="fa fa-comments kt-shape-font-color-4"></i> Related Parameters Comments
                  </a>
                  @endif
                </li>
            </ul>
            <div class="tab-content">
               
                @if($show_comments == 1)
                <div class="tab-pane active" id="tab_basecomments" role="tabpanel">
                @else
                <div class="tab-pane" id="tab_basecomments" role="tabpanel">
                @endif
                   
                    <!--begin::Form-->
                    <form id="form-basecomments-data" class="kt-form" action="{{url('commentbank/create/'.$inst_type_id)}}"  method="POST">
                        @csrf
                        <input type="hidden"  name="_count" value="<?php echo count($commentsbank_dataset);
?>">
                        @csrf
                        <input type="hidden"  name="_parameterid" value="{{$parameter_id}}">

                        @csrf
                        <div class="form-group row">
                          <?php for ($i = 0; $i < count($commentsbank_dataset); $i++): ?>
                            <div class="col-md-1 col-3">
                                <label class="col-form-label pt-0 pb-0">&nbsp;</label>
                                <h2><span class="badge badge-light">{{$commentsbank_dataset[$i]->grade_desc}}</span></h2>
                            </div>
                            <div class="col-md-5 col-9">
                                <label class="col-form-label pt-0 pb-0">&nbsp;</label>
                                <input type="hidden"  name="_id{{$i}}" value="{{$commentsbank_dataset[$i]->id}}">
                                <textarea type="textarea"  name="txt_desc{{$i}}" class="form-control textbox" rows="3" maxlength="1000" required>{{$commentsbank_dataset[$i]->comment_desc}}</textarea>
                            </div>
                            <?php
endfor;
?>
                        </div>
                            
                        <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg kt-separator--portlet-fit"></div>
                        
                        <div class="form-group row text-right">
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary">Save Info</button>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                
                @if($show_comments == 2)
                <div class="tab-pane active" id="tab_relatedcomments" role="tabpanel">
                  
                @else
                <div class="tab-pane" id="tab_relatedcomments" role="tabpanel">
                  
                @endif
                
                  
                   <!--begin::Form-->
                  
                      <!--begin::Portlet-->
                        <div class="kt-portlet" style="box-shadow:0px 0px 13px 0px rgba(82, 103, 105, 0.25);">
                            <div class="kt-portlet__body">
                                <div class="kt-portlet__content">
                                 
                                    <div class="row">
                               
                                      <div class="col-3">

                                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                          <!-- <?php
for ($i = 0; $i < count($relatedparameter_dataset); $i++):
?> 
                                          @if($relatedparameter_dataset[$i]->related_parameter_id == $related_parameter_id)
                                          <a class="nav-link active" data-toggle="pill" 
                                          href="{{url('commentbank/getRelatedParameterComments/'.$inst_type_id.'/'.$parameter_id.'/'.$relatedparameter_dataset[$i]->related_parameter_id)}}" role="tab" aria-selected="true">{{$relatedparameter_dataset[$i]->parameter_name}}</a>
                                          @else
                                          <a class="nav-link" data-toggle="pill" href="{{url('commentbank/getRelatedParameterComments/'.$inst_type_id.'/'.$parameter_id.'/'.$relatedparameter_dataset[$i]->related_parameter_id)}}" role="tab" aria-selected="true">{{$relatedparameter_dataset[$i]->parameter_name}}</a>
                                          @endif
                                          <?php
endfor;
?> -->

                                          <?php
for ($i = 0; $i < count($relatedparameter_dataset); $i++):
?> 
                                          @if($relatedparameter_dataset[$i]->related_parameter_id == $related_parameter_id)
                                          <a class="nav-link active" href="{{url('commentbank/getRelatedParameterComments/'.$inst_type_id.'/'.$parameter_id.'/'.$relatedparameter_dataset[$i]->related_parameter_id).'/2'}}">{{$relatedparameter_dataset[$i]->parameter_name}}</a>
                                          @else
                                          <a class="nav-link" href="{{url('commentbank/getRelatedParameterComments/'.$inst_type_id.'/'.$parameter_id.'/'.$relatedparameter_dataset[$i]->related_parameter_id.'/2')}}">{{$relatedparameter_dataset[$i]->parameter_name}}</a>
                                          @endif
                                          <?php endfor;?>


                                          <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#tab_new" role="tab" aria-controls="v-pills-settings" aria-selected="false"><i class="fa fa-plus-circle mr-2"></i>Add New</a>
                                         
                                        </div>

                                      </div>
                                      <div class="col-9">
                                        <div class="tab-content" id="v-pills-tabContent">
                                         
                                          <?php
                                              for ($i = 0; $i < count($relatedparameter_dataset); $i++):
                                           ?>
                                            @if($i == 0)
                                            <div class="tab-pane fade show active" id="{{$related_parameter_id}}" role="tabpanel">
                                            @else
                                            <div class="tab-pane fade show" id="{{$related_parameter_id}}" role="tabpanel">
                                            @endif
                                          

                                              <form id="form-basecomments-data" class="kt-form" action="{{url('commentbank/storerelatedparameter')}} "  method="POST">


                                     @csrf
                         <input type="hidden"  name="_count" value="<?php echo count($commentsbank_dataset); ?>">
                        @csrf
                         <input type="hidden"  name="_parameterid" value="{{$parameter_id}}">
                          @csrf
                        <input type="hidden"  name="_relatedparameterid" value="{{$related_parameter_id}}">

                        @csrf

                                            <div class="row">
                                                 
                                            <input type="hidden"  name="_totalgrades" value="{{count($relatedparameter_comments)}}">
                                            <?php
                                           for ($j = 0; $j < count($relatedparameter_comments); $j++):
?>
                                                  <input type="hidden"  name="_id{{$j}}" value="{{$relatedparameter_comments[$j]->id}}">
                                                  <input type="hidden"  name="_grade_id{{$j}}" value="{{$relatedparameter_comments[$j]->grade_id}}">
                                                <div class="col-sm-6 mb-3">
                                                    <div class="card">
                                                      <div class="card-header kt-font-bolder">Grade<span class="kt-font-bold ml-2" style="font-size:1.25em;">{{$relatedparameter_comments[$j]->grade}}</span></div>

                                                      <div class="card-body">
                                                       
                                                      <textarea type="textarea" class="form-control" rows="4" name="txt_desc{{$j}}">{{$relatedparameter_comments[$j]->comment_desc}}</textarea>
                                                      </div>
                                                      </div>
                                                 </div>
                                            <?php
                                        endfor;
                                        ?>



                                        </div>

                                          <div class="form-group row text-right">
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary">Save Info</button>
                            </div>
                        </div>        
                                          </div>
                      </form>     
                                          <?php
                                               endfor;
                                            ?>
                                          

                                           
                                           
                                    <div class="tab-pane fade show" id="tab_new" role="tabpanel">
                            <form id="form-basecomments-data" class="kt-form" action="{{url('commentbank/insertrelatedparameter')}} "  method="POST">
                                @csrf

                                 <input type="hidden"  name="_count" value="<?php echo count($grade_dataset); ?>">
                            @csrf
                              <input type="hidden"  name="_parameterid" value="{{$parameter_id}}">
                                        <div class="row">
                                             <div class="col-sm-6 mb-4">
                                                    <select class="form-control kt-font-bold" name="dropdown_new_related_parameter" id="dropdown_new_related_parameter">
                                                     <option value="-1" {{ ( +$parameter_id  == -1 ) ? 'selected' : '' }}>-- Select Entity Type --</option>
                                                      <?php
                                                      for ($i = 0; $i < count($new_related_parameters); $i++):
                                                        ?> 

  
                                                      @if ($new_related_parameters[$i]->id != $parameter_id)
                            
                                                        <option value="{{ $parameter_dataset[$i]->id }}">{{$new_related_parameters[$i]->parameter_name}}

                                                        </option>
                                                      @endif


                                                        <?php
                                                      endfor;
                                                      ?>
                                                    </select>
                                                </div>
                                               
                                                 <div class="col-sm-6 mb-4">

                                                   

                                                 </div>
                                                   <?php for ($j = 0; $j < count($grade_dataset); $j++):?>

                                              <input type="hidden"  name="_grade_id{{$j}}" value="{{$grade_dataset[$j]->id}}">
                                               
                                                <div class="col-sm-6 mb-3">
                                                    <div class="card">
                                                        <div class="card-header kt-font-bolder">Grade is<span class="kt-font-bold ml-2" style="font-size:1.25em;">{{$grade_dataset[$j]->grade_desc}}</span></div>
                                                         <div class="card-body">
                                                              <textarea type="textarea" class="form-control" rows="4" name="txt_desc{{$j}}"></textarea>
                                                         </div>
                                                      </div>
                                               </div>
                                                 <?php
    endfor;
?>
                                     
                                             
                                          </div>
                                            <div class="form-group row text-right">
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary">Save Info</button>
                            </div>
                        </div>        
                                           <!--  <div class="kt-portlet__foot">
                                <div class="row align-items-center">
                                    <div class="col-12 kt-align-right">
                                     
                                            <button type="button" class="btn btn-primary ">Save Info</button>
                                    
                                    </div>
                                </div>
                            </div> -->
                                           
                          </form>
                                           
                                        </div>
                                      </div>
                                    </div>
                                   
                                   
                                </div>
                            </div>
                           
                        </div>
                        <!--end::Portlet-->
                       
                    </form>
                    <!--end::Form-->
                   
 
                </div>
            </div>
                                            
            
        </div>
    </div>
</div>
<!-- end:: Content -->

</div>

@endsection

@section('modal-CONFIRM-footer-content')

<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
<button type="submit" class="btn btn-danger">Yes! Update Comments Data</button>

@endsection

@section('custom-script-content')

<script>
"use strict";
jQuery(document).ready(function() {
  // DropDown Change for filter 
    $('#dropdown_institute_types').on('change', function(e){
        console.log('hi dropdown_institute_types for commentsbank');

        $(this).closest('form').submit();
       
    });
   // DropDown Change for filter 
    $('#dropdown_parameters').on('change', function(e){

         $(this).closest('form').submit();
        
    });
    
    
    $('.base-comment-button').click(function(e) {
        $('#modal-CONFIRM').modal('show');
    });
    
    $('.related-comment-button').click(function(e) {
        $('#modal-CONFIRM').modal('show');
    });

    $('#modal-CRUD').on('hide.bs.modal', function() {
        $("#form-modal-info").validate().resetForm();
    });

    // Form validations (client side)
    $('#form-modal-info').validate({
        rules: {
            txt_desc: {
                required:true
            }
        }
    });

    // $('.base-comment-button').click(function(e) {
    //     $('#modal-CONFIRM').modal('show');
    // });


   });

//    $(document).ready(function(){
//    $('textarea').keyup(function(){
//            console.log('inside on click textbox');
//           var inst_type_id = "<?php
echo session('inst_type_id');
?>";
//         var parameter_id = "<?php
echo session('parameter_id');
?>";
        
        
//         if ($ints_type_id = -1  && $parameter_id = -1){
//             alert('Please select the Institute Type and Parameter');
//         }
//     });
// });


</script>

<!-- <script>
    $(document).ready(function(){
   $('.textbox').keyup(function(){
           console.log('inside on click textbox');
          var inst_type_id = "<?php
echo session('inst_type_id');
?>";
        var parameter_id = "<?php
echo session('parameter_id');
?>";
        
        
        if ($ints_type_id = -1  && $parameter_id = -1){
            alert('Please select the Institute Type and Parameter');
        }
    });
});
</script> -->
@endsection


