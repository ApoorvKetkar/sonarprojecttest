@extends('layouts.master')

@section('title', 'Institute - ')
 
@section('page-content')

<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">


<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">Master Institute Details</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            
            <form class="kt-form kt-form--label-right" action="institute/show/{{$crud}}" method="GET">
                @csrf
 <!--begin::Form-->
 @csrf
        <div class="form-group row">
    
            <div class="col-sm-12"> 
                <h5 class="kt-subheader__title">View Master Institute Details</h5>
            </div>
@if (!empty($inst_type))
            <div class="col-sm-6">
            <label class="col-form-label">Entity Type</label>
            <select class="form-control" name="modal_dropdown_institute_types" id="modal_dropdown_institute_types" >
                                            
                        <option  readonly value="{{ $inst_type[0]->id }}" >{{$inst_type[0]->inst_type_name}}
                        </option>
                </select>   
            </div>
            @endif
            @if (!empty($crud))
            <div class="col-sm-6">
               
                <label class="col-form-label">Institute Name <span class="kt-font-danger">*</span></label>
                <input id="txt_name" name="txt_name" type="text" autocomplete="off" readonly value="{{ $crud[0]->master_institute_name }}" class="form-control" maxlength="50">
            </div>


            <div class="col-sm-6">
               
                <label class="col-form-label">Address <span class="kt-font-danger">*</span></label>
                <textarea id="txt_address" name="txt_address" type="textarea" autocomplete="off" readonly class="form-control" row="4" maxlength="50">{{ $crud[0]->address }}</textarea>
            </div>


            <div class="col-sm-6">
               
                <label class="col-form-label">City <span class="kt-font-danger">*</span></label>
                <input id="txt_city" name="txt_city" type="text" autocomplete="off" readonly value="{{ $crud[0]->city }}" class="form-control" maxlength="30">
            </div>

            <div class="col-sm-6">
               
                <label class="col-form-label">State <span class="kt-font-danger">*</span></label>
                <input id="txt_state" name="txt_state" type="text" autocomplete="off" readonly value="{{ $crud[0]->state }}" class="form-control" maxlength="30">
            </div>

            <div class="col-sm-6">
               
                <label class="col-form-label">Country <span class="kt-font-danger">*</span></label>
                <input id="txt_country" name="txt_country" type="text" autocomplete="off" readonly value="{{ $crud[0]->country }}" class="form-control" maxlength="30">
            </div>

            <div class="col-sm-6">
               
                <label class="col-form-label">Pincode <span class="kt-font-danger">*</span></label>
                <input id="txt_pincode" name="txt_pincode" type="number" autocomplete="off" readonly value="{{ $crud[0]->pincode }}"  class="form-control" maxlength="6">
            </div>
            @endif
            <div class="col-sm-12" style="margin-top:20px">
                    <h5 class="kt-subheader__title">User Details</h5>
            </div>
            <div class="col-sm-6">
               
               <label class="col-form-label">Salutation</label>
               <select class="form-control" name="dropdown_institute_types" id="dropdown_institute_types" >
                   <option value="{{$user_details[0]->salutation}}" readonly>{{$user_details[0]->salutation}}</option>
                  
               </select>   
           </div>

            <div class="col-sm-6">
               
                <label class="col-form-label">First Name <span class="kt-font-danger">*</span></label>
                <input id="txt_firstname" name="txt_firstname" type="text" readonly  value="{{ $user_details[0]->first_name }}" autocomplete="off" class="form-control" maxlength="30">
            </div>

            <div class="col-sm-6">
               
                <label class="col-form-label">Last Name <span class="kt-font-danger"></span></label>
                <input id="txt_lastname" name="txt_lastname" type="text" autocomplete="off" readonly value="{{ $user_details[0]->last_name }}" class="form-control" maxlength="30">
            </div>

            <div class="col-sm-6">
               
                <label class="col-form-label">Email ID <span class="kt-font-danger">*</span></label>
                <input id="txt_email" name="txt_email" type="text" autocomplete="off" readonly value="{{ $user_details[0]->email }}" class="form-control" maxlength="50">
            </div>

            <div class="col-sm-6">
               
                <label class="col-form-label">Mobile No <span class="kt-font-danger">*</span></label>
                <input id="txt_contact" name="txt_contact" type="number" autocomplete="off" readonly value="{{ $user_details[0]->contact }}"  class="form-control" maxlength="10">
            </div>

           

           <div class="col-sm-12" style="margin-top:20px">
                    <h5 class="kt-subheader__title">User Credentials</h5>
            </div>

            <div class="col-sm-6">
               
               <label class="col-form-label">User Name <span class="kt-font-danger">*</span></label>
               <input id="txt_username" name="txt_username" type="text" autocomplete="off" readonly value="{{ $user_credential[0]->user_name }}" class="form-control" maxlength="30">
           </div>

           <div class="col-sm-6">
               
               <label class="col-form-label">Password <span class="kt-font-danger">*</span></label>
               <input id="txt_password" name="txt_password" type="password" autocomplete="off" readonly value="{{ $user_credential[0]->password }}" class="form-control" maxlength="30">
           </div>

            <div style="margin-left:92%; margin-top:1%">
                <button id="btnClose"  type="button" class="btn close-button1 btn-secondary" onclick="window.location.href='manageinstitutes';">Close</button>
            </div>
        </div>
    </form>
    <!--end::Form-->

            </form>


        </div>
    </div>
</div>
<!-- end:: Content -->

</div>

@endsection
@section('custom-script-content')

<script>
"use strict";
jQuery(document).ready(function() {

    // DropDown Change for filter 
    $('#dropdown_institute_types').on('change', function(e){
        $(this).closest('form').submit();
    });

    // DropDown Change for filter 
    $('.add-button').click(function(e) {
        $(this).closest('form').submit();
    });

    $('.close-button1').click(function(e) {
        var record = $(this).data('id');
        var inst_id = "<?php echo session('inst_id')?>";
        $('#form-modal-confirm').attr('action', 'institute/delete/' + record. inst_id + '/' +  inst_id);
        $(this).closest('form').exit();
        $('#btnClose').exit();
      
    });

     // DropDown Change for filter 
     $('.close-button').click(function(e) {
        $(this).closest('form').close();
    });

    // CRUD - Add info
    $('.add-button1').click(function(e) {
        var inst_id = "<?php echo session('inst_id')?>";

        //console.log("Inst Id : " + inst_id);
        $("#txTitle").text('Add New Institute');

// Institute Details
        $("#txt_name").val("");
        $('#txt_name').prop('disabled', false);
        $("#txt_address").val("");
        $('#txt_address').prop('disabled', false);
        $("#txt_city").val("");
        $('#txt_city').prop('disabled', false);
        $("#txt_state").val("");
        $('#txt_state').prop('disabled', false);
        $("#txt_country").val("");
        $('#txt_country').prop('disabled', false);
        $("#txt_pincode").val("");
        $('#txt_pincode').prop('disabled', false);


//  User Details
        $("#txt_firstname").val("");
        $('#txt_firstname').prop('disabled', false);
        $("#txt_lastname").val("");
        $('#txt_lastname').prop('disabled', false);
        $("#txt_email").val("");
        $('#txt_email').prop('disabled', false);
        $("#txt_contact").val("");
        $('#txt_contact').prop('disabled', false);

      
        $("#modal_dropdown_institute_types").val(inst_id).change();
        $("#modal_dropdown_institute_types").prop('disabled', true);
        $('#btnSave').show();
        $('#form-modal-info').attr('action', 'institute/institutestore/'+ inst_id);
        $('#modal-CRUD').modal('show');
        
    });

    // CRUD - Delete info
    $('.delete-button').click(function(e) {
        var record = $(this).data('id');
        var inst_id = "<?php echo session('inst_id')?>";
        $('#form-modal-confirm').attr('action', 'institute/delete/' + record. inst_id + '/' +  inst_id);
        $('#modal-CONFIRM').modal('show');
    });

    // CRUD - Update info
    $('.update-button').click(function(e) {
        var record = $(this).data('id');
        var inst_id = "<?php echo session('inst_id')?>";
        $("#txt_name").val(record.institute_name);
        $('#txt_name').prop('disabled', false);
        $("#txt_address").val(record.address);
        $('#txt_address').prop('disabled', false);
        $("#txt_city").val(record.city);
        $('#txt_city').prop('disabled', false);
        $("#txt_state").val(record.state);
        $('#txt_state').prop('disabled', false);
        $("#txt_country").val(record.country);
        $('#txt_country').prop('disabled', false);
        $("#txt_pincode").val(record.pincode);
        $('#txt_pincode').prop('disabled', false);
        

        $("#txt_firstname").val(record.first_name);
        $('#txt_firstname').prop('disabled', false);
        $("#txt_lastname").val(record.last_name);
        $('#txt_lastname').prop('disabled', false);
        $("#txt_email").val(record.email);
        $('#txt_email').prop('disabled', false);
        $("#txt_contact").val(record.contact);
        $('#txt_contact').prop('disabled', false);

        $("#txt_username").val(record.contact);
        $('#txt_username').prop('disabled', false);
        $("#txt_password").val(record.contact);
        $('#txt_password').prop('disabled', false);

       
        $("#modal_dropdown_institute_types").val(inst_id).change();
        $("#modal_dropdown_institute_types").prop('disabled', true);
        $("#txTitle").text('Update Institute Details');
        $('#btnSave').show();
        $('#form-modal-info').attr('action', 'institute/instituteupdate/' + record.inst_id + '/' +  inst_id);
        $('#modal-CRUD').modal('show');
    });

    // CRUD - Show info
    $('.show-button').click(function(e) {
        var record = $(this).data('id');
        var inst_id = "<?php echo session('inst_id')?>";
        $("#txt_name").val(record.institute_name);
        $('#txt_name').prop('disabled', true);
        $("#txt_address").val(record.address);
        $('#txt_address').prop('disabled', false);
        $("#txt_city").val(record.city);
        $('#txt_city').prop('disabled', false);
        $("#txt_state").val(record.state);
        $('#txt_state').prop('disabled', false);
        $("#txt_country").val(record.country);
        $('#txt_country').prop('disabled', false);
        $("#txt_pincode").val(record.pincode);
        $('#txt_pincode').prop('disabled', false);

        $("#txt_firstname").val(record.first_name);
        $('#txt_firstname').prop('disabled', false);
        $("#txt_lastname").val(record.last_name);
        $('#txt_lastname').prop('disabled', false);
        $("#txt_email").val(record.email);
        $('#txt_email').prop('disabled', false);
        $("#txt_contact").val(record.contact);
        $('#txt_contact').prop('disabled', false);

       

        $("#modal_dropdown_institute_types").val(inst_id).change();
        $("#modal_dropdown_institute_types").prop('disabled', true);
        $('#btnSave').hide();
        $("#txTitle").text('View Institute Details');
        $('#modal-CRUD').modal('show');
    });


  





    // CRUD - Reset Form
    $('#modal-CRUD').on('hide.bs.modal', function() {
        $("#form-modal-info").validate().resetForm();
    });

    // Form validations (client side)
    $('#form-modal-info').validate({
        rules: {
            txt_name: {
                required: true,
                minlength:5
            },
          
            txt_email: {
                required: true,
                email:true
            },
            txt_mobile: {
                required: true,
                minlength:10,
                maxlength:10
               
            },
            txt_address: {
                required: true,
               
            },
            txt_city: {
                required: true,
               
            },
            txt_state: {
                required: true,
               
            },
            txt_country: {
                required: true,
               
            },
            txt_pincode: {
                required: true,
               
            },
            txt_firstname: {
                required: true,
               
            },
            txt_lastname: {
                maxlength:30,
               
            },
            txt_email: {
                required: true,
               
            },
            txt_contact: {
                required: true,
               
            },
            txt_username: {
                required: true,
               
            },
            txt_password: {
                required: true,
               
            },
           
        }
    });
   });

</script>

@endsection


