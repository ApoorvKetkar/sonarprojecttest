@extends('layouts.master')

@section('title', 'Institutes - ')
 
@section('page-content')

<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

<!-- begin:: Subheader -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid kt-margin-t-10 kt-margin-b-10">
       
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Institutes</h3>

        </div>
        <form action="institute/create/{{$inst_type_id}}" method="GET">
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a class="btn btn-icon- btn btn-label btn-label-primary btn-upper btn-sm btn-bold add-button">
                <i class="fa fa-plus"></i>&nbsp;&nbsp;Add New</a>
            </div>
        </div>
        </form>
        
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    
    <div class="kt-portlet kt-portlet--mobile">
        <!-- <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">Manage Parameters Information</h3>
            </div>
        </div> -->
        <div class="kt-portlet__body">
            
            <form class="kt-form kt-form--label-right" action="institute/filterdata" method="POST">
                @csrf
                <div class="form-group row">
                    <div class="col-sm-3 col-8">
                        <label class="col-form-label pt-0 kt-font-bold" style="padding-top:6px !important;">Choose Entity Type <span class="kt-font-danger">*</span></label>
                    </div>
                    <div class="col-sm-6 col-8">
                        <select class="form-control" name="dropdown_institute_types" id="dropdown_institute_types">
                        <option value="-1" {{ ( +$inst_type_id == -1 ) ? 'selected' : '' }}>-- Select Entity Type --</option>
                        <?php for($i = 0; $i < count($dataset2); $i++) : ?>                           
                            <option value="{{ $dataset2[$i]->id }}" 
                            {{ ( $dataset2[$i]->id == $inst_type_id ) ? 'selected' : '' }}
                            >{{$dataset2[$i]->inst_type_name}}</option>
                        <?php endfor ?>
                        </select>   
                    </div>
                   
                </div>
                   
                <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg kt-separator--portlet-fit"></div>
                    
            </form>

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable basicDatatables">
                <thead>
                    <tr>
                        <th style="width:5%">#</th>
                        <th>Institute Name</th>
                      <th>Institute Address</th>
                        <th style="width:15%"  class="no-sort">Actions</th>
                        <th style="width:23%"  class="no-sort">Active / Inactive</th>
                    </tr>
                </thead>
                <tbody>
                        @if(!empty($datasetFilterData))
                    <?php for($i = 0; $i < count($datasetFilterData); $i++) : ?> 
                        <tr>
                            <td style="width:5%"> <?= $i + 1 ?></td>
                            <td><?= $datasetFilterData[$i]->master_institute_name?></td>
                            <td><?= $datasetFilterData[$i]->address?></td>
                            
                        <td style="width:15%">
                            <a class="btn btn-sm btn-secondary btn-bold " href="{{ URL::to('institute/show/' . $datasetFilterData[$i]->id) }}" >
                                    <span class="kt-font-secondary">Show</span></a>
                            <a class="btn btn-sm btn-secondary btn-bold" href="{{ URL::to('institute/edit/'. $datasetFilterData[$i]->id) }}"
                                data-id="$datasetFilterData[$i]">
                                <span class="kt-font-primary">Edit</span></a>
                            <!-- <a class="btn btn-sm btn-secondary btn-bold delete-button"
                                data-id="$datasetFilterData[$i]">
                                <span class="kt-font-danger">Delete</span></a> -->
                                
                        </td>
                       
                            @if ($datasetFilterData[$i]->status === 1)
                            <form action="institute/disable/{{$datasetFilterData[$i]->id}}/{{$inst_type_id}}" method="POST">
                                @csrf
                               <td style="width:23%;vertical-align:middle;">
                                    <span class="badge badge-success mr-2">Active</span>
                                    <input type="submit" Value="Mark Inactive"
                                    class="btn btn-sm btn-secondary btn-bold disable-button" name="Disable">
                                </td>
                            </form>
                            @else
                            <form action="institute/enable/{{$datasetFilterData[$i]->id}}/{{$inst_type_id}}" method="POST">    
                                @csrf
                                 <td style="width:23%;vertical-align:middle;">
                                    <span class="badge badge-danger mr-2">Inactive</span>
                                    <input type="submit" Value="Mark Active"
                                    class="btn btn-sm btn-secondary btn-bold disable-button" name="Enable">
                                </td> 
                            </form>
                            @endif
                       
                    </tr>
                    <?php endfor ?> 
                    @endif
                   
                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
</div>
<!-- end:: Content -->

</div>

@endsection



@section('custom-script-content')

<script>
"use strict";
jQuery(document).ready(function() {

    // DropDown Change for filter 
    $('#dropdown_institute_types').on('change', function(e){
        $(this).closest('form').submit();
    });

    // DropDown Change for filter 
    $('.add-button').click(function(e) {
        $(this).closest('form').submit();
    });

    // CRUD - Add info
    $('.add-button1').click(function(e) {
        var inst_id = "<?php echo session('inst_id')?>";
        

        //console.log("Inst Id : " + inst_id);
        $("#txTitle").text('Add New Institute');

// Institute Details
        $("#txt_name").val("");
        $('#txt_name').prop('disabled', false);
        $("#txt_address").val("");
        $('#txt_address').prop('disabled', false);
        $("#txt_city").val("");
        $('#txt_city').prop('disabled', false);
        $("#txt_state").val("");
        $('#txt_state').prop('disabled', false);
        $("#txt_country").val("");
        $('#txt_country').prop('disabled', false);
        $("#txt_pincode").val("");
        $('#txt_pincode').prop('disabled', false);


//  User Details
        $("#txt_firstname").val("");
        $('#txt_firstname').prop('disabled', false);
        $("#txt_lastname").val("");
        $('#txt_lastname').prop('disabled', false);
        $("#txt_email").val("");
        $('#txt_email').prop('disabled', false);
        $("#txt_contact").val("");
        $('#txt_contact').prop('disabled', false);

      
        $("#modal_dropdown_institute_types").val(inst_id).change();
        $("#modal_dropdown_institute_types").prop('disabled', true);
        $('#btnSave').show();
        $('#form-modal-info').attr('action', 'institute/institutestore/'+ inst_id);
        $('#modal-CRUD').modal('show');
        
    });

    // CRUD - Delete info
    $('.delete-button').click(function(e) {
        var record = $(this).data('id');
        var inst_id = "<?php echo session('inst_id')?>";
        $('#form-modal-confirm').attr('action', 'institute/delete/' + record. inst_id + '/' +  inst_id);
        $('#modal-CONFIRM').modal('show');
    });

    // CRUD - Update info
    $('.update-button').click(function(e) {
        var record = $(this).data('id');
        var inst_id = "<?php echo session('inst_id')?>";
        $("#txt_name").val(record.institute_name);
        $('#txt_name').prop('disabled', false);
        $("#txt_address").val(record.address);
        $('#txt_address').prop('disabled', false);
        $("#txt_city").val(record.city);
        $('#txt_city').prop('disabled', false);
        $("#txt_state").val(record.state);
        $('#txt_state').prop('disabled', false);
        $("#txt_country").val(record.country);
        $('#txt_country').prop('disabled', false);
        $("#txt_pincode").val(record.pincode);
        $('#txt_pincode').prop('disabled', false);
        

        $("#txt_firstname").val(record.first_name);
        $('#txt_firstname').prop('disabled', false);
        $("#txt_lastname").val(record.last_name);
        $('#txt_lastname').prop('disabled', false);
        $("#txt_email").val(record.email);
        $('#txt_email').prop('disabled', false);
        $("#txt_contact").val(record.contact);
        $('#txt_contact').prop('disabled', false);

       
        $("#modal_dropdown_institute_types").val(inst_id).change();
        $("#modal_dropdown_institute_types").prop('disabled', true);
        $("#txTitle").text('Update Institute Details');
        $('#btnSave').show();
        $('#form-modal-info').attr('action', 'institute/instituteupdate/' + record.inst_id + '/' +  inst_id);
        $('#modal-CRUD').modal('show');
    });

    // CRUD - Show info
    $('.show-button').click(function(e) {
        var record = $(this).data('id');
        var inst_id = "<?php echo session('inst_id')?>";
        $("#txt_name").val(record.institute_name);
        $('#txt_name').prop('disabled', true);
        $("#txt_address").val(record.address);
        $('#txt_address').prop('disabled', false);
        $("#txt_city").val(record.city);
        $('#txt_city').prop('disabled', false);
        $("#txt_state").val(record.state);
        $('#txt_state').prop('disabled', false);
        $("#txt_country").val(record.country);
        $('#txt_country').prop('disabled', false);
        $("#txt_pincode").val(record.pincode);
        $('#txt_pincode').prop('disabled', false);

        $("#txt_firstname").val(record.first_name);
        $('#txt_firstname').prop('disabled', false);
        $("#txt_lastname").val(record.last_name);
        $('#txt_lastname').prop('disabled', false);
        $("#txt_email").val(record.email);
        $('#txt_email').prop('disabled', false);
        $("#txt_contact").val(record.contact);
        $('#txt_contact').prop('disabled', false);

       

        $("#txt_desc").val(record.para_desc);
        $('#txt_desc').prop('disabled', true);
        $("#modal_dropdown_institute_types").val(inst_id).change();
        $("#modal_dropdown_institute_types").prop('disabled', true);
        $('#btnSave').hide();
        $("#txTitle").text('View Institute Details');
        $('#modal-CRUD').modal('show');
    });

    // CRUD - Reset Form
    $('#modal-CRUD').on('hide.bs.modal', function() {
        $("#form-modal-info").validate().resetForm();
    });

    // Form validations (client side)
    $('#form-modal-info').validate({
        rules: {
            txt_name: {
                required: true,
                minlength:5
            },
            txt_username: {
                required: true,
                minlength:2
            },
            txt_email: {
                required: true,
                email:true
            },
            txt_mobile: {
                required: true,
                minlength:10,
                maxlength:10
               
            },
            txt_address: {
                required: true,
               
            },
           
        }
    });
   });

</script>

@endsection
