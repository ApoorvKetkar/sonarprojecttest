@extends('layouts.master')

@section('title', 'Institute - ')
 
@section('page-content')

<!-- begin:: Top Div -->
<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

<!-- begin:: Subheader -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid kt-margin-t-10 kt-margin-b-10">
       
        <!-- <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Add/Update Master Institute</h3>

        </div> -->
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" >

    <form class="kt-form kt-form--label-right" id="form-modal-info" action="{{ URL::to('institute/instituteupdate/' . $user->id) }}" method="POST">
        <!--begin::Form-->
        @csrf
        <div class="form-group row">
    
            <div class="col-sm-12"> 
                <h5 class="kt-subheader__title">Master Institute Details</h5>
            </div>

            <div class="col-sm-6">
            @if (!empty($dataset2) )
                <label class="col-form-label">Entity Type</label>
                <select class="form-control" name="dropdown_institute_types" id="dropdown_institute_types" disable>
                        
                        <?php for($i = 0; $i < count($dataset2); $i++) : ?>                           
                            <option value="{{ $dataset2[$i]->id }}" 
                            {{ ( $dataset2[$i]->id == $inst_type[0]->id ) ? 'selected' : '' }}
                            >{{$dataset2[$i]->inst_type_name}}</option>
                        <?php endfor ?>
                        </select>   
                @endIf  
            </div>
            @if (!empty($user))
            <div class="col-sm-6">
               
                <label class="col-form-label">Institute Name <span class="kt-font-danger">*</span></label>
                <input id="txt_name" name="txt_name" type="text" autocomplete="off" value="{{$user->master_institute_name}}" class="form-control" maxlength="50">
            </div>


            <div class="col-sm-6">
               
                <label class="col-form-label">Address <span class="kt-font-danger">*</span></label>
                <textarea id="txt_address" name="txt_address" type="textarea" autocomplete="off"  class="form-control" row="4" maxlength="50">{{$user->address}}</textarea>
            </div>


            <div class="col-sm-6">
               
                <label class="col-form-label">City <span class="kt-font-danger">*</span></label>
                <input id="txt_city" name="txt_city" type="text" autocomplete="off" value="{{$user->city}}" class="form-control" maxlength="30">
            </div>

            <div class="col-sm-6">
               
                <label class="col-form-label">State <span class="kt-font-danger">*</span></label>
                <input id="txt_state" name="txt_state" type="text" autocomplete="off" value="{{$user->state}}" class="form-control" maxlength="30">
            </div>

            <div class="col-sm-6">
               
                <label class="col-form-label">Country <span class="kt-font-danger">*</span></label>
                <input id="txt_country" name="txt_country" type="text" autocomplete="off" value="{{$user->country}}" class="form-control" maxlength="30">
            </div>

            <div class="col-sm-6">
               
                <label class="col-form-label">Pincode <span class="kt-font-danger">*</span></label>
                <input id="txt_pincode" name="txt_pincode" type="number" autocomplete="off" value="{{$user->pincode}}"  class="form-control" maxlength="6">
            </div>
            @endIf  
            <div class="col-sm-12" style="margin-top:20px">
                    <h5 class="kt-subheader__title">User Details</h5>
            </div>
            @if (!empty($user_detail))
            <div class="col-sm-6">
               
                <label class="col-form-label">Salutation</label>
                <select class="form-control" name="dropdown_institute_types" id="dropdown_institute_types" disabled >
                    <option >{{$user_detail[0]->salutation}}</option>
                </select>   
            </div>

            <div class="col-sm-6">
               
                <label class="col-form-label">First Name <span class="kt-font-danger">*</span></label>
                <input id="txt_firstname" name="txt_firstname" type="text" value="{{$user_detail[0]->first_name}}" readonly autocomplete="off" class="form-control" disabled >
            </div>

            <div class="col-sm-6">
               
                <label class="col-form-label">Last Name <span class="kt-font-danger"></span></label>
                <input id="txt_lastname" name="txt_lastname" type="text" autocomplete="off" readonly value="{{$user_detail[0]->last_name}}" class="form-control" disabled>
            </div>

            <div class="col-sm-6">
               
                <label class="col-form-label">Email ID <span class="kt-font-danger">*</span></label>
                <input id="txt_email" name="txt_email" type="text" autocomplete="off" readonly value="{{$user_detail[0]->email }}" class="form-control" disabled>
            </div>

            <div class="col-sm-6">
               
                <label class="col-form-label">Mobile No <span class="kt-font-danger">*</span></label>
                <input id="txt_contact" name="txt_contact" type="number" autocomplete="off" readonly value="{{$user_detail[0]->contact}}"  class="form-control" disabled>
            </div>
            @endIf  
           

           <div class="col-sm-12" style="margin-top:20px">
                    <h5 class="kt-subheader__title">User Credentials</h5>
            </div>
            @if (!empty($user_credintials))
            <div class="col-sm-6">
               
               <label class="col-form-label">User Name <span class="kt-font-danger">*</span></label>
               <input id="txt_username" name="txt_username" type="text" autocomplete="off" readonly value="{{$user_credintials[0]->user_name}}" class="form-control" disabled>
           </div>

           <div class="col-sm-6">
               
               <label class="col-form-label">Password <span class="kt-font-danger">*</span></label>
               <input id="txt_password" name="txt_password" type="password" autocomplete="off" readonly value="{{$user_credintials[0]->password }}" class="form-control" disabled>
           </div>
           @endIf  
            <div style="margin-left:83%; margin-top:1%">
                <button  type="button" class="btn btn-secondary" onclick="window.location.href='manageinstitutes';">Close</button>
                <button id="btnSave" type="submit" class="btn btn-primary">Save Info</button>
            </div>
        </div>
    </form>
   
    <!--end::Form-->


</div>
<!-- end:: Content -->

</div>
<!-- end:: Top Div -->



@endsection

@section('custom-script-content')

<script>
"use strict";
jQuery(document).ready(function() {

    // DropDown Change for filter 
    $('#dropdown_institute_types').on('change', function(e){
        $(this).closest('form').submit();
    });

    // DropDown Change for filter 
    $('.add-button').click(function(e) {
        $(this).closest('form').submit();
    });

    // CRUD - Add info
    $('.add-button1').click(function(e) {
        var inst_id = "<?php echo session('inst_id')?>";

        //console.log("Inst Id : " + inst_id);
        $("#txTitle").text('Add New Institute');

// Institute Details
        $("#txt_name").val("");
        $('#txt_name').prop('disabled', false);
        $("#txt_address").val("");
        $('#txt_address').prop('disabled', false);
        $("#txt_city").val("");
        $('#txt_city').prop('disabled', false);
        $("#txt_state").val("");
        $('#txt_state').prop('disabled', false);
        $("#txt_country").val("");
        $('#txt_country').prop('disabled', false);
        $("#txt_pincode").val("");
        $('#txt_pincode').prop('disabled', false);


//  User Details
        $("#txt_firstname").val("");
        $('#txt_firstname').prop('disabled', false);
        $("#txt_lastname").val("");
        $('#txt_lastname').prop('disabled', false);
        $("#txt_email").val("");
        $('#txt_email').prop('disabled', false);
        $("#txt_contact").val("");
        $('#txt_contact').prop('disabled', false);

      
        $("#modal_dropdown_institute_types").val(inst_id).change();
        $("#modal_dropdown_institute_types").prop('disabled', true);
        $('#btnSave').show();
        $('#form-modal-info').attr('action', 'institute/institutestore/'+ inst_id);
        $('#modal-CRUD').modal('show');
        
    });

    // CRUD - Delete info
    $('.delete-button').click(function(e) {
        var record = $(this).data('id');
        var inst_id = "<?php echo session('inst_id')?>";
        $('#form-modal-confirm').attr('action', 'institute/delete/' + record. inst_id + '/' +  inst_id);
        $('#modal-CONFIRM').modal('show');
    });

    // CRUD - Update info
    $('.update-button').click(function(e) {
        var record = $(this).data('id');
        var inst_id = "<?php echo session('inst_id')?>";
        $("#txt_name").val(record.institute_name);
        $('#txt_name').prop('disabled', false);
        $("#txt_address").val(record.address);
        $('#txt_address').prop('disabled', false);
        $("#txt_city").val(record.city);
        $('#txt_city').prop('disabled', false);
        $("#txt_state").val(record.state);
        $('#txt_state').prop('disabled', false);
        $("#txt_country").val(record.country);
        $('#txt_country').prop('disabled', false);
        $("#txt_pincode").val(record.pincode);
        $('#txt_pincode').prop('disabled', false);
        

        $("#txt_firstname").val(record.first_name);
        $('#txt_firstname').prop('disabled', false);
        $("#txt_lastname").val(record.last_name);
        $('#txt_lastname').prop('disabled', false);
        $("#txt_email").val(record.email);
        $('#txt_email').prop('disabled', false);
        $("#txt_contact").val(record.contact);
        $('#txt_contact').prop('disabled', false);

        $("#txt_username").val(record.contact);
        $('#txt_username').prop('disabled', false);
        $("#txt_password").val(record.contact);
        $('#txt_password').prop('disabled', false);

       
        $("#modal_dropdown_institute_types").val(inst_id).change();
        $("#modal_dropdown_institute_types").prop('disabled', true);
        $("#txTitle").text('Update Institute Details');
        $('#btnSave').show();
        $('#form-modal-info').attr('action', 'institute/instituteupdate/' + record.inst_id + '/' +  inst_id);
        $('#modal-CRUD').modal('show');
    });

    // CRUD - Show info
    $('.show-button').click(function(e) {
        var record = $(this).data('id');
        var inst_id = "<?php echo session('inst_id')?>";
        $("#txt_name").val(record.institute_name);
        $('#txt_name').prop('disabled', true);
        $("#txt_address").val(record.address);
        $('#txt_address').prop('disabled', false);
        $("#txt_city").val(record.city);
        $('#txt_city').prop('disabled', false);
        $("#txt_state").val(record.state);
        $('#txt_state').prop('disabled', false);
        $("#txt_country").val(record.country);
        $('#txt_country').prop('disabled', false);
        $("#txt_pincode").val(record.pincode);
        $('#txt_pincode').prop('disabled', false);

        $("#txt_firstname").val(record.first_name);
        $('#txt_firstname').prop('disabled', false);
        $("#txt_lastname").val(record.last_name);
        $('#txt_lastname').prop('disabled', false);
        $("#txt_email").val(record.email);
        $('#txt_email').prop('disabled', false);
        $("#txt_contact").val(record.contact);
        $('#txt_contact').prop('disabled', false);

       

        $("#modal_dropdown_institute_types").val(inst_id).change();
        $("#modal_dropdown_institute_types").prop('disabled', true);
        $('#btnSave').hide();
        $("#txTitle").text('View Institute Details');
        $('#modal-CRUD').modal('show');
    });

    // CRUD - Reset Form
    $('#modal-CRUD').on('hide.bs.modal', function() {
        $("#form-modal-info").validate().resetForm();
    });

    // Form validations (client side)
    $('#form-modal-info').validate({
        rules: {
            txt_name: {
                required: true,
                minlength:2,
                 maxlength:500
            },
          
            txt_email: {
                required:true,
                email:true
            },
          
            txt_address: {
                required:true,
                minlength:2,
                 maxlength:1000
               
            },
            txt_city: {
                required: true,
                minlength:2,
                 maxlength:500
               
            },
            txt_state: {
                required: true,
                minlength:2,
                 maxlength:500
               
            },
            txt_country: {
                required: true,
                minlength:2,
                 maxlength:500
               
            },
            txt_pincode: {
                required: true,
                digit:true,
                 maxlength:6
                
            },
            txt_firstname: {
                required: true,
                 minlength:2,
                 maxlength:100
            },
            txt_lastname: {
               maxlength:100
               
            },
                txt_contact: {
                required: true,
                minlength:10,
                maxlength:100
            },
            txt_username: {
                required: true,
                 maxlength:100
               
            },
            txt_password: {
                required:true
               
            },
           
        }
    });
   });

</script>

@endsection

