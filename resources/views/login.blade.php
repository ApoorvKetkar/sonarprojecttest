@extends('layouts.login-master')
@section('title', 'Login')
@section('page-content')                
<div class="kt-grid__item  kt-grid  kt-grid--ver  kt-grid__item--fluid">
    <!--begin::Body-->
    <div class="kt-login-v2__body">
        <!--begin::Wrapper-->
        <div class="kt-login-v2__wrapper">
            <div class="kt-login-v2__container">
                <div class="kt-login-v2__title">
                <h3>Admin Sign In</h3>
                </div>

                <!--begin::Form-->
                <!--  -->
                <form class="kt-login-v2__form kt-form" id="form-login" 
                action="login" method="post"  autocomplete="off"  >
                        @csrf
                    <div class="form-group">
                        @if (session('login-error'))
                        <!--begin:: Alert message-->
                        <div class="alert alert-danger" role="alert">
                            <div class="alert-text">{{ session('login-error') }}</div>
                        </div>
                        <!--end:: Alert message-->
                        @endif
                    </div>
                    <div class="form-group">
                    <input class="form-control" type="text" placeholder="Username" id="username" name="username" autocomplete="off">

                    </div>
                    <div class="form-group">
                        <input class="form-control" type="password" placeholder="Password" id="password" name="password" autocomplete="off">

                    </div>
                    <!--begin::Action-->
                    <div class="kt-login-v2__actions">
                        <a href="/dashboard" class="kt-link kt-link--brand">  
                            Forgot Password ?
                        </a>
                        <button id="login-button" type="submit" class="btn btn-primary">Sign In</button>
                    </div>          
                    <!--end::Action-->
                </form>
                <!--end::Form-->
            </div>
        </div>
        <!--end::Wrapper-->

        <!--begin::Image-->
        <div class="kt-login-v2__image">
            <img src="assets/media/misc/bg_icon.svg" alt="">
        </div>
        <!--begin::Image-->
    </div>
    <!--begin::Body-->
</div>
@endsection

@section('custom-script-content')
<script>
"use strict";
$(document).ready(function () {
    // $('#login-button').click(function(e) {
    //     $('#form-login').attr('action', 'login');
    // });

    $('#form-login').validate({ 
        rules: {
            username: {
                required: true
            },
            password: {
                required: true
            },
          
        }
    });
});
</script>
@endsection