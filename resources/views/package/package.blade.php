@extends('layouts.master')

@section('title', 'Packages - ')
 
@section('page-content')

<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

<!-- begin:: Subheader -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid kt-margin-t-10 kt-margin-b-10">
       
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Packages</h3>

        </div>
        
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a class="btn btn-icon- btn btn-label btn-label-primary btn-upper btn-sm btn-bold add-button">
                <i class="fa fa-plus"></i>&nbsp;&nbsp;Add New</a>
            </div>
        </div>
        
        
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    
    <div class="kt-portlet kt-portlet--mobile">
        <!-- <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">Manage Parameters Information</h3>
            </div>
        </div> -->
        <div class="kt-portlet__body">
            
            <form class="kt-form kt-form--label-right" action="package/filterdata" method="POST">
                @csrf
                <div class="form-group row">
                    <div class="col-sm-3 col-4">
                        <label class="col-form-label pt-0 kt-font-bold" style="padding-top:6px !important;">Choose Entity Type <span class="kt-font-danger">*</span></label>
                    </div>
                    <div class="col-sm-6 col-8">
                        <select class="form-control" name="dropdown_institute_types" id="dropdown_institute_types">
                        <option value="-1" {{ ( +$inst_type_id == -1 ) ? 'selected' : '' }}>-- Select Entity Type --</option>
                        <?php for($i = 0; $i < count($dataset2); $i++) : ?>                           
                            <option value="{{ $dataset2[$i]->id }}" 
                            {{ ( $dataset2[$i]->id == $inst_type_id ) ? 'selected' : '' }}
                            >{{$dataset2[$i]->inst_type_name}}</option>
                        <?php endfor ?>
                        </select>   
                    </div>
                   
                </div>
                   
                <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg kt-separator--portlet-fit"></div>
                    
            </form>

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable basicDatatables">
                <thead>
                    <tr>
                        <th style="width:5%">#</th>
                        <th> Package Name</th>
                        <th> Package Cost</th>
                        <th> Numberof Surveys</th>
                        <th> FeedBacks_Per_Teacher</th>
                        
                        <th style="width:17%"  class="no-sort">Actions</th>
                        <th style="width:23%"  class="no-sort">Active / Inactive</th>
                    </tr>
                </thead>
                <tbody>
                        @if(!empty($datasetFilterData))
                    <?php for($i = 0; $i < count($datasetFilterData); $i++) : ?> 
                        <tr>
                            <td style="width:5%"> <?= $i + 1 ?></td>
                            <td><?= $datasetFilterData[$i]->package_name?></td>
                            <td><?= $datasetFilterData[$i]->package_cost ?></td>
                            <td><?= $datasetFilterData[$i]->number_of_surveys?></td>
                            <td><?= $datasetFilterData[$i]->feedbacks_per_teacher?></td>
                        <td style="width:15%">
                            <a class="btn btn-sm btn-secondary btn-bold show-button"
                                    data-id="{{ $datasetFilterData[$i]}}">
                                    <span class="kt-font-secondary">Show</span></a>
                            <a class="btn btn-sm btn-secondary btn-bold update-button"
                                data-id="{{ $datasetFilterData[$i]}}">
                                <span class="kt-font-primary">Edit</span></a>
                           
                        </td>
                       
                            @if ($datasetFilterData[$i]->status === 1)
                            <form action="package/disable/{{$datasetFilterData[$i]->id}}/{{session('inst_id')}}" method="POST">
                                @csrf
                                <td style="width:23%;vertical-align:middle;">
                                    <span class="badge badge-success mr-2">Active</span>
                                    <input type="submit" Value="Mark Inactive"
                                    class="btn btn-sm btn-secondary btn-bold disable-button" name="Disable">
                                </td>
                            </form>
                            @else
                            <form action="package/enable/{{$datasetFilterData[$i]->id}}/{{session('inst_id')}}" method="POST">    
                                @csrf
                            
                                     <td style="width:23%;vertical-align:middle;">
                                    <span class="badge badge-danger mr-2">Inactive</span>
                                    <input type="submit" Value="Mark Active"
                                    class="btn btn-sm btn-secondary btn-bold disable-button" name="Enable">
                                </td>
                            </form>
                            @endif
                       
                    </tr>
                    <?php endfor ?> 
                    @endif
                   
                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
</div>
<!-- end:: Content -->

</div>

@endsection

<!--begin:: Layout for Modal-->
@section('modal-CRUD-header-content')

<h5 id="txTitle" class="modal-title"></h5>

@endsection

@section('modal-CRUD-body-content')

<!--begin::Form-->
<div class="form-group row">
    <div class="col-sm-12">
        <label class="col-form-label">Institute Type</label>
        <select   class="form-control" name="modal_dropdown_institute_types" id="modal_dropdown_institute_types" >
               <option value="-1" {{ ( +$inst_type_id == -1 ) ? 'selected' : '' }}>-- Select Entity Type --</option>
            <?php for($i = 0; $i < count($dataset2); $i++) : ?>                           
                <option value="{{ $dataset2[$i]->id }}" {{ ( $dataset2[$i]->id == $inst_type_id ) ? 'selected' : '' }}>{{$dataset2[$i]->inst_type_name}}
                </option>
            <?php endfor ?>
        </select>   
    </div>
    <div class="col-sm-12">
        @csrf
        <label class="col-form-label">Package Name <span class="kt-font-danger">*</span></label>
        <input id="txt_name" name="txt_name" type="text" autocomplete="off" class="form-control"  required pattern = "^(?![\s.]+$)[.0-9a-zA-Z\s,-/']+$" >
     </div>
      <div class="col-sm-12">
    
        <label class="col-form-label">Package Cost <span class="kt-font-danger">*</span></label>
        <input id="txt_package_cost" name="txt_package_cost" type="text" autocomplete="off" class="form-control" required>
     </div>
     <div class="col-sm-12">
    
        <label class="col-form-label">Number of Surveys <span class="kt-font-danger">*</span></label>
        <input id="txt_noof_surveys" name="txt_noof_surveys" type="text" autocomplete="off" class="form-control"  required>
     </div>
     <div class="col-sm-12">
    
        <label class="col-form-label">feedbacks_per_teacher <span class="kt-font-danger">*</span></label>
        <input id="txt_feedbacks_per_teacher" name="txt_feedbacks_per_teacher" type="text" autocomplete="off" class="form-control"  required>
     </div>
    
</div>
<!--end::Form-->

@endsection

@section('modal-CRUD-footer-content')

<button id="btnClose" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
<button id="btnSave" type="submit" class="btn btn-primary">Save Info</button>

@endsection

@section('modal-CONFIRM-footer-content')

<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
<button type="submit" class="btn btn-danger">Yes! Delete This Data</button>

@endsection

@section('custom-script-content')

<script>
"use strict";
$(document).ready(function() {

    // DropDown Change for filter 
    $('#dropdown_institute_types').on('change', function(e){
        $(this).closest('form').submit();
    });

    // CRUD - Add info
    $('.add-button').click(function(e) {
        var inst_id = "<?php echo session('inst_id')?>";

        //console.log("Inst Id : " + inst_id);
        $("#txTitle").text('Add New Package');
        $("#txt_name").val("");
        $('#txt_name').prop('disabled', false);

        $("#txt_package_cost").val("");
        $('#txt_package_cost').prop('disabled', false);

        $("#txt_noof_surveys").val("");
        $('#txt_noof_surveys').prop('disabled', false);

         $("#txt_feedbacks_per_teacher").val("");
        $('#txt_feedbacks_per_teacher').prop('disabled', false);
        
        $("#modal_dropdown_institute_types").val(inst_id).change();
        $("#modal_dropdown_institute_types").prop('disabled',true);
        $('#btnSave').show();
        $('#form-modal-info').attr('action', 'package/store/'+inst_id);
        $('#modal-CRUD').modal('show');
    });

    // CRUD - Delete info
    $('.delete-button').click(function(e) {
        var record = $(this).data('id');
        var inst_id = "<?php echo session('inst_id')?>";
        $('#form-modal-confirm').attr('action', 'package/delete/' + record.id + '/' +  inst_id);
        $('#modal-CONFIRM').modal('show');
    });

    // CRUD - Update info
    $('.update-button').click(function(e) {
        var record = $(this).data('id');
        var inst_id = "<?php echo session('inst_id')?>";
        $("#txt_name").val(record. package_name);
        $('#txt_name').prop('disabled', false);
        $("#txt_package_cost").val(record. package_cost);
        $('#txt_package_cost').prop('disabled', false);

         $("#txt_noof_surveys").val(record.number_of_surveys);
        $('#txt_noof_surveys').prop('disabled', false);

         $("#txt_feedbacks_per_teacher").val(record.feedbacks_per_teacher);
        $('#txt_feedbacks_per_teacher').prop('disabled', false);

        $("#modal_dropdown_institute_types").val(inst_id).change();
        $("#modal_dropdown_institute_types").prop('disabled', true);

        
        
        $("#txTitle").text('Update Package Details');
        $('#btnSave').show();
        $('#form-modal-info').attr('action', 'package/update/' + record.id + '/' +  inst_id);
        $('#modal-CRUD').modal('show');
    });

    // CRUD - Show info
    $('.show-button').click(function(e) {
        var record = $(this).data('id');
        var inst_id = "<?php echo session('inst_id')?>";
        $("#txt_name").val(record.package_name);
        $('#txt_name').prop('disabled', true);
        $("#txt_package_cost").val(record.package_cost);
        $('#txt_package_cost').prop('disabled', true);

         $("#txt_noof_surveys").val(record.number_of_surveys);
        $('#txt_noof_surveys').prop('disabled', true);
         $("#txt_feedbacks_per_teacher").val(record.feedbacks_per_teacher);
        $('#txt_feedbacks_per_teacher').prop('disabled', true);

        $("#modal_dropdown_institute_types").val(inst_id).change();
        $("#modal_dropdown_institute_types").prop('disabled', true);
        $('#btnSave').hide();
        $("#txTitle").text('View Package Details');
        $('#modal-CRUD').modal('show');
    });


    
    $.validator.addMethod('decimal', function(value, element) {
  return this.optional(element) || /^((\d+(\\.\d{0,2})?)|((\d*(\.\d{1,2}))))$/.test(value);
}, "Please enter a correct number, format 0.00");


    // CRUD - Reset Form
    $('#modal-CRUD').on('hide.bs.modal', function() {
        $("#form-modal-info").validate().resetForm();
    });
  
      
        $('#form-modal-info').validate({
        rules: {
            txt_name: {
                required: true,
                minlength:2,
                maxlength:50
            },
         txt_package_cost: {
                required:true,
                decimal:true
                },
           txt_noof_surveys: {
                required:true,
                digit:true
                
            },
          txt_feedbacks_per_teacher: {
                required:true,
                digit:true
            }
        }
    });
   });

</script>


@endsection
