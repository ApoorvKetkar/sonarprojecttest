@extends('layouts.master')

@section('title', 'Dashboard')
 
@section('page-content')

<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

<!-- begin:: Subheader -->
<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container kt-container--fluid kt-margin-t-10 kt-margin-b-10">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Dashboard</h3>
        </div>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid">

    <!--begin::Dashboard 5-->

    <!--begin::Row-->
    <div class="row">
        
        <div class="col-lg-6 col-xl-4">
            <!--begin::Portlet-->
            <div class="kt-portlet kt-portlet--height-fluid kt-widget-10" style="height:auto;">
                <div class="kt-portlet__body">
                    <div class="kt-widget-10__wrapper">
                        <div class="kt-widget-10__details">
                            <div class="kt-widget-10__title">Institutes</div>
                            <div class="kt-widget-10__desc">Total Master Institues</div>
                        </div>
                        <div class="kt-widget-10__num kt-font-primary">158</div>
                    </div>
                </div>
            </div>
            <!--end::Portlet-->
        </div>
        
        <div class="col-lg-6 col-xl-4">
            <!--begin::Portlet-->
            <div class="kt-portlet kt-portlet--height-fluid kt-widget-10" style="height:auto;">
                <div class="kt-portlet__body">
                    <div class="kt-widget-10__wrapper">
                        <div class="kt-widget-10__details">
                            <div class="kt-widget-10__title">Teachers</div>
                            <div class="kt-widget-10__desc">Total On-boarded</div>
                        </div>
                        <div class="kt-widget-10__num kt-font-danger">8695</div>
                    </div>
                </div>
            </div>
            <!--end::Portlet-->
        </div>
        
        <div class="col-lg-6 col-xl-4">
            <!--begin::Portlet-->
            <div class="kt-portlet kt-portlet--height-fluid kt-widget-10" style="height:auto;">
                <div class="kt-portlet__body">
                    <div class="kt-widget-10__wrapper">
                        <div class="kt-widget-10__details">
                            <div class="kt-widget-10__title">Feedbacks</div>
                            <div class="kt-widget-10__desc">Total Captured</div>
                        </div>
                        <div class="kt-widget-10__num kt-font-success">78798</div>
                    </div>
                </div>
            </div>
            <!--end::Portlet-->
        </div>
        
        <div class="col-12">
            
            <div class="kt-portlet kt-portlet--mobile">
            
            <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Feedbacks - Pending for your approval</h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-toolbar-wrapper">
                            <div class="dropdown dropdown-inline">
                                <button type="button" class="btn btn-primary btn-sm btn-bold">View All Feedbacks</button>
                            </div>
                        </div>
                    </div>
                </div>
        
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable basicDatatables">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Institue Name</th>
                        <th>Feedback Date</th>
                        <th>Total Contributors</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Some Institute Name</td>
                        <td>28-Jan-2020</td>
                        <td>89</td>
                        <td>
                            <a class="btn btn-sm btn-secondary btn-bold update-button">
                            <span class="kt-font-primary">Check Details</span></a>
                            <a class="btn btn-sm btn-danger btn-bold delete-button">
                            <span class="kt-font-light">Approve</span></a>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Some Institute Name</td>
                        <td>25-Jan-2020</td>
                        <td>60</td>
                        <td>
                            <a class="btn btn-sm btn-secondary btn-bold update-button">
                            <span class="kt-font-primary">Check Details</span></a>
                            <a class="btn btn-sm btn-danger btn-bold delete-button">
                            <span class="kt-font-light">Approve</span></a>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Some Institute Name</td>
                        <td>25-Jan-2020</td>
                        <td>49</td>
                        <td>
                            <a class="btn btn-sm btn-secondary btn-bold update-button">
                            <span class="kt-font-primary">Check Details</span></a>
                            <a class="btn btn-sm btn-danger btn-bold delete-button">
                            <span class="kt-font-light">Approve</span></a>
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Some Institute Name</td>
                        <td>24-Jan-2020</td>
                        <td>170</td>
                        <td>
                            <a class="btn btn-sm btn-secondary btn-bold update-button">
                            <span class="kt-font-primary">Check Details</span></a>
                            <a class="btn btn-sm btn-danger btn-bold delete-button">
                            <span class="kt-font-light">Approve</span></a>
                        </td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Some Institute Name</td>
                        <td>22-Jan-2020</td>
                        <td>68</td>
                        <td>
                            <a class="btn btn-sm btn-secondary btn-bold update-button">
                            <span class="kt-font-primary">Check Details</span></a>
                            <a class="btn btn-sm btn-danger btn-bold delete-button">
                            <span class="kt-font-light">Approve</span></a>
                        </td>
                    </tr>
                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
            
        </div>
        
    </div>

    <!--end::Row-->

    <!--end::Dashboard 5-->
</div>
<!-- end:: Content -->

</div>

@endsection

@section('custom-script-content')

<script src="assets/js/pages/dashboard.js" type="text/javascript"></script>

@endsection