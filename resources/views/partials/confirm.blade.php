<!-- Confirm Modal -->
<div class="modal fade hide" id="modal-CONFIRM" tabindex="-1" role="dialog"
aria-hidden="true" style="display:none;" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Are you absolutely sure?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="kt-form" id="form-modal-confirm" action="">
            <div class="modal-body">
                <div class="alert alert-solid-danger alert-bold" role="alert" style="margin-bottom:0;">
                    <div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
                    <div class="alert-text">This action will delete the records you have selected from the database. Are you sure you want to do this?</div>
                </div>
            </div>
            <div class="modal-footer">
                <!--begin::Modal Footer-->
                @yield('modal-CONFIRM-footer-content')
                <!--end::Modal Footer-->
            </div>
            </form>
        </div>
    </div>
</div>