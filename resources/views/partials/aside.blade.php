<!-- begin:: Aside -->
<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

    <!-- begin::Aside Brand -->
    <div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
        <div class="kt-aside__brand-logo">
            <a href="index.html">
                <a href="index.html"><img src="{{asset('assets/media/logos/logo.png')}}" /></a>
            </a>
        </div>
        <div class="kt-aside__brand-tools">
            <button class="kt-aside__brand-aside-toggler kt-aside__brand-aside-toggler--left" id="kt_aside_toggler"><span></span></button>
        </div>
    </div>

    <!-- end:: Aside Brand -->

    <!-- begin:: Aside Menu -->
    <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
        <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
            <ul class="kt-menu__nav ">
                
                <!-- menu section -->
                <li class="kt-menu__item {{ Request::path() == 'dashboard' ? 'kt-menu__item--here' : '' }}">
                    <a href="{{ url('dashboard') }}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-tachometer-alt"></i><span class="kt-menu__link-text">Dashboard</span></a>
                </li>
                
                <!-- menu section -->
                <li class="kt-menu__section "><h4 class="kt-menu__section-text">Reports</h4><i class="kt-menu__section-icon fa fa-ellipsis-h"></i></li>
                  <li class="kt-menu__section "><h4 class="kt-menu__section-text">Clients</h4><i class="kt-menu__section-icon fa fa-ellipsis-h"></i></li>
                   <li class="kt-menu__item {{ Request::path() == '/commentbank' ? 'kt-menu__item--here' : '' }}">
                    <a href="{{ url('institute/manageinstitutes') }}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-city"></i><span class="kt-menu__link-text">Institute Master</span></a>
                </li> 

               <!--   <li class="kt-menu__section "><h4 class="kt-menu__section-text"> <a href="{{ url('institute/manageinstitutes') }}" class="kt-menu__link "><span class="kt-menu__link-text">Clients</span></a><i class="kt-menu__section-icon fa fa-ellipsis-h"></i></h4></li> -->


                 <li class="kt-menu__section "> <a  href="{{ url('/') }}"class="kt-menu__link "><span class="kt-menu__link-text">Users</span></a><i class="kt-menu__section-icon fa fa-ellipsis-h"></i></li>
                 <li class="kt-menu__section "><h4 class="kt-menu__section-text">Comments Bank</h4><i class="kt-menu__section-icon fa fa-ellipsis-h"></i></li>
                  <li class="kt-menu__item {{ Request::path() == '/commentbank' ? 'kt-menu__item--here' : '' }}">
                    <a href="{{ url('/commentbank') }}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-table"></i><span class="kt-menu__link-text">Comments Bank</span></a>
                </li>

                  <li class="kt-menu__section "><h4 class="kt-menu__section-text">Calculations</h4><i class="kt-menu__section-icon fa fa-ellipsis-h"></i></li>


                <li class="kt-menu__item {{ Request::path() == 'Calculations/grades' ? 'kt-menu__item--here' : '' }}">
                    <a href="{{ url('Calculations/grades') }}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-poll"></i><span class="kt-menu__link-text">Grades</span></a>
                </li>
                 <li class="kt-menu__item {{ Request::path() == 'Calculations/weightage' ? 'kt-menu__item--here' : '' }}">
                    <a href="{{ url('Calculations/weightage') }}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-poll"></i><span class="kt-menu__link-text">Weightage</span></a>
                </li>
                 <li class="kt-menu__section "><h4 class="kt-menu__section-text">Config Data</h4><i class="kt-menu__section-icon fa fa-ellipsis-h"></i></li>
                <li class="kt-menu__item {{ Request::path() == 'configData/institutetypes' ? 'kt-menu__item--here' : '' }}">
                    <a href="{{ url('configData/institutetypes') }}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-ellipsis-h"></i></i><span class="kt-menu__link-text">Institute Type</span></a>
                
                </li>
                 <li class="kt-menu__item {{ Request::path() == 'configData/parameters' ? 'kt-menu__item--here' : '' }}">
                    <a href="{{ url('configData/parameters') }}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-city"></i><span class="kt-menu__link-text">Parameters</span></a>
                </li>
                   <li class="kt-menu__item {{ Request::path() == 'Questions/questions' ? 'kt-menu__item--here' : '' }}">
                    <a href="{{ url('questions/questions') }}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-list"></i><span class="kt-menu__link-text">Questions</span></a>
                </li>
                
                <!-- menu section -->
               
            
                
               
                <li class="kt-menu__item {{ Request::path() == 'package/package' ? 'kt-menu__item--here' : '' }}">
                    <a href="{{ url('package/package') }}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-money-bill-alt"></i><span class="kt-menu__link-text">Packages</span></a>
                </li>

                
            </ul>
        </div>
    </div>

    <!-- end:: Aside Menu -->

    <!-- begin:: Aside Footer -->
    <div class="kt-aside__footer kt-grid__item" id="kt_aside_footer">
        <div class="kt-aside__footer-nav">
            <div class="kt-aside__footer-item">
                <a href="#" class="btn btn-icon"><i class="flaticon2-gear"></i></a>
            </div>
            <div class="kt-aside__footer-item">
                <a href="#" class="btn btn-icon"><i class="flaticon2-cube"></i></a>
            </div>
            <div class="kt-aside__footer-item">
                <a href="#" class="btn btn-icon"><i class="flaticon2-bell-alarm-symbol"></i></a>
            </div>
            <div class="kt-aside__footer-item">
                <button type="button" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="flaticon2-add"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-left">
                    <ul class="kt-nav">
                        <li class="kt-nav__section kt-nav__section--first">
                            <span class="kt-nav__section-text">Export Tools</span>
                        </li>
                        <li class="kt-nav__item">
                            <a href="#" class="kt-nav__link">
                                <i class="kt-nav__link-icon la la-print"></i>
                                <span class="kt-nav__link-text">Print</span>
                            </a>
                        </li>
                        <li class="kt-nav__item">
                            <a href="#" class="kt-nav__link">
                                <i class="kt-nav__link-icon la la-copy"></i>
                                <span class="kt-nav__link-text">Copy</span>
                            </a>
                        </li>
                        <li class="kt-nav__item">
                            <a href="#" class="kt-nav__link">
                                <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                <span class="kt-nav__link-text">Excel</span>
                            </a>
                        </li>
                        <li class="kt-nav__item">
                            <a href="#" class="kt-nav__link">
                                <i class="kt-nav__link-icon la la-file-text-o"></i>
                                <span class="kt-nav__link-text">CSV</span>
                            </a>
                        </li>
                        <li class="kt-nav__item">
                            <a href="#" class="kt-nav__link">
                                <i class="kt-nav__link-icon la la-file-pdf-o"></i>
                                <span class="kt-nav__link-text">PDF</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="kt-aside__footer-item">
                <a href="#" class="btn btn-icon"><i class="flaticon2-calendar-2"></i></a>
            </div>
        </div>
    </div>

    <!-- end:: Aside Footer-->
</div>
<!-- end:: Aside -->