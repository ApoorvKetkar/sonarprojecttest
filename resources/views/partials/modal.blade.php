<!-- Modal -->
<div class="modal fade hide" id="modal-CRUD" tabindex="-1" role="dialog" 
aria-hidden="true" style="display:none;" data-backdrop="static">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--begin::Modal Header-->
                @yield('modal-CRUD-header-content')
                <!--end::Modal Header-->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="kt-form" id="form-modal-info" action="" method="post">
            <div class="modal-body">
                <!--begin::Modal Body-->
                @yield('modal-CRUD-body-content')
                <!--end::Modal Body-->
            </div>
            <div class="modal-footer">
                <!--begin::Modal Footer-->
                @yield('modal-CRUD-footer-content')
                <!--end::Modal Footer-->
            </div>
            </form>
        </div>
    </div>
</div>