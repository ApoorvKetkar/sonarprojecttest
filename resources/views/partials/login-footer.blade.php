<!--begin::Item-->
<div class="kt-grid__item">
    <div class="kt-login-v2__footer">
        <div class="kt-login-v2__link">
<!--            <a href="#" class="kt-link kt-font-brand">About</a>-->
            <a href="#" class="kt-link kt-font-brand">Privacy</a>
            <a href="#" class="kt-link kt-font-brand">Legal</a>
            <a href="#" class="kt-link kt-font-brand">Contact</a>
        </div>
        <div class="kt-login-v2__info">
            <a href="#" class="kt-link">&copy;&nbsp;&nbsp;2020&nbsp;&nbsp;<a href="#">TeaQIP</a>&nbsp;&nbsp;&bull;&nbsp;&nbsp;All rights reserved</a>
        </div>
    </div>
</div>
<!--end::Item-->