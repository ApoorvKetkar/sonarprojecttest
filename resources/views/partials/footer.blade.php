<!-- begin:: Footer -->
<div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-footer__copyright">
            &copy;&nbsp;&nbsp;2020&nbsp;&nbsp;<a href="#" target="_blank" class="kt-link">TeaQIP</a>&nbsp;&nbsp;&bull;&nbsp;&nbsp;All rights reserved
        </div>
        <div class="kt-footer__menu">
            <a href="#" target="_blank" class="kt-footer__menu-link kt-link">About</a>
            <a href="#" target="_blank" class="kt-footer__menu-link kt-link">Privacy</a>
            <a href="#" target="_blank" class="kt-footer__menu-link kt-link">Legal</a>
            <a href="#" target="_blank" class="kt-footer__menu-link kt-link">Contact</a>
        </div>
    </div>
</div>
<!-- end:: Footer -->