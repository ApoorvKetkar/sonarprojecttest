<!-- begin:: Aside Secondary -->
<!--<button class="kt-aside-secondary-close kt-hide " id="kt_aside_secondary_close_btn"><i class="la la-close"></i></button>-->
<div class="kt-aside-secondary" id="kt_aside_secondary">
   
<!--
    <div class="kt-aside-secondary__toggle" id="kt_aside_secondary_toggler"></div>
    
    <button class="kt-aside-secondary__mobile-nav-toggler" id="kt_aside_secondary_mobile_nav_toggler" data-toggle="kt-tooltip" title="aside Secondary" data-placement="left"></button>
-->
    
    <div class="kt-aside-secondary__nav kt-grid kt-grid--hor">
       
        <div class="kt-grid__item kt-grid__item--fluid kt-aside-secondary__nav-body">
            <ul class="kt-aside-secondary__nav-toolbar nav nav-tabs" role="tablist" id="kt_aside_secondary_nav">
               
                <li class="kt-aside-secondary__nav-toolbar-item nav-item" data-toggle="kt-tooltip" title="Visit Website" data-placement="left">
                    <a class="kt-aside-secondary__nav-toolbar-icon nav-link" href="http://teaqip.com" target="_blank">
                        <i class="fa fa-globe kt-font-brand"></i>
                    </a>
                </li>
                <li class="kt-aside-secondary__nav-toolbar-item nav-item" data-toggle="kt-tooltip" title="Feedback Manager" data-placement="left">
                    <a class="kt-aside-secondary__nav-toolbar-icon nav-link" href="#" target="_blank">
                        <i class="fa fa-bullhorn kt-font-danger"></i>
                    </a>
                </li>
                <li class="kt-aside-secondary__nav-toolbar-item nav-item" data-toggle="kt-tooltip" title="User Guide" data-placement="left">
                    <a class="kt-aside-secondary__nav-toolbar-icon nav-link" href="#" target="_blank">
                        <i class="fa fa-book-reader kt-font-success"></i>
                    </a>
                </li>
                
            </ul>
        </div>
        
        <div class="kt-grid__item kt-aside-secondary__nav-foot">
            <ul class="kt-aside-secondary__nav-toolbar">
                <li class="kt-aside-secondary__nav-toolbar-item" data-toggle="kt-tooltip" title="Helpdesk" data-placement="left">
                    <a class="kt-aside-secondary__nav-toolbar-icon" href="#" target="_blank">
                        <i class="fa fa-exclamation-circle kt-font-warning"></i>
                    </a>
                </li>
            </ul>
        </div>
        
    </div>
</div>
<!-- end:: Aside Secondary -->