<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('login');
// });

Route::get('/dashboard', function () {
    return view('dashboard');
});


// Route::get('/feedback/parameters', function () {
//     return view('feedback.parameters');
// });


// Route::get('/feedback/commentsbank', function () {
//     return view('feedback.commentsbank');
// });

//Final Route of Login
Route::get('/','loginController@index');
Route::post('login','loginController@store');
Route::post('/demo/{related_parameter_id}','CommentsBank\commentsbankController@create');




// Final Route of institutetypes

Route::get('/configData/institutetypes','configData\instituteTypesController@index');
Route::post('/institutetypescreate','configData\instituteTypesController@store');
Route::get('institutetypesdelete/{id}','configData\instituteTypesController@destroy');
Route::post('institutetypesupdate/{id}','configData\instituteTypesController@update');
Route::post('/institutetype/enable/{id}','configData\instituteTypesController@enable');
Route::post('/institutetype/disable/{id}','configData\instituteTypesController@disable');

//Route::get('institutetypesEdit','institute\instituteTypesController@update');


//Final Route of questions
// Route::get('/feedback/questions','feedback\questionsController@index');
// Route::post('/questionscreate','feedback\questionsController@store');
// Route::get('questionsdelete/{id}','feedback\questionsController@destroy');
// Route::post('questionsupdate/{id}','feedback\questionsController@update');
//Final Route of grades
Route::get('/feedback/grade','feedback\gradeController@index');
Route::post('/gradecreate','feedback\gradeController@store');
Route::get('gradedelete/{id}','feedback\gradeController@destroy');
Route::post('gradeupdate/{id}','feedback\gradeController@update');

// NITIN + 05Jan2020 START
// Final Routes for Parameters
Route::post('/parameters/parameterchange','configData\parametersController@radiobuttonparameterchange');
Route::get ('/configData/parameters', 'configData\parametersController@index');
Route::post('/parameters/store/{int_type_id}/{mst_inst_id}/{radio}','configData\parametersController@store');
Route::post('/parameters/update/{id}/{int_type_id}/{mst_inst_id}/{radio}','configData\parametersController@update');
Route::post('/parameters/enable/{id}/{int_type_id}/{mst_inst_id}/{radio}','configData\parametersController@enable');
Route::post('/parameters/disable/{id}/{int_type_id}/{mst_inst_id}/{radio}','configData\parametersController@disable');
Route::get ('/parameters/Institutetypes','configData\parametersController@institutetypes');
Route::post('/parameters/institutefilterdata','configData\parametersController@showfilterdata1');
Route::post('/parameters/filterdata/{myRadio}/{mst_inst_id}','configData\parametersController@showfilterdata');
Route::get ('/parameters/delete/{id}/{int_type_id}','configData\parametersController@destroy');


// NITIN + 05Jan2020 END

//Final Route of Institue
Route::get('/institute/manageinstitutes','institute\instituteController@index');
Route::get ('/institute/create/{int_type_id}','institute\instituteController@create');
Route::post('institute/institutestore/{int_type_id}','institute\instituteController@store');
Route::get('/institute/edit/{id}','institute\instituteController@edit');
Route::post('/institute/instituteupdate/{id}','institute\instituteController@update');
Route::get('/institute/show/{id}','institute\instituteController@show');
Route::post('/institute/enable/{id}/{int_type_id}','institute\instituteController@enable');
Route::post('/institute/disable/{id}/{int_type_id}','institute\instituteController@disable');
Route::get ('/institute/Institutetypes','institute\instituteController@institutetypes');
Route::post('/institute/filterdata','institute\instituteController@showfilterdata');
Route::get('/institute/institutedelete/{id}/{int_type_id}','institute\instituteController@destroy');


//Final Questions

// Route::post('/questions/questionschange','configData\questionsController@radiobuttonquestionschange');
// Route::post('/questions/questionsfilterdata','configData\questionsController@showmasterinstitutefilterdata');
// Route::get('/Questions/questions','configData\questionsController@index');
// Route::get ('/questions/create/{int_type_id}/{mst_inst_id}/{radio}','configData\questionsController@create');
// Route::post('/questionstore/{int_type_id}','configData\questionsController@store');
// Route::post('/questions/questionupdate/{id}/{{int_type_id}}/{mst_inst_id}/{radio}','configData\questionsController@update');
// Route::post('/questions/enable/{id}/{int_type_id}/{mst_inst_id}/{radio}','configData\questionsController@enable');
// Route::post('/questions/disable/{id}/{int_type_id}/{mst_inst_id}/{radio}','configData\questionsController@disable');
// Route::get('/questions/show/{id}/{int_type_id}/{mst_inst_id}/{radio}','configData\questionsController@show');
// Route::get('/questions/edit/{id}/{int_type_id}','configData\questionsController@edit');
// Route::post('/questions/getParameters/{mst_inst_id}/{radio}','configData\questionsController@getParameters');
// Route::post('/questions/filterQuestions/{int_type_id}/{mst_inst_id}/{radio}','configData\questionsController@filterQuestions');


// Route::post('/questions/questionschange','configData\questionsController@radiobuttonquestionschange');
// Route::post('/questions/questionsfilterdata','configData\questionsController@showmasterinstitutefilterdata');
// Route::get('/questions/questions','configData\questionsController@index');
// Route::get ('/questions/create/{int_type_id}/{mst_inst_id}/{radio}','configData\questionsController@create');
// Route::post('/questions/questionsstore/{int_type_id}/{mst_inst_id}/{radio}','configData\questionsController@store');
// Route::post('/questions/questionupdate/{id}/{int_type_id}/{mst_inst_id}/{radio}','configData\questionsController@update');
// Route::post('/questions/enable/{id}/{int_type_id}/{mst_inst_id}/{radio}','configData\questionsController@enable');
// Route::post('/questions/disable/{id}/{int_type_id}/{mst_inst_id}/{radio}','configData\questionsController@disable');
// Route::get('/questions/show/{id}/{int_type_id}/{mst_inst_id}/{radio}','configData\questionsController@show');
// Route::get('/questions/edit/{id}/{int_type_id}/{mst_inst_id}/{radio}','configData\questionsController@edit');
// Route::post('/questions/getParameters/{mst_inst_id}/{radio}','configData\questionsController@getParameters');
// Route::post('/questions/filterQuestions/{int_type_id}/{mst_inst_id}/{radio}','configData\questionsController@filterQuestions');



Route::post('/questions/questionschange','configData\questionsController@radiobuttonquestionschange');
Route::post('/questions/questionsfilterdata','configData\questionsController@showmasterinstitutefilterdata');
Route::get('/questions/questions','configData\questionsController@index');
Route::get ('/questions/create/{int_type_id}/{mst_inst_id}/{radio}','configData\questionsController@create');
Route::post('/questions/questionsstore/{int_type_id}/{mst_inst_id}/{radio}','configData\questionsController@store');
Route::post('/questions/questionupdate/{id}/{int_type_id}/{mst_inst_id}/{radio}','configData\questionsController@update');
Route::post('/questions/enable/{id}/{int_type_id}/{mst_inst_id}/{radio}','configData\questionsController@enable');
Route::post('/questions/disable/{id}/{int_type_id}/{mst_inst_id}/{radio}','configData\questionsController@disable');
Route::get('/questions/show/{id}/{int_type_id}/{mst_inst_id}/{radio}','configData\questionsController@show');
Route::get('/questions/edit/{id}/{int_type_id}/{mst_inst_id}/{radio}','configData\questionsController@edit');
Route::post('/questions/getParameters/{mst_inst_id}/{radio}','configData\questionsController@getParameters');
Route::post('/questions/filterQuestions/{int_type_id}/{mst_inst_id}/{radio}','configData\questionsController@filterQuestions');


// Final Routes for grade
Route::get('/Calculations/grades','Calculations\gradesController@index');
Route::post('/gradecreate/{mst_inst_id}/{radio}','Calculations\gradesController@store');
Route::post('gradeupdate/{id}/{mst_inst_id}/{radio}','Calculations\gradesController@update');
Route::post('/grade/enable/{id}/{mst_inst_id}/{radio}','Calculations\gradesController@enable');
Route::post('/grade/disable/{id}/{mst_inst_id}/{radio}','Calculations\gradesController@disable');
Route::post('/grades/gradechange','Calculations\gradesController@radiobuttongradechange');
Route::post('/grades/institutefilterdata','Calculations\gradesController@showfiltermasterinstitutedata');
Route::get('gradedelete/{id}','Calculations\gradesController@destroy');

//Final Routes for Package

Route::get ('/package/package', 'package\packageController@index');
Route::get ('/package/Institutetypes','package\packageController@institutetypes');
Route::post('/package/filterdata','package\packageController@showfilterdata');
Route::post('/package/enable/{id}/{int_type_id}','package\packageController@enable');
Route::post('/package/disable/{id}/{int_type_id}','package\packageController@disable');
Route::get ('/package/delete/{id}/{int_type_id}','package\packageController@destroy');
Route::post('/package/update/{id}/{int_type_id}','package\packageController@update');
Route::post('/package/store/{int_type_id}','package\packageController@store');


//Final commentBanks
Route::get('/commentbank','CommentsBank\commentsbankController@index');
Route::post('/commentbank/getParameters','CommentsBank\commentsbankController@getParameters');
Route::post('/commentbank/filtercommentsbank/{int_type_id}','CommentsBank\commentsbankController@filterCommentsbank');
Route::post('/commentbank/create/{int_type_id}','CommentsBank\commentsbankController@store');


//Final Related Parameters Comments
Route::post('/commentbank/storerelatedparameter','CommentsBank\commentsbankController@relatedParameterstore');
Route::post('/commentbank/insertrelatedparameter','CommentsBank\commentsbankController@insertrelatedParameters');
Route::get('/commentbank/getRelatedParameterComments/{int_type_id}/{parameter_id}/{related_parameter_id}/{show_comments} ','CommentsBank\commentsbankController@getRelatedParameterComments');



//Final Routes for weightage
Route::get('/Calculations/weightage','Calculations\weightageController@index');
Route::post('/weightagecreate','Calculations\weightageController@store');