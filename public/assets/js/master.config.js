"use strict";

jQuery(document).ready(function() {

    // Configuration for Datatables
	$('.basicDatatables').DataTable({
        responsive: true,
        pagingType: 'full_numbers',
        columnDefs: [
        {
         "orderable": false, 
          "targets": 'no-sort'
          }
        ]
    });

    // Configuration for Toasts
    $('.toast').toast({
        delay: 5000
    });
    $('.toast').toast('show');
    
});